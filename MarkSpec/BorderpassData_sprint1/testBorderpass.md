 ระบบทะเบียนผู้ถือหนังสือผ่านแดน
------------------------------------

กำหนดให้มีการกรอกข้อมูลผู้ถือหนังสือผ่านแดน แล้วในกรณีบันทึกสำเร็จ

เมื่อเปิด Google Chrome

    เปิด "C:/Users/Administrator/AppData/Local/Google/Chrome SxS/Application/chrome.exe"

คลิกที่ ![](urladdress.png)

    เจอ "urladdress"
    คลิก "urladdress"

สั่งเปิดหน้า

    พิมพ์ "localhost:8080/immigration/LoginBorderpass.zul\n"



กรอก ข้อมูลผู้ถือหนังสือผ่านแดน
คลิกที่ ![](idperonal.png)

    เจอ "idperonal"
    คลิก    "idperonal"
    พิมพ์    "4444444444444"
    หยุด    2 วิ

คลิกที่ ![](name.png)

    เจอ "name"
    คลิก    "name"
    พิมพ์    "Dominic"
    หยุด    2 วิ

คลิกที่ ![](lastname.png)

    เจอ "lastname"
    คลิก    "lastname"
    พิมพ์    "Toretto"
    หยุด    2 วิ

คลิกที่ ![](occupation.png)

    เจอ "occupation"
    คลิก    "occupation"
    พิมพ์    "Driver"
    หยุด    2 วิ

คลิกที่ ![](nationality.png)

    เจอ "nationality"

คลิกที่ ![](combonationality.png)

    เจอ "combonationality"
    คลิก    "combonationality"

คลิกที่ ![](namenationality.png)

    เจอ "namenationality"
    คลิก    "namenationality"
    หยุด    2 วิ

คลิกที่ ![](datebirth.png)

    เจอ "datebirth"

คลิกที่ ![](Date.png)

    เจอ "Date"
    คลิก    "Date"

คลิกที่ ![](datemonth.png)

    เจอ "datemonth"
    คลิก    "datemonth"
    หยุด    2 วิ

คลิกที่ ![](domicile.png)

    เจอ "domicile"
    คลิก    "domicile"
    พิมพ์    "Karasin"
    หยุด    2 วิ

คลิกที่ ![](sex.png)

    เจอ "sex"

คลิกที่ ![](male.png)

    คลิก    "male"
    หยุด    2 วิ

คลิกที่ ![](slide.png)

    เจอ "slide"
    คลิก    "slide"
    หยุด    2 วิ

คลิกที่ ![](submit.png)

    เจอ "submit"
    คลิก    "submit"
    หยุด    2 วิ

คลิกที่ ![](showsubmit.png)

    เจอ "showsubmit"

คลิกที่ ![](ok.png)

    คลิก    "ok"
    หยุด    2 วิ




