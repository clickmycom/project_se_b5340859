#เอกสารการทดสอบการบันทึกและแก้ไขข้อมูลการกระทำความผิด

    ใช้ Sikuli


ข้อกำหนดที่1 การ Log in เข้าระบบ
----------------------------


<b><u>กรณีที่ Login เข้าระบบไม่ได้</u></b>


เตรียมทำการเปิด Google Chrome ขึ้นมา

    เปิด "C:/Program Files/Google/Chrome/Application/chrome.exe"

เมื่อคลิกที่ ![](Urlbar.png)

    คลิก "Urlbar"

และสั่งเปิดหน้า Log in

    พิมพ์   "localhost:8080/immigration/LoginDataOffense.zul\n"
    หยุด   5 วิ

และกรอก Username และ Password คลิกที่ ![](Username.png)

    เจอ     "Username"
    คลิก    "Username"
    พิมพ์    "CPE5317738"
    หยุด    1 วิ

และกรอก Password คลิกที่ ![](Password.png)

    เจอ     "Password"
    คลิก    "Password"
    พิมพ์    "asdfghjklqwert"
    หยุด    1 วิ

และกดปุ่ม Log in คลิกที่ปุ่ม ![](btnLogin1.png)

    เจอ     "btnLogin1"
    คลิก    "btnLogin1"
    หยุด    1 วิ

และจะไม่พบข้อมูลที่กรอกเข้ามา แสดงหน้าต่างแจ้งเตือน ![](alertNoUser2.png)

    เจอ     "alertNoUser2"

และคลิกที่ปุ่ม OK ![](btnOK.png)

    เจอ     "btnOK"
    คลิก    "btnOK"
    หยุด     1 วิ

<b><u>กรณีที่ Log in ได้ถูกต้อง</u></b>


และกรอก Password ใหม่ คลิกที่ ![](passName.png)

    เจอ     "passName"
    คลิก    "passName"
    พิมพ์    "0815934019"
    หยุด    1 วิ

และกดปุ่ม Log in คลิกที่ปุ่ม ![](btnLogin1.png)

    เจอ     "btnLogin1"
    คลิก    "btnLogin1"
    หยุด     1 วิ

และแสดงหน้าต่างแจ้งการ login เข้าระบบสำเร็จ ![](alertAdmin.png)

    เจอ     "alertAdmin"
    หยุด     1 วิ

แล้วคลิกที่ปุ่ม ![](ok.png)

    เจอ     "ok"
    คลิก    "ok"


ข้อกำหนดที่2 การบันทึกข้อมูลประวัติการกระทำความผิด
------------------------------------------------


เมื่อกดปุ่ม บันทึกข้อมูลบนเมนูที่หน้า Home คลิกที่ปุ่ม ![](saveData.png)

    เจอ     "saveData"
    คลิก    "saveData"
    หยุด     1 วิ

และกรอกชื่อผู้กระทำความผิดคลิกที่ช่อง ![](firstname.png)

    เจอ     "firstname"
    คลิกที่ "140px" ด้านขวาของ "firstname"
    พิมพ์    "robinvan"
    หยุด    1 วิ

และกรอกนามสกุลผู้กระทำความผิดคลิกที่ช่อง ![](lastname.png)

    เจอ     "lastname"
    คลิกที่  "140px"    ด้านขวาของ  "lastname"
    พิมพ์    "persie"
    หยุด    1 วิ

และกรอกเลขบัตรประจำตัวประชาชนคลิกที่ช่อง ![](identify.png)

    เจอ     "identify"
    คลิกที่ "140px" ด้านขวาของ "identify"
    พิมพ์    "1361390034162"
    หยุด    1 วิ

และกรอกอายุคลิกที่ช่อง ![](age.png)

    เจอ     "age1"
    คลิก     "age1"
    พิมพ์     "50"
    หยุด     1 วิ

และกรอกวัน/เดือน/ปี เกิดคลิกที่ ![](birthDate.png)

    เจอ     "birthDate"
    คลิก     "btnDate"
    คลิก     "selectDate"
    หยุด     1 วิ

และกรอกสัญชาติคลิกที่ ![](nationality.png)

    เจอ     "nationality"
    คลิกที่  "145px" ด้านขวาของ "nationality"
    คลิก    "selectNationality"
    หยุด    1 วิ

และกรอกที่อยู่คลิกที่ ![](address.png)

    เจอ     "address"
    คลิกที่  "145px" ด้านขวาของ "address"
    พิมพ์    "Manchester City,England"
    หยุด    5 วิ

และคลิกที่ ![](เลื่อนลง.png)

    คลิก    "เลื่อนลง"
    คลิก    "เลื่อนลง"
    หยุด    3 วิ

และกรอกหมายเลขคดีความคลิกที่ ![](lawsuitID.png)

    เจอ     "lawsuitID"
    คลิกที่  "145px" ด้านขวาของ "lawsuitID"
    พิมพ์    "N1234564"
    หยุด    1 วิ

และกรอกชื่อคดีความคลิกที่ ![](lawsuitName.png)

    เจอ     "lawsuitName"
    คลิกที่  "145px" ด้านขวาของ "lawsuitName"
    พิมพ์   "malefaction"
    หยุด    1 วิ

และกรอกวันเริ่มอายุคดีความคลิกที่ ![](prescription.png)

    เจอ     "prescription"
    คลิกที่  "140px" ด้านขวาของ "prescription"
    คลิก     "Selectprescription"
    หยุด     1 วิ

และกรอกวันหมดอายุคดีความคลิกที่ ![](Eprescription.png)

    เจอ     "Eprescription"
    คลิกที่  "140px" ด้านขวาของ "Eprescription"
    คลิก     "SelectEprescription"
    หยุด     1 วิ

และกรอกรายละเอียดคดีความคลิกที่ ![](detail.png)

    เจอ     "detail1"
    คลิก    "detail2"
    พิมพ์    "Shoplifting, Clinic for Exotic Animals Make-a-Wish Batman Fantasy"
    หยุด     1 วิ

และกดปุ่ม Save เพื่อบันทึกข้อมูล คลิกทีปุ่ม ![](btnSave.png)

    เจอ     "btnSave"
    คลิก    "btnSave"
    หยุด    1 วิ

และจะไม่สามารถบันทึกข้อมูลได้แสดงหน้าต่างแจ้งเตือน ![](alertNosave.png)คลิกที่ปุ่ม ![](btnOK1.png)

    เจอ     "btnOK1"
    คลิก    "btnOK1"
    หยุด     1 วิ

<b><u>กรณีที่บันทึกข้อมูลได้ถูกต้อง</u></b>

และกรอกชื่อเจ้าของคดีความคลิกที่ ![](owner.png)

    เจอ     "owner"
    คลิกที่  "145px" ด้านขวาของ "owner"
    พิมพ์   "Somkid Jaidee"
    หยุด    1 วิ

และกดปุ่ม Save เพื่อบันทึกข้อมูลอีกครั้ง คลิกทีปุ่ม ![](btnSave.png)

    เจอ     "btnSave"
    คลิก    "btnSave"
    หยุด    1 วิ


และแสดงหน้าต่างแจ้งเตือน ![](alertYesSave.png)
และคลิกที่ปุ่ม ![](btnOk2.png)

    เจอ     "btnOk2"
    คลิก    "btnOk2"
    หยุด    1 วิ

และทำการ clear ข้อมูลในช่องกรอกข้อมูล
และคลิกที่ปุ่ม ![](btnreset.png)

    เจอ     "btnreset"
    คลิก    "btnreset"
    หยุด    1 วิ

แล้วคลิกที่ ![](เลื่อนขึ้น.png)

    คลิก    "เลื่อนขึ้น"
    คลิก    "เลื่อนขึ้น"
    คลิก    "เลื่อนขึ้น"
    คลิก    "เลื่อนขึ้น"
    คลิก    "เลื่อนขึ้น"
    คลิก    "เลื่อนขึ้น"
    คลิก    "เลื่อนขึ้น"
    คลิก    "เลื่อนขึ้น"
    คลิก    "เลื่อนขึ้น"
    หยุด    1 วิ

ข้อกำหนดที่ 3 การแก้ไขข้อมูลประวัติการกระทำความผิด
-----------------------------

เมื่อกดปุ่มแก้ไขข้อมูลบนเมนู คลิกที่ ![](editData.png)

    เจอ     "editData"
    คลิก    "editData"
    หยุด    1 วิ

และทำการค้นหาข้อมูลเพื่อนำมาแก้ไขโดยใช้หมายเลขคดีค้นหาคลิกที่ ![](findText.png)

    เจอ     "findText"
    คลิก    "findText"
    พิมพ์    "11111111"
    หยุด    1 วิ

และค้นหาข้อมูลคลิกที่ปุ่ม ![](find.png)

    เจอ     "find"
    คลิก    "find"
    หยุด    1 วิ

และแสดงหน้าต่างแจ้งเตือน ![](findYes.png)
และคลิกที่ปุ่ม ![](btnOK.png)

    เจอ     "btnOK"
    คลิก    "btnOK"
    หยุด    1 วิ

และกดปุ่มแก้ไขข้อมูลคลิกที่ปุ่ม ![](btnEdit.png)

    เจอ     "btnEdit"
    คลิก    "btnEdit"
    หยุด    1 วิ

และคลิกที่ ![](เลื่อนลง.png)

    คลิก    "เลื่อนลง"
    คลิก    "เลื่อนลง"
    หยุด    1 วิ

และกรอกข้อมูลที่ต้องการแก้ไขคลิกที่ช่อง ![](editDetail.png)

    เจอ     "editDetail1"
    คลิก    "editDetail2"
    พิมพ์     "Application,England to manchester, Clinic for Exotic Animals, Make-a-Wish Batman Fantasy"
    หยุด    1 วิ


และกดปุ่ม Save เพื่อบันทึกการแก้ไขข้อมูลคลิกทีปุ่ม ![](btnSave.png)

    เจอ     "btnSave"
    คลิก    "btnSave"
    หยุด    1 วิ

และแสดงหน้าต่างแจ้งเตือน ![](alertYesSave.png)
และคลิกที่ปุ่ม ![](btnOk2.png)

    เจอ     "btnOk2"
    คลิก    "btnOk2"
    หยุด    1 วิ

และคลิกที่ ![](เลื่อนขึ้น.png)

    คลิก    "เลื่อนขึ้น"
    คลิก    "เลื่อนขึ้น"
    คลิก    "เลื่อนขึ้น"
    คลิก    "เลื่อนขึ้น"
    คลิก    "เลื่อนขึ้น"
    คลิก    "เลื่อนขึ้น"
    คลิก    "เลื่อนขึ้น"
    คลิก    "เลื่อนขึ้น"
    คลิก    "เลื่อนขึ้น"
    คลิก    "เลื่อนขึ้น"
    หยุด    2 วิ

แล้วออกจากระบบคลิกที่ ![](logout.png)

    เจอ     "logout"
    คลิก     "logout"
    หยุด     1 วิ
















