![](logo.png) ระบบลงทะเบียนสถานประกอบการ
------------------------------------


กำหนดให้กรณีที่มีAccountสถานประกอบการแล้ว


เมื่อเปิด Google Chrome

    เปิด "C:/Program Files (x86)/Google/Chrome/Application/chrome.exe"


คลิกที่ ![](Addressbar.png)

    คลิก "Addressbar"

สั่งเปิดหน้า

    พิมพ์   "localhost:8080/immigration/loginenterpriseindex.zul\n"

คลิกที่ ![](Textbox-Username.png)

    เจอ "Textbox-Username"
    คลิก "Textbox-Username"
    หยุด 1 วิ
    พิมพ์ "enterprise"

คลิกที่ ![](Textbox-Password.png)

    เจอ "Textbox-Password"
    คลิก "Textbox-Password"
    หยุด 1 วิ
    พิมพ์ "enterprise"

คลิกที่ปุ่ม Login ![](Button-Login.png)

    เจอ "Button-Login"
    คลิก "Button-Login"
    หยุด 4 วิ

ควรจะพบหน้าจอผู้ใช้บริการ ![](DataEnterprise.png)

    เจอ "DataEnterprise"

ต้องการบันทึกรับคนต่างด้าว คลิก ![](ไอคอนบันทึกรับคนต่างด้าว.png)

    เจอ "ไอคอนบันทึกรับคนต่างด้าว"
    คลิก "ไอคอนบันทึกรับคนต่างด้าว"

ควรจะพบ Pop up บันทึกรับคนต่างด้าว คลิก ![](บันทึกรับคนต่างด้าว.png)

    เจอ "บันทึกรับคนต่างด้าว"
    คลิก "บันทึกรับคนต่างด้าว"

ควรจะพบกับหน้าเจอ ![](หน้าจอบันทึกรับคนต่างด้าว.png)

    เจอ "หน้าจอบันทึกรับคนต่างด้าว"

เมื่อพบวันที่เข้าพัก ![](วันที่เข้าพัก.png)

    เจอ "วันที่เข้าพัก"

คลิกที่ปฏิทินเพื่อบันทึกรับ ![](Datebox.png)

    เจอ "Datebox"
    คลิก "Datebox"

แล้วควรจะเจอ ปฏิทินเดือน ![](ปฏิทิน.png)

    เจอ "ปฏิทิน"

แล้วทำการเลือกวันที่ ![](วันที่รับเข้า.png)

    เจอ "วันที่รับเข้า"
    คลิก "วันที่รับเข้า"
    หยุด 2 วิ

