 ระบบลงทะเบียนสถานประกอบการ
------------------------------------

กำหนดให้กรณีที่มีAccountสถานประกอบการแล้ว Login ผิดพลาด



เมื่อเปิด Google Chrome

    เปิด "C:/Program Files (x86)/Google/Chrome/Application/chrome.exe"

คลิกที่ ![](Addressbar.png)

    คลิก "Addressbar"

สั่งเปิดหน้า

    พิมพ์   "localhost:8080/immigration/loginenterpriseindex.zul\n"
    หยุด 2 วิ

คลิกที่ ![](Textbox-Username.png)

    เจอ "Textbox-Username"
    คลิก "Textbox-Username"
    หยุด 1 วิ
    พิมพ์ "123888"

คลิกที่ ![](Textbox-Password.png)

    เจอ "Textbox-Password"
    คลิก "Textbox-Password"
    หยุด 1 วิ
    พิมพ์ "12345"

คลิกที่ปุ่ม Login ![](Button-Login.png)

    เจอ "Button-Login"
    คลิก "Button-Login"
    หยุด 3 วิ

ควรจะเจอ ข้อความ ![]("Account_ถูกระงับการใช้งาน"
.png)

    เจอ "Account_ถูกระงับการใช้งาน"

คลิกที่ปุ่ม OK ![](กดปุ่ม_OK.png)

    เจอ "กดปุ่ม_OK"
    คลิก "กดปุ่ม_OK"
