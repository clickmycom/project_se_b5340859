 ระบบลงทะเบียนสถานประกอบการ
------------------------------------

กำหนดให้กรณีที่มีAccountสถานประกอบการแล้ว



เมื่อเปิด Google Chrome

    เปิด "C:/Program Files (x86)/Google/Chrome/Application/chrome.exe"


คลิกที่ ![](Addressbar.png)

    คลิก "Addressbar"

สั่งเปิดหน้า

    พิมพ์   "localhost:8080/immigration/loginenterpriseindex.zul\n"

คลิกที่ ![](Textbox-Username.png)

    เจอ "Textbox-Username"
    คลิก "Textbox-Username"
    หยุด 1 วิ
    พิมพ์ "00000"

คลิกที่ ![](Textbox-Password.png)

    เจอ "Textbox-Password"
    คลิก "Textbox-Password"
    หยุด 1 วิ
    พิมพ์ "00000"

คลิกที่ปุ่ม Login ![](Button-Login.png)

    เจอ "Button-Login"
    คลิก "Button-Login"
    หยุด 4 วิ

ควรจะพบหน้าจอผู้ใช้บริการ ![](DataEnterprise.png)

    เจอ "DataEnterprise"

ต้องการดูข้อมูลสถานประกอบการ คลิก ![](icon-Enterprise.png)

    เจอ "icon-Enterprise"
    คลิก "icon-Enterprise"

เปิดข้อมูลสถานประกอบการ คลิก ![](popup-enterprise.png)

    ความเหมือน 0.9
    เจอ "popup-enterprise"
    คลิก "popup-enterprise"

ควรจะพบข้อมูลสถานประกอบการ ![](enterprise.png)

    เจอ "enterprise"
    หยุด 5 วิ

หากต้องการกลับหน้าหลัก คลิก ![](Button-Home.png)

    คลิก "Button-Home"

หากต้องการออกจากระบบ คลิก ![](Button-Logout.png)

    เจอ "Button-Logout"
    คลิก "Button-Logout"
