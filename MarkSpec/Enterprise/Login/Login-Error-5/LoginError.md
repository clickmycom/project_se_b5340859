 ระบบลงทะเบียนสถานประกอบการ
------------------------------------

กำหนดให้กรณีที่มีAccountสถานประกอบการแล้ว Login ผิดพลาด



เมื่อเปิด Google Chrome

    เปิด "C:/Program Files (x86)/Google/Chrome/Application/chrome.exe"


คลิกที่ ![](Addressbar.png)

    คลิก "Addressbar"

สั่งเปิดหน้า

    พิมพ์   "localhost:8080/immigration/loginenterpriseindex.zul\n"
    หยุด 2 วิ


คลิกที่ ![](Textbox-Username.png)

    เจอ "Textbox-Username"
    คลิก "Textbox-Username"
    หยุด 1 วิ
    พิมพ์ "00000"

คลิกที่ ![](Textbox-Password.png)

    เจอ "Textbox-Password"
    คลิก "Textbox-Password"
    หยุด 1 วิ
    พิมพ์ "12345"

คลิกที่ปุ่ม Login ![](Button-Login.png)

    เจอ "Button-Login"
    คลิก "Button-Login"
    หยุด 3 วิ

ควรจะเจอ ![](Error-Message.png)

    เจอ "Error-Message"

ควนจะเจอ ![](Error-1.png)

    เจอ "Error-1"

คลิกที่ปุ่ม OK ![](Button-OK.png)

    เจอ "Button-OK"
    คลิก "Button-OK"

คลิกที่ ![](Textbox-Username.png)

    เจอ "Textbox-Username"
    คลิก "Textbox-Username"
    หยุด 1 วิ
    พิมพ์ "00000"

คลิกที่ ![](Textbox-Password.png)

    เจอ "Textbox-Password"
    คลิก "Textbox-Password"
    หยุด 1 วิ
    พิมพ์ "12345"

คลิกที่ปุ่ม Login ![](Button-Login.png)

    เจอ "Button-Login"
    คลิก "Button-Login"
    หยุด 3 วิ

ควรจะเจอ ![](Error-Message.png)

    เจอ "Error-Message"

ควนจะเจอ ![](Error-2.png)

    เจอ "Error-2"

คลิกที่ปุ่ม OK ![](Button-OK.png)

    เจอ "Button-OK"
    คลิก "Button-OK"

คลิกที่ ![](Textbox-Username.png)

    เจอ "Textbox-Username"
    คลิก "Textbox-Username"
    หยุด 1 วิ
    พิมพ์ "00000"

คลิกที่ ![](Textbox-Password.png)

    เจอ "Textbox-Password"
    คลิก "Textbox-Password"
    หยุด 1 วิ
    พิมพ์ "12345"

คลิกที่ปุ่ม Login ![](Button-Login.png)

    เจอ "Button-Login"
    คลิก "Button-Login"
    หยุด 3 วิ

ควรจะเจอ ![](Error-Message.png)

    เจอ "Error-Message"

ควนจะเจอ ![](Error-3.png)

    เจอ "Error-3"

คลิกที่ปุ่ม OK ![](Button-OK.png)

    เจอ "Button-OK"
    คลิก "Button-OK"

คลิกที่ ![](Textbox-Username.png)

    เจอ "Textbox-Username"
    คลิก "Textbox-Username"
    หยุด 1 วิ
    พิมพ์ "00000"

คลิกที่ ![](Textbox-Password.png)

    เจอ "Textbox-Password"
    คลิก "Textbox-Password"
    หยุด 1 วิ
    พิมพ์ "12345"

คลิกที่ปุ่ม Login ![](Button-Login.png)

    เจอ "Button-Login"
    คลิก "Button-Login"
    หยุด 3 วิ

ควรจะเจอ ![](Error-Message.png)

    เจอ "Error-Message"

ควนจะเจอ ![](Error-4.png)

    เจอ "Error-4"

คลิกที่ปุ่ม OK ![](Button-OK.png)

    เจอ "Button-OK"
    คลิก "Button-OK"

คลิกที่ ![](Textbox-Username.png)

    เจอ "Textbox-Username"
    คลิก "Textbox-Username"
    หยุด 1 วิ
    พิมพ์ "00000"

คลิกที่ ![](Textbox-Password.png)

    เจอ "Textbox-Password"
    คลิก "Textbox-Password"
    หยุด 1 วิ
    พิมพ์ "12345"

คลิกที่ปุ่ม Login ![](Button-Login.png)

    เจอ "Button-Login"
    คลิก "Button-Login"
    หยุด 3 วิ

ควนจะเจอ ![](Error-5.png)

    เจอ "Error-5"

คลิกที่ปุ่ม OK ![](Button-OK.png)

    เจอ "Button-OK"
    คลิก "Button-OK"

คลิกที่ ![](Textbox-Username.png)

    เจอ "Textbox-Username"
    คลิก "Textbox-Username"
    หยุด 1 วิ
    พิมพ์ "00000"

คลิกที่ ![](Textbox-Password.png)

    เจอ "Textbox-Password"
    คลิก "Textbox-Password"
    หยุด 1 วิ
    พิมพ์ "12345"

คลิกที่ปุ่ม Login ![](Button-Login.png)

    เจอ "Button-Login"
    คลิก "Button-Login"
    หยุด 3 วิ

ควรจะเจอ ![](Error-Message-Block.png)

    เจอ "Error-Message-Block"

คลิกที่ปุ่ม OK ![](Button-OK.png)

    เจอ "Button-OK"
    คลิก "Button-OK"

พยามยามเข้าระบบ

    หยุด 3 วิ
    ปิด

เปิด Google Chrome

    เปิด "C:/Program Files (x86)/Google/Chrome/Application/chrome.exe"


คลิกที่ ![](Addressbar.png)

    คลิก "Addressbar"

สั่งเปิดหน้า

    พิมพ์   "localhost:8080/immigration/loginenterpriseindex.zul\n"
    หยุด 2 วิ

ควรจะเจอ ![](Error-Message-Block.png)

    เจอ "Error-Message-Block"

คลิกที่ปุ่ม OK ![](Button-OK.png)

    เจอ "Button-OK"
    คลิก "Button-OK"

ออกจากระบบ

    หยุด 3 วิ
    ปิด

