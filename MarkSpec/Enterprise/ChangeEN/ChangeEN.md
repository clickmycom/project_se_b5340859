 ระบบลงทะเบียนสถานประกอบการ
------------------------------------

กำหนดให้กรณีที่มีต้องการเปลียนภาษาอังกฤษ



เมื่อเปิด Google Chrome

    เปิด "C:/Program Files (x86)/Google/Chrome/Application/chrome.exe"


คลิกที่ ![](Addressbar.png)

    คลิก "Addressbar"

สั่งเปิดหน้า

    พิมพ์   "localhost:8080/immigration/loginenterpriseindex.zul\n"

คลิกที่ ![](Textbox-Username.png)

    เจอ "Textbox-Username"
    คลิก "Textbox-Username"
    หยุด 1 วิ
    พิมพ์ "00000"

คลิกที่ ![](Textbox-Password.png)

    เจอ "Textbox-Password"
    คลิก "Textbox-Password"
    หยุด 1 วิ
    พิมพ์ "00000"

คลิกที่ปุ่ม Login ![](Button-Login.png)

    เจอ "Button-Login"
    คลิก "Button-Login"
    หยุด 4 วิ

ควรจะพบหน้าจอผู้ใช้บริการ ![](DataEnterprise.png)

    เจอ "DataEnterprise"

ต้องการเปลี่ยนภาษาไทย  ![](เปลี่ยนภาษาอังกฤษ.png)

    เจอ "เปลี่ยนภาษาอังกฤษ"
    คลิก "เปลี่ยนภาษาอังกฤษ"

แล้วควรที่ภาษาจะเปลี่ยนเป็นภาษาไทย ![](ภาษาเปลี่ยน.png)

    เจอ "ภาษาเปลี่ยน"
