 ระบบลงทะเบียนสถานประกอบการ
------------------------------------

กำหนดให้กรณีที่มีAccountสถานประกอบการแล้ว Login ผิดพลาด



เมื่อเปิด Google Chrome

    เปิด "C:/Program Files (x86)/Google/Chrome/Application/chrome.exe"

คลิกที่ ![](Addressbar.png)

    คลิก "Addressbar"

สั่งเปิดหน้า

    พิมพ์   "localhost:8080/immigration/loginenterpriseindex.zul\n"
    หยุด 2 วิ

คลิกที่ปุ่ม Register ![](Button-Register.png)

    เจอ "Button-Register"
    คลิก "Button-Register"
    หยุด 3 วิ

ควรจะเจอ ![](Register-Form.png)

    เจอ "Register-Form"

คลิกที่ Textbox ผู้ใช้บริการ ![](Text-1.png)

    เจอ "Text-1"
    พิมพ์ "Tanawad Kanrai"

คลิกที่ Textbox ต่ำแหน่ง ![](Text-2.png)

    เจอ "Text-2"
    คลิก "Text-2"
    พิมพ์ "Student"

คลิกที่ Textbox อีเมลล์ ![](Text-3.png)

    เจอ "Text-3"
    คลิก "Text-3"
    พิมพ์ "clickmycom"

คลิกที่ Textbox UserID ![](Text-4.png)

    เจอ "Text-4"
    คลิก "Text-4"
    พิมพ์ "Enterprise-Thailand"

คลิกที่ Textbox Password ![](Text-5.png)

    เจอ "Text-5"
    คลิก "Text-4"
    พิมพ์ "00000"

คลิกที่ Textbox ชื่อสถานประกอบการ ![](Text-6.png)

    เจอ "Text-6"
    คลิก "Text-6"
    พิมพ์ "Enterprise-Cat"

เลื่อนแถบ ![](bar.png)

    เจอ "bar"
    คลิก "bar"

คลิกที่ Textbox ประเภทสถานประกอบการ ![](Text-7.png)

    เจอ "Text-7"
    คลิก "Text-7"
    พิมพ์ "OOOOOO"

คลิกที่ Textbox ตำบล ![](Text-8.png)

    เจอ "Text-8"
    คลิก "Text-8"
    พิมพ์ "ABCDEFG"

คลิกที่ Textbox อำเภอ ![](Text-9.png)

    เจอ "Text-9"
    คลิก "Text-9"
    พิมพ์ "Donkunng"

คลิกที่ Textbox จังหวัด ![](Text-10.png)

    เจอ "Text-10"
    คลิก "Text-10"
    พิมพ์ "Ratchaburi"

คลิกที่ Textbox รหัสไปรษณีย์ ![](Text-11.png)

    เจอ "Text-11"
    คลิก "Text-11"
    พิมพ์ "70130"

คลิกที่ Textbox โทรศัพท์ ![](Text-12.png)

    เจอ "Text-12"
    คลิก "Text-12"
    พิมพ์ "0000000000"

คลิกที่ Textbox แฟ็ก ![](Text-13.png)

    เจอ "Text-13"
    คลิก "Text-13"
    พิมพ์ "044-22324"

คลิกที่ปุ่ม Submit ![](Submit-Button.png)

    เจอ "Submit-Button"
    คลิก "Submit-Button"

ควรจะเจอ แจ้งเตือนบันทึก ![](save.png)

    เจอ "save"

คลิกที่ปุ่ม Ok  ![](OK-Button.png)

    คลิก "OK-Button"






