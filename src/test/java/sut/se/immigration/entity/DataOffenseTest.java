package sut.se.immigration.entity;

import org.junit.Test;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.junit.runner.RunWith;
import sut.se.immigration.entity.DataOffense;
import java.lang.Exception;
 
import javax.persistence.TypedQuery;
 
import static org.junit.Assert.*;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:/META-INF/spring/applicationContext*.xml")

public class DataOffenseTest {

    private DataOffense dataOffense = new DataOffense();
    
    // บันทึกข้อมูลถูกต้อง
    @Test
    public void persistDataOffense() {
        DataOffense d = new DataOffense();
                 d.setFirstName("Somsai");
                 d.setLastName("Boonmee");
                 d.setIdentifyNumber("1234567890123");
                 d.setAge("50");
                 d.setBirthDate("13/04/1876");
                 d.setNationality("Thai");
                 d.setAddress("Suranaree Nakhonratchasima");
                 d.setLawsuitId("N5600011");
                 d.setLawsuitName("malefaction");
                 d.setPrescription("20/11/2556");
                 d.setExpiratePrescription("20/11/2559");
                 d.setLawsuitDetail("Shoplifting, Clinic for Exotic Animals");
                 d.setOwnerLawsuit("Surasak tona");
                 
                 d.persist();
        org.junit.Assert.assertTrue(true);
    }
    
    // กรอก FirstName ไม่ถูกต้อง จะต้องกรอก a-z,A-Z เท่านั้น
    @Test
    public void persistFirstNameFalse() {
        DataOffense d = new DataOffense();
                 d.setFirstName("Somsai23");
                 d.setLastName("Boonmee");
                 d.setIdentifyNumber("1234567890123");
                 d.setAge("50");
                 d.setBirthDate("13/04/1876");
                 d.setNationality("Thai");
                 d.setAddress("Suranaree Nakhonratchasima");
                 d.setLawsuitId("N5600011");
                 d.setLawsuitName("malefaction");
                 d.setPrescription("20/11/2556");
                 d.setExpiratePrescription("20/11/2559");
                 d.setLawsuitDetail("Shoplifting, Clinic for Exotic Animals");
                 d.setOwnerLawsuit("Surasak tona");
                 
             try {
                    d.persist();
                    fail("Should not FirstName");
        } catch(javax.validation.ConstraintViolationException e) {
              
            
        }
        org.junit.Assert.assertTrue(true);
    }

    // ไม่กรอก FirstName
    @Test
    public void persistFirstNameFalseValue() {
        DataOffense d = new DataOffense();
                 d.setFirstName("");
                 d.setLastName("Boonmee");
                 d.setIdentifyNumber("1234567890123");
                 d.setAge("50");
                 d.setBirthDate("13/04/1876");
                 d.setNationality("Thai");
                 d.setAddress("Suranaree Nakhonratchasima");
                 d.setLawsuitId("N5600011");
                 d.setLawsuitName("malefaction");
                 d.setPrescription("20/11/2556");
                 d.setExpiratePrescription("20/11/2559");
                 d.setLawsuitDetail("Shoplifting, Clinic for Exotic Animals");
                 d.setOwnerLawsuit("Surasak tona");
                 
             try {
                    d.persist();
                    fail("Should not FirstName");
        } catch(javax.validation.ConstraintViolationException e) {
              
            
        }
        org.junit.Assert.assertTrue(true);
    }

    // กรอก LastName ไม่ถูกต้อง จะต้องกรอก a-z,A-Z เท่านั้น
    @Test
    public void persistLastNameFalse() {
        DataOffense d = new DataOffense();
                 d.setFirstName("Somsai");
                 d.setLastName("Boonmee23456");
                 d.setIdentifyNumber("1234567890123");
                 d.setAge("50");
                 d.setBirthDate("13/04/1876");
                 d.setNationality("Thai");
                 d.setAddress("Suranaree Nakhonratchasima");
                 d.setLawsuitId("N5600011");
                 d.setLawsuitName("malefaction");
                 d.setPrescription("20/11/2556");
                 d.setExpiratePrescription("20/11/2559");
                 d.setLawsuitDetail("Shoplifting, Clinic for Exotic Animals");
                 d.setOwnerLawsuit("Surasak tona");
                 
             try {
                    d.persist();
                    fail("Should not LastName");
        } catch(javax.validation.ConstraintViolationException e) {
              
            
        }
        org.junit.Assert.assertTrue(true);
    }

    // ไม่กรอก LastName 
    @Test
    public void persistLastNameFalseValue() {
        DataOffense d = new DataOffense();
                 d.setFirstName("Somsai");
                 d.setLastName("");
                 d.setIdentifyNumber("1234567890123");
                 d.setAge("50");
                 d.setBirthDate("13/04/1876");
                 d.setNationality("Thai");
                 d.setAddress("Suranaree Nakhonratchasima");
                 d.setLawsuitId("N5600011");
                 d.setLawsuitName("malefaction");
                 d.setPrescription("20/11/2556");
                 d.setExpiratePrescription("20/11/2559");
                 d.setLawsuitDetail("Shoplifting, Clinic for Exotic Animals");
                 d.setOwnerLawsuit("Surasak tona");
                 
             try {
                    d.persist();
                    fail("Should not LastName");
        } catch(javax.validation.ConstraintViolationException e) {
              
            
        }
        org.junit.Assert.assertTrue(true);
    }


    // กรอก IdentifyNumber ไม่ถูกต้อง จะต้องกรอก 0-9 จำนวน 13 ตัวเท่านั้น
    @Test
    public void persistIdentifyNumberFalse() {
        DataOffense d = new DataOffense();
                 d.setFirstName("Somsai");
                 d.setLastName("Boonmee");
                 d.setIdentifyNumber("AD3434GHJ");
                 d.setAge("50");
                 d.setBirthDate("13/04/1876");
                 d.setNationality("Thai");
                 d.setAddress("Suranaree Nakhonratchasima");
                 d.setLawsuitId("N5600011");
                 d.setLawsuitName("malefaction");
                 d.setPrescription("20/11/2556");
                 d.setExpiratePrescription("20/11/2559");
                 d.setLawsuitDetail("Shoplifting, Clinic for Exotic Animals");
                 d.setOwnerLawsuit("Surasak tona");
                 
             try {
                    d.persist();
                    fail("Should not IdentifyNumber");
        } catch(javax.validation.ConstraintViolationException e) {
              
            
        }
        org.junit.Assert.assertTrue(true);
    }

    // ไม่กรอก IdentifyNumber
    @Test
    public void persistIdentifyNumberFalseValue() {
        DataOffense d = new DataOffense();
                 d.setFirstName("Somsai");
                 d.setLastName("Boonmee");
                 d.setIdentifyNumber("");
                 d.setAge("50");
                 d.setBirthDate("13/04/1876");
                 d.setNationality("Thai");
                 d.setAddress("Suranaree Nakhonratchasima");
                 d.setLawsuitId("N5600011");
                 d.setLawsuitName("malefaction");
                 d.setPrescription("20/11/2556");
                 d.setExpiratePrescription("20/11/2559");
                 d.setLawsuitDetail("Shoplifting, Clinic for Exotic Animals");
                 d.setOwnerLawsuit("Surasak tona");
                 
             try {
                    d.persist();
                    fail("Should not IdentifyNumber");
        } catch(javax.validation.ConstraintViolationException e) {
              
            
        }
        org.junit.Assert.assertTrue(true);
    }

    // กรอก Age ไม่ถูกต้อง จะต้องกรอก 0-9 จำนวน 2 ตัวเท่านั้น
    @Test
    public void persistAgeFalse() {
        DataOffense d = new DataOffense();
                 d.setFirstName("Somsai");
                 d.setLastName("Boonmee");
                 d.setIdentifyNumber("1234567890123");
                 d.setAge("A050");
                 d.setBirthDate("13/04/1876");
                 d.setNationality("Thai");
                 d.setAddress("Suranaree Nakhonratchasima");
                 d.setLawsuitId("N5600011");
                 d.setLawsuitName("malefaction");
                 d.setPrescription("20/11/2556");
                 d.setExpiratePrescription("20/11/2559");
                 d.setLawsuitDetail("Shoplifting, Clinic for Exotic Animals");
                 d.setOwnerLawsuit("Surasak tona");
                 
             try {
                    d.persist();
                    fail("Should not Age");
        } catch(javax.validation.ConstraintViolationException e) {
              
            
        }
        org.junit.Assert.assertTrue(true);
    }

    // ไม่กรอก Age 
    @Test
    public void persistAgeFalseValue() {
        DataOffense d = new DataOffense();
                 d.setFirstName("Somsai");
                 d.setLastName("Boonmee");
                 d.setIdentifyNumber("1234567890123");
                 d.setAge("");
                 d.setBirthDate("13/04/1876");
                 d.setNationality("Thai");
                 d.setAddress("Suranaree Nakhonratchasima");
                 d.setLawsuitId("N5600011");
                 d.setLawsuitName("malefaction");
                 d.setPrescription("20/11/2556");
                 d.setExpiratePrescription("20/11/2559");
                 d.setLawsuitDetail("Shoplifting, Clinic for Exotic Animals");
                 d.setOwnerLawsuit("Surasak tona");
                 
             try {
                    d.persist();
                    fail("Should not Age");
        } catch(javax.validation.ConstraintViolationException e) {
              
            
        }
        org.junit.Assert.assertTrue(true);
    }

    // ไม่กรอก birthDate 
    @Test
    public void persistbirthDateFalse() {
        DataOffense d = new DataOffense();
                 d.setFirstName("Somsai");
                 d.setLastName("Boonmee");
                 d.setIdentifyNumber("1234567890123");
                 d.setAge("50");
                 d.setBirthDate("");
                 d.setNationality("Thai");
                 d.setAddress("Suranaree Nakhonratchasima");
                 d.setLawsuitId("N5600011");
                 d.setLawsuitName("malefaction");
                 d.setPrescription("20/11/2556");
                 d.setExpiratePrescription("20/11/2559");
                 d.setLawsuitDetail("Shoplifting, Clinic for Exotic Animals");
                 d.setOwnerLawsuit("Surasak tona");
                 
             try {
                    d.persist();
                    fail("Should not birthDate");
        } catch(javax.validation.ConstraintViolationException e) {
              
            
        }
        org.junit.Assert.assertTrue(true);
    }

    // กรอก Nationality ไม่ถูกต้อง จะต้องกรอก a-z,A-Z จำนวนไม่เกิน 40 ตัวเท่านั้น
    @Test
    public void persistNationalityFalse() {
        DataOffense d = new DataOffense();
                 d.setFirstName("Somsai");
                 d.setLastName("Boonmee");
                 d.setIdentifyNumber("1234567890123");
                 d.setAge("50");
                 d.setBirthDate("13/04/1876");
                 d.setNationality("Thai+66");
                 d.setAddress("Suranaree Nakhonratchasima");
                 d.setLawsuitId("N5600011");
                 d.setLawsuitName("malefaction");
                 d.setPrescription("20/11/2556");
                 d.setExpiratePrescription("20/11/2559");
                 d.setLawsuitDetail("Shoplifting, Clinic for Exotic Animals");
                 d.setOwnerLawsuit("Surasak tona");
                 
             try {
                    d.persist();
                    fail("Should not Nationality");
        } catch(javax.validation.ConstraintViolationException e) {
              
            
        }
        org.junit.Assert.assertTrue(true);
    }

    // ไม่กรอก Nationality 
    @Test
    public void persistNationalityFalseValue() {
        DataOffense d = new DataOffense();
                 d.setFirstName("Somsai");
                 d.setLastName("Boonmee");
                 d.setIdentifyNumber("1234567890123");
                 d.setAge("50");
                 d.setBirthDate("13/04/1876");
                 d.setNationality("");
                 d.setAddress("Suranaree Nakhonratchasima");
                 d.setLawsuitId("N5600011");
                 d.setLawsuitName("malefaction");
                 d.setPrescription("20/11/2556");
                 d.setExpiratePrescription("20/11/2559");
                 d.setLawsuitDetail("Shoplifting, Clinic for Exotic Animals");
                 d.setOwnerLawsuit("Surasak tona");
                 
             try {
                    d.persist();
                    fail("Should not Nationality");
        } catch(javax.validation.ConstraintViolationException e) {
              
            
        }
        org.junit.Assert.assertTrue(true);
    }

    // กรอก LawsuitId ไม่ถูกต้อง โดยจะต้องกรอก N ขึ้นต้นแล้วตามด้วยเลข 7 ตัว
    @Test
    public void persistLawsuitIdFalse() {
        DataOffense d = new DataOffense();
                 d.setFirstName("Somsai");
                 d.setLastName("Boonmee");
                 d.setIdentifyNumber("1234567890123");
                 d.setAge("50");
                 d.setBirthDate("13/04/1876");
                 d.setNationality("Thai");
                 d.setAddress("Suranaree Nakhonratchasima");
                 d.setLawsuitId("Baaa1234");
                 d.setLawsuitName("malefaction");
                 d.setPrescription("20/11/2556");
                 d.setExpiratePrescription("20/11/2559");
                 d.setLawsuitDetail("Shoplifting, Clinic for Exotic Animals");
                 d.setOwnerLawsuit("Surasak tona");
                 
             try {
                    d.persist();
                    fail("Should not ILawsuitId");
        } catch(javax.validation.ConstraintViolationException e) {
              
            
        }
        org.junit.Assert.assertTrue(true);
    }

    // ไม่กรอก LawsuitId 
    @Test
    public void persistLawsuitIdFalseValue() {
        DataOffense d = new DataOffense();
                 d.setFirstName("Somsai");
                 d.setLastName("Boonmee");
                 d.setIdentifyNumber("1234567890123");
                 d.setAge("50");
                 d.setBirthDate("13/04/1876");
                 d.setNationality("Thai");
                 d.setAddress("Suranaree Nakhonratchasima");
                 d.setLawsuitId("");
                 d.setLawsuitName("malefaction");
                 d.setPrescription("20/11/2556");
                 d.setExpiratePrescription("20/11/2559");
                 d.setLawsuitDetail("Shoplifting, Clinic for Exotic Animals");
                 d.setOwnerLawsuit("Surasak tona");
                 
             try {
                    d.persist();
                    fail("Should not ILawsuitId");
        } catch(javax.validation.ConstraintViolationException e) {
              
            
        }
        org.junit.Assert.assertTrue(true);
    }

    // กรอก LawsuitName ไม่ถูกต้อง กรอกได้ a-z A-Z และช่องว่าง ไม่เกิน 50 ตัวเท่านั้น
    @Test
    public void persistLawsuitNameFalse() {
        DataOffense d = new DataOffense();
                 d.setFirstName("Somsai");
                 d.setLastName("Boonmee");
                 d.setIdentifyNumber("1234567890123");
                 d.setAge("50");
                 d.setBirthDate("13/04/1876");
                 d.setNationality("Thai");
                 d.setAddress("Suranaree Nakhonratchasima");
                 d.setLawsuitId("N5600011");
                 d.setLawsuitName("อาชญกรรม0999");
                 d.setPrescription("20/11/2556");
                 d.setExpiratePrescription("20/11/2559");
                 d.setLawsuitDetail("Shoplifting, Clinic for Exotic Animals");
                 d.setOwnerLawsuit("Surasak tona");  
                 
             try {
                    d.persist();
                    fail("Should not LawsuitName");
        } catch(javax.validation.ConstraintViolationException e) {
              
            
        }
        org.junit.Assert.assertTrue(true);
    }

    // ไม่กรอก LawsuitName 
    @Test
    public void persistLawsuitNameFalseValue() {
        DataOffense d = new DataOffense();
                 d.setFirstName("Somsai");
                 d.setLastName("Boonmee");
                 d.setIdentifyNumber("1234567890123");
                 d.setAge("50");
                 d.setBirthDate("13/04/1876");
                 d.setNationality("Thai");
                 d.setAddress("Suranaree Nakhonratchasima");
                 d.setLawsuitId("N5600011");
                 d.setLawsuitName("");
                 d.setPrescription("20/11/2556");
                 d.setExpiratePrescription("20/11/2559");
                 d.setLawsuitDetail("Shoplifting, Clinic for Exotic Animals");
                 d.setOwnerLawsuit("Surasak tona");  
                 
             try {
                    d.persist();
                    fail("Should not LawsuitName");
        } catch(javax.validation.ConstraintViolationException e) {
              
            
        }
        org.junit.Assert.assertTrue(true);
    }

    // ไม่กรอก Prescription
    @Test
    public void persistPrescriptionFalse() {
        DataOffense d = new DataOffense();
                 d.setFirstName("Somsai");
                 d.setLastName("Boonmee");
                 d.setIdentifyNumber("1234567890123");
                 d.setAge("50");
                 d.setBirthDate("13/04/1876");
                 d.setNationality("Thai");
                 d.setAddress("Suranaree Nakhonratchasima");
                 d.setLawsuitId("N5600011");
                 d.setLawsuitName("malefaction");
                 d.setPrescription("");
                 d.setExpiratePrescription("20/11/2559");
                 d.setLawsuitDetail("Shoplifting, Clinic for Exotic Animals");
                 d.setOwnerLawsuit("Surasak tona");  
                 
             try {
                    d.persist();
                    fail("Should not Prescription");
        } catch(javax.validation.ConstraintViolationException e) {
              
            
        }
        org.junit.Assert.assertTrue(true);
    }

    // ไม่กรอก ExpiratePrescription 
    @Test
    public void persistExpiratePrescriptionFalse() {
        DataOffense d = new DataOffense();
                 d.setFirstName("Somsai");
                 d.setLastName("Boonmee");
                 d.setIdentifyNumber("1234567890123");
                 d.setAge("50");
                 d.setBirthDate("13/04/1876");
                 d.setNationality("Thai");
                 d.setAddress("Suranaree Nakhonratchasima");
                 d.setLawsuitId("N5600011");
                 d.setLawsuitName("malefaction");
                 d.setPrescription("20/11/2556");
                 d.setExpiratePrescription("");
                 d.setLawsuitDetail("Shoplifting, Clinic for Exotic Animals");
                 d.setOwnerLawsuit("Surasak tona");  
                 
             try {
                    d.persist();
                    fail("Should not ExpiratePrescription");
        } catch(javax.validation.ConstraintViolationException e) {
              
            
        }
        org.junit.Assert.assertTrue(true);
    }

    // กรอก OwnerLawsuit ไม่ถูกต้อง โดยกรอกได้ a-z A-Z 0-9 ช่องว่าง แต่ห้ามมีอักขระ ใส่ได้ไม่เกิน 30 ตัว
    @Test
    public void persistOwnerLawsuitFalse() {
        DataOffense d = new DataOffense();
                 d.setFirstName("Somsai");
                 d.setLastName("Boonmee");
                 d.setIdentifyNumber("1234567890123");
                 d.setAge("50");
                 d.setBirthDate("13/04/1876");
                 d.setNationality("Thai");
                 d.setAddress("Suranaree Nakhonratchasima");
                 d.setLawsuitId("N5600011");
                 d.setLawsuitName("malefaction");
                 d.setPrescription("20/11/2556");
                 d.setExpiratePrescription("20/11/2559");
                 d.setLawsuitDetail("Shoplifting, Clinic for Exotic Animals");
                 d.setOwnerLawsuit("Surasak tona,Somsai,Thana");
                 
             try {
                    d.persist();
                    fail("Should not OwnerLawsuit");
        } catch(javax.validation.ConstraintViolationException e) {
              
            
        }
        org.junit.Assert.assertTrue(true);
    }

     // ไม่กรอก OwnerLawsuit 
    @Test
    public void persistOwnerLawsuitFalseValue() {
        DataOffense d = new DataOffense();
                 d.setFirstName("Somsai");
                 d.setLastName("Boonmee");
                 d.setIdentifyNumber("1234567890123");
                 d.setAge("50");
                 d.setBirthDate("13/04/1876");
                 d.setNationality("Thai");
                 d.setAddress("Suranaree Nakhonratchasima");
                 d.setLawsuitId("N5600011");
                 d.setLawsuitName("malefaction");
                 d.setPrescription("20/11/2556");
                 d.setExpiratePrescription("20/11/2559");
                 d.setLawsuitDetail("Shoplifting, Clinic for Exotic Animals");
                 d.setOwnerLawsuit("");
                 
             try {
                    d.persist();
                    fail("Should not OwnerLawsuit");
        } catch(javax.validation.ConstraintViolationException e) {
              
            
        }
        org.junit.Assert.assertTrue(true);
    }
    
}
