package sut.se.immigration.entity;

import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.junit.runner.RunWith;
import sut.se.immigration.entity.Workers;
import javax.persistence.TypedQuery;
 
import static org.junit.Assert.*;
import org.junit.Test;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:/META-INF/spring/applicationContext*.xml")

public class WorkersTest {

    // //ข้อกำหนด
    // setRegister_position         รับได้ 13 ตัว            ห้ามไว้ว่าง
    // setRegister_number           รับได้ 13 ตัว            ห้ามไว้ว่าง
    // setRegister_name             รับได้แต่ภาษาอังกฤษ      ห้ามไว้ว่าง
    // setRegister_Mname            รับได้แต่ภาษาอังกฤษ      ห้ามไว้ว่าง
    // setRegister_sex              ไม่มีข้อกำหนด          ห้ามไว้ว่าง
    // setRegister_chad             รับได้แต่ภาษาอังกฤษ      ห้ามไว้ว่าง
    // setRegister_relation         รับได้แต่ภาษาอังกฤษ      ห้ามไว้ว่าง


    private Workers workers = new Workers();

    @Test
    public void Test_true() {
        Workers t = new Workers();
        t.setRegister_position("1111111111111");
        t.setCheckin("26/11/2013");
        t.setRegister_number("1111111111111");
        t.setRegister_name("Hicamaru kkkk");
        t.setRegister_Mname("popdk");
        t.setRegister_sex("Male");
        t.setRegister_chad("thai");
        t.setRegister_relation("ooooo");
            t.persist();
            org.junit.Assert.assertTrue(true);
    }

    @Test
    public void Test_setRegister_position_Number_False() {
        Workers t = new Workers();
        t.setRegister_position("11211111111114"); //ใส่ตัวไป 14 ตัว
        t.setCheckin("26/11/2013");
        t.setRegister_number("1111111111111");
        t.setRegister_name("Hicamaru kkkk");
        t.setRegister_Mname("popdk");
        t.setRegister_sex("Male");
        t.setRegister_chad("thai");
        t.setRegister_relation("ooooo");

        try{
             t.persist();
             fail("Should not pass");
              org.junit.Assert.assertTrue(true);
        }
        catch(javax.validation.ConstraintViolationException e) {
            
          
        }
    }

    @Test
    public void Test_setRegister_position_Notnull_False() {
        Workers t = new Workers();
        t.setRegister_position(null); //ใส่เป็น notnull
        t.setCheckin("26/11/2013");
        t.setRegister_number("1111111111111");
        t.setRegister_name("Hicamaru kkkk");
        t.setRegister_Mname("popdk");
        t.setRegister_sex("Male");
        t.setRegister_chad("thai");
        t.setRegister_relation("ooooo");

        try{
             t.persist();
             fail("Should not pass");
              org.junit.Assert.assertTrue(true);
        }
        catch(javax.validation.ConstraintViolationException e) {
            
          
        }
    }


    @Test
    public void Test_setRegister_position_Character_False() {
        Workers t = new Workers();
        t.setRegister_position("abcdeffffasdc"); //ใส่ตัวอักษรไป 13 ตัว
        t.setCheckin("26/11/2013");
        t.setRegister_number("1111111111111");
        t.setRegister_name("Hicamaru kkkk");
        t.setRegister_Mname("popdk");
        t.setRegister_sex("Male");
        t.setRegister_chad("thai");
        t.setRegister_relation("ooooo");

        try{
             t.persist();
             fail("Should not pass");
              org.junit.Assert.assertTrue(true);
        }
        catch(javax.validation.ConstraintViolationException e) {
            
          
        }
    }

    @Test
    public void Test_setRegister_position_Character_ANd_Number_False() {
        Workers t = new Workers();
        t.setRegister_position("abcde32asdc44"); //ใส่ตัวอักษร ผสมตัวเลข 13 หลัก
        t.setCheckin("26/11/2013");
        t.setRegister_number("1111111111111");
        t.setRegister_name("Tanwad Kanrai");
        t.setRegister_Mname("popdk");
        t.setRegister_sex("Male");
        t.setRegister_chad("thai");
        t.setRegister_relation("ooooo");

        try{
             t.persist();
             fail("Should not pass");
              org.junit.Assert.assertTrue(true);
        }
        catch(javax.validation.ConstraintViolationException e) {
            
          
        }
    }



    @Test
    public void Test_setCheckin_Char_Number_False() {
        Workers t = new Workers();
        t.setRegister_position("1111111111111");
        t.setCheckin("d26/a11/tt2013");   //ใส่ตัวเลขผสมตัวอักษร
        t.setRegister_number("1111111111111");
        t.setRegister_name("Tanwad Kanrai");
        t.setRegister_Mname("popdk");
        t.setRegister_sex("Male");
        t.setRegister_chad("thai");
        t.setRegister_relation("ooooo");

        try{
             t.persist();
             fail("Should not pass");
        }catch(javax.validation.ConstraintViolationException e) {
            
           
        }
    }

    @Test
    public void Test_setCheckin_Number_False() {
        Workers t = new Workers();
        t.setRegister_position("1111111111111");
        t.setCheckin("3326/12311/342342013"); //ใส่ตัวเลขเกิน
        t.setRegister_number("1111111111111"); 
        t.setRegister_name("Tanwad Kanrai");
        t.setRegister_Mname("popdk");
        t.setRegister_sex("Male");
        t.setRegister_chad("thai");
        t.setRegister_relation("ooooo");

        try{
             t.persist();
             fail("Should not pass");
        }catch(javax.validation.ConstraintViolationException e) {
                     
        }
    }



    @Test
    public void Test_setRegister_number_False() {
        Workers t = new Workers();
        t.setRegister_position("1111111111111");
        t.setCheckin("26/11/2013");
        t.setRegister_number("11111111111114"); //ใส่เป็นตัวเลขเกิน 13 หลัก
        t.setRegister_name("Tanwad Kanrai");
        t.setRegister_Mname("popdk");
        t.setRegister_sex("Male");
        t.setRegister_chad("thai");
        t.setRegister_relation("ooooo");

        try{
             t.persist();
             fail("Should not pass");
        }catch(javax.validation.ConstraintViolationException e) {
                     
        }
    }

    @Test
    public void Test_setRegister_number_Character_False() {
        Workers t = new Workers();
        t.setRegister_position("1111111111111");
        t.setCheckin("26/11/2013");
        t.setRegister_number("kdfuafkasdfi"); //ใส่เป็นตัวอักษร
        t.setRegister_name("Tanwad Kanrai");
        t.setRegister_Mname("popdk");
        t.setRegister_sex("Male");
        t.setRegister_chad("thai");
        t.setRegister_relation("ooooo");

        try{
             t.persist();
             fail("Should not pass");
        }catch(javax.validation.ConstraintViolationException e) {
                     
        }
    }

    @Test
    public void Test_setRegister_number_NUll_False() {
        Workers t = new Workers();
        t.setRegister_position("1111111111111");
        t.setCheckin("26/11/2013");
        t.setRegister_number(null); //ใส่เป็นค่าว่างๆ
        t.setRegister_name("Tanwad Kanrai");
        t.setRegister_Mname("popdk");
        t.setRegister_sex("Male");
        t.setRegister_chad("thai");
        t.setRegister_relation("ooooo");

        try{
             t.persist();
             fail("Should not pass");
        }catch(javax.validation.ConstraintViolationException e) {
                     
        }
    }

     @Test
    public void Test_setRegister_number_NUll_TOW_False() {
        try{
        
        Workers t = new Workers();
        t.setRegister_position("1111111111111");
        t.setCheckin("26/11/2013");
        t.setRegister_number("1111111111111"); //ใส่ให้ซ้ำกัน
        t.setRegister_name("Tanwad Kanrai");
        t.setRegister_Mname("popdk");
        t.setRegister_sex("Male");
        t.setRegister_chad("thai");
        t.setRegister_relation("ooooo");
        t.persist();

        Workers t1 = new Workers();
        t1.setRegister_position("88888881888888");
        t1.setCheckin("26/11/2013");
        t1.setRegister_number("1111111111111"); //ใส่ให้ซ้ำกัน
        t1.setRegister_name("Tanwad Kanrai");
        t1.setRegister_Mname("popdk");
        t1.setRegister_sex("Male");
        t1.setRegister_chad("thai");
        t1.setRegister_relation("ooooo");
        t1.persist();

        org.junit.Assert.assertTrue(true);
        }catch(javax.validation.ConstraintViolationException e1) {
                 
        }
    }



    @Test
    public void Test_setRegister_name_Number_False() {
        Workers t = new Workers();
        t.setRegister_position("1111111111111");
        t.setCheckin("26/11/2013");
        t.setRegister_number("1111111111111"); 
        t.setRegister_name("127624784234"); //ใส่เป็นตัวเลข
        t.setRegister_Mname("popdk");
        t.setRegister_sex("Male");
        t.setRegister_chad("thai");
        t.setRegister_relation("ooooo");

        try{
             t.persist();
             fail("Should not pass");
        }catch(javax.validation.ConstraintViolationException e) {
                     
        }
    }


    @Test
    public void Test_setRegister_name_False() {
        Workers t = new Workers();
        t.setRegister_position("1111111111111");
        t.setCheckin("26/11/2013");
        t.setRegister_number("1111111111111"); 
        t.setRegister_name("Tan12312awad"); //ใส่เป็นตัวเลข
        t.setRegister_Mname("popdk");
        t.setRegister_sex("Male");
        t.setRegister_chad("thai");
        t.setRegister_relation("ooooo");

        try{
             t.persist();
             fail("Should not pass");
        }catch(javax.validation.ConstraintViolationException e) {
                     
        }
    }



    @Test
    public void Test_setRegister_Mname_Number_False() {
        Workers t = new Workers();
        t.setRegister_position("1111111111111");
        t.setCheckin("26/11/2013");
        t.setRegister_number("1111111111111"); 
        t.setRegister_name("Tanawad"); 
        t.setRegister_Mname("1234567"); //ใส่เป็นตัวเลข
        t.setRegister_sex("Male");
        t.setRegister_chad("thai");
        t.setRegister_relation("ooooo");

        try{
             t.persist();
             fail("Should not pass");
        }catch(javax.validation.ConstraintViolationException e) {
                     
        }
    }
    

    @Test
    public void Test_setRegister_chad_False() {
        Workers t = new Workers();
        t.setRegister_position("1111111111111");
        t.setCheckin("26/11/2013");
        t.setRegister_number("1111111111111"); 
        t.setRegister_name("Tanawad"); 
        t.setRegister_Mname("1234567"); 
        t.setRegister_sex("Male");
        t.setRegister_chad("123123123"); //ใส่เป็นตัวเลข
        t.setRegister_relation("ooooo");

        try{
             t.persist();
             fail("Should not pass");
        }catch(javax.validation.ConstraintViolationException e) {
                     
        }
    }


    @Test
    public void Test_setRegister_relation_False() {
        Workers t = new Workers();
        t.setRegister_position("1111111111111");
        t.setCheckin("26/11/2013");
        t.setRegister_number("1111111111111"); 
        t.setRegister_name("Tanawad"); 
        t.setRegister_Mname("1234567"); 
        t.setRegister_sex("Male");
        t.setRegister_chad("thai"); 
        t.setRegister_relation("abcdeabcdeabcdeabcdeabcdeabcdeabcde"); //ใส่เป็นตัวอักษรเกิน 30 ตัว

        try{
             t.persist();
             fail("Should not pass");
        }catch(javax.validation.ConstraintViolationException e) {
                     
        }
    }


    @Test
    public void Test_setRegister_relation1_False() {
        Workers t = new Workers();
        t.setRegister_position("1111111111111");
        t.setCheckin("26/11/2013");
        t.setRegister_number("1111111111111"); 
        t.setRegister_name("Tanawad"); 
        t.setRegister_Mname("1234567"); 
        t.setRegister_sex("Male");
        t.setRegister_chad("thai"); 
        t.setRegister_relation("12312312"); //ใส่เป็นตัวเลข

        try{
             t.persist();
             fail("Should not pass");
        }catch(javax.validation.ConstraintViolationException e) {
                     
        }
    }


}
