package sut.se.immigration.web;

import org.zkoss.zk.roo.annotations.ZKComposer;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zul.Doublebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.*;
import org.zkoss.zk.ui.event.*;
import static org.zkoss.zk.ui.select.Selectors.find;
import org.zkoss.zul.*;
import sut.se.immigration.entity.BorderpassData;
import java.lang.*;
import java.io.*;
import java.util.*;
import javax.persistence.*;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.lang.Exception;
import java.lang.Object;
import org.zkoss.zul.Listbox;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import javax.persistence.NoResultException;

import java.lang.NullPointerException;

@ZKComposer(zul = "BorderpassSearch.zul")
public class BorderpassSearchComposer {

    private static final long serialVersionUID = -1415602477221157958L;

    public void afterCompose(Component comp) {
    }

    @Listen("onClick = #btnSearch")
    public void btnSearch_Clicked(Event event) {

    try{  

      TypedQuery<BorderpassData> bd = BorderpassData.findBorderpassDatasByIdpersonal(txtsearch.getValue());
               BorderpassData ds = bd.getSingleResult(); 
               alert("พบข้อมูลผู้ถือหนังสือผ่านแดน ID  " + " " +ds.getIdpersonal()); 

                     Row row = new Row();
                       Label lb1 = new Label();
                       Label lb2 = new Label();
                       Label lb3 = new Label();  
                       Label lb4 = new Label();
                       Label lb5 = new Label();
                       Label lb6 = new Label();
                       Label lb7 = new Label();
                       Label lb8 = new Label();
                       

                       lb1.setValue(ds.getIdpersonal());
                       row.appendChild(lb1);
                       lb2.setValue(ds.getName());
                       row.appendChild(lb2);
                       lb3.setValue(ds.getLastname());
                       row.appendChild(lb3);
                       lb4.setValue(ds.getOccupation());
                       row.appendChild(lb4);
                       lb5.setValue(ds.getNationality());
                       row.appendChild(lb5);
                       lb6.setValue(ds.getDatebirth());
                       row.appendChild(lb6);
                       lb7.setValue(ds.getDomicile());
                       row.appendChild(lb7);
                       lb8.setValue(ds.getSex());
                       row.appendChild(lb8);
                       r111.appendChild(row);

        }
      catch(Exception e){
                alert("ไม่พบ ID ที่องการค้นหา กรุณากรอกข้อมูลใหม่");
            }
        
       

    }
    @Listen("onClick = #btnReset3")
    public void btnReset3_Clicked(Event event) {
      btnEdit.setVisible(true);
      btnSave.setVisible(true);
      btnDelete.setVisible(true);

}

@Listen("onClick = #btnEdit")
    public void btnEdit_Clicked(Event event) {
                   
                   txtsearch11.setVisible(true);
                   txtsearch2.setVisible(true);
                   txtsearch3.setVisible(true);
                   txtsearch4.setVisible(true);
                   txtsearch5.setVisible(true);
                   txtsearch6.setVisible(true);
                   txtsearch7.setVisible(true);
                   txtsearch8.setVisible(true);

      TypedQuery<BorderpassData> bd = BorderpassData.findBorderpassDatasByIdpersonal(txtsearch.getValue());
               BorderpassData ds = bd.getSingleResult(); 

                       txtsearch11.setValue(ds.getIdpersonal());
                       txtsearch2.setValue(ds.getName());
                       txtsearch3.setValue(ds.getLastname());
                       txtsearch4.setValue(ds.getOccupation());
                       txtsearch5.setValue(ds.getNationality());
                       txtsearch6.setValue(ds.getDatebirth());
                       txtsearch7.setValue(ds.getDomicile());
                       txtsearch8.setValue(ds.getSex());
                       
                      



}

@Listen("onClick = #btnSave")
    public void btnSave_Clicked(Event event) {

      
       TypedQuery<BorderpassData> bd = BorderpassData.findBorderpassDatasByIdpersonal(txtsearch.getValue());
               BorderpassData ds = bd.getSingleResult(); 

                       ds.setIdpersonal(txtsearch11.getValue());
                       ds.setName(txtsearch2.getValue());
                       ds.setLastname(txtsearch3.getValue());
                       ds.setOccupation(txtsearch4.getValue());
                       ds.setNationality(txtsearch5.getValue());
                       ds.setDatebirth(txtsearch6.getValue());
                       ds.setDomicile(txtsearch7.getValue());
                       ds.setSex(txtsearch8.getValue());

        try{
                ds.persist();
                alert("บันทึกข้อมูลเรียบร้อยแล้ว");


        }catch(Exception e){
                alert("ไม่สามารถบันทึกข้อมูลได้");
            }

                   txtsearch11.setVisible(false);
                   txtsearch2.setVisible(false);
                   txtsearch3.setVisible(false);
                   txtsearch4.setVisible(false);
                   txtsearch5.setVisible(false);
                   txtsearch6.setVisible(false);
                   txtsearch7.setVisible(false);
                   txtsearch8.setVisible(false); 
}

  @Listen("onClick = #btnDelete")
    public void btnDelete_Clicked(Event event) {
      try{
       TypedQuery<BorderpassData> bd = BorderpassData.findBorderpassDatasByIdpersonal(txtsearch.getValue());
               BorderpassData ds = bd.getSingleResult(); 
                 alert("ลบข้อมูลของหมายเลข " + " " + ds.getIdpersonal() + " " + "เรียบร้อยแล้ว"); 
                    ds.remove();

        }catch(Exception e){
                alert("ข้อมูลถูกลบไปแล้วไม่สามารถลบข้อมูลได้");
          } 

                   txtsearch11.setVisible(false);
                   txtsearch2.setVisible(false);
                   txtsearch3.setVisible(false);
                   txtsearch4.setVisible(false);
                   txtsearch5.setVisible(false);
                   txtsearch6.setVisible(false);
                   txtsearch7.setVisible(false);
                   txtsearch8.setVisible(false);    
}    

  @Listen("onClick = #tbSave")
    public void tbSave_Clicked(Event event) {
       Executions.sendRedirect("LoginBorderpass.zul");
}
  @Listen("onClick = #tbEdit")
    public void tbEdit_Clicked(Event event) {
       Executions.sendRedirect("BorderpassSearch.zul");
}



}
