package sut.se.immigration.web;

import java.*;
import java.lang.Exception;
import java.lang.Object;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import javax.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;
import org.zkoss.*;
import org.zkoss.zk.roo.annotations.ZKComposer;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import static org.zkoss.zk.ui.select.Selectors.find;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zul.Doublebox;
import org.zkoss.zul.Listbox;
import sut.se.immigration.entity.Notify;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

@ZKComposer(zul = "notity.zul")
public class NotityComposer {

    private static final long serialVersionUID = 1003247123129309956L;

    public void afterCompose(Component comp) {
    }


    @Listen("onClick = #ddmmyystart")
    public void btnA1dd_Clicked(Event event) {
    		

    }

    @Listen("onClick = #btnAdd")
    public void btnAdd_Clicked(Event event) {
        Notify d = new Notify();

        DateFormat datesave = new SimpleDateFormat("dd/MM/yyyy");
        d.setDdmmyystart(datesave.format(ddmmyystart.getValue()));
        d.setDdmmyyent(datesave.format(ddmmyyent.getValue()));
        d.setName(name.getValue());
        d.setNationality(nationality.getValue());
        d.setVisa(visa.getValue());
        d.setIdpassport(idpassport.getValue());
        d.setIdarrival(idarrival.getValue());
        d.setAddress(address.getValue());
       
        d.setPhone(phone.getValue());
       
        d.setNameofficer(nameofficer.getValue());



        try {
            d.persist();
            
            alert("บันทึกเรียบร้อย" );
             name.setValue("");
        nationality.setValue("");
        visa.setValue("");
         idpassport.setValue("");
        idarrival.setValue("");
        address.setValue("");
        phone.setValue("");
        nameofficer.setValue("");
        } catch (Exception e) {
            alert("ไม่สามารถบันทึกข้อมูลได้");
        }
    }

    @Listen("onClick = #btnExit")
    public void btnExit_Clicked(Event event) {
    	 Notify d = new Notify();
        DateFormat datesave = new SimpleDateFormat("dd/MM/yyyy");
        d.setDdmmyystart(datesave.format(ddmmyystart.getValue()));
        d.setDdmmyyent(datesave.format(ddmmyyent.getValue()));
        d.setName(name.getValue());
        d.setNationality(nationality.getValue());
        d.setVisa(visa.getValue());
        d.setIdpassport(idpassport.getValue());
        d.setIdarrival(idarrival.getValue());
        d.setAddress(address.getValue());
        d.setPhone(phone.getValue());
        d.setNameofficer(nameofficer.getValue());
    	
        name.setValue("");
         nationality.setValue("");
         visa.setValue("");
                
          idpassport.setValue("");
         idarrival.setValue("");
        address.setValue("");
         phone.setValue("");
         nameofficer.setValue("");
          
            }
    
}
