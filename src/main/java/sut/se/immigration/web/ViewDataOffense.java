package sut.se.immigration.web;

import org.zkoss.zk.roo.annotations.ZKComposer;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zul.Doublebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.*;
import org.zkoss.zk.ui.event.*;
import static org.zkoss.zk.ui.select.Selectors.find;
import org.zkoss.zul.*;
import sut.se.immigration.entity.DataOffense;
import java.lang.*;
import java.io.*;
import java.util.*;
import javax.persistence.*;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.lang.Exception;
import java.lang.Object;
import org.zkoss.zul.Listbox;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import sut.se.immigration.entity.Personnal;
import sut.se.immigration.entity.AdminPersonnel;
import org.zkoss.zul.Window;
import org.zkoss.zk.ui.select.annotation.Wire;

@ZKComposer(zul = "ViewDataOffense.zul")
public class ViewDataOffense {

    private static final long serialVersionUID = -1805867571136800996L;
   

    public void afterCompose(Component comp) {

      Session sessidentifyNumber =  Sessions.getCurrent();
          String id = String.valueOf(sessidentifyNumber.getAttribute("identifyNumberId"));
          txtOutput3.setValue(id); 

          
      TypedQuery<DataOffense> cki = DataOffense.findDataOffensesByIdentifyNumber(txtOutput3.getValue());
                DataOffense checki = cki.getSingleResult(); 

                       txtOutput1.setValue(checki.getFirstName());
                       txtOutput2.setValue(checki.getLastName());
                       txtOutput4.setValue(checki.getAge());
                       txtOutput5.setValue(checki.getBirthDate());
                       txtOutput6.setValue(checki.getNationality());
                       txtOutput7.setValue(checki.getAddress());
                       txtOutput8.setValue(checki.getLawsuitId());
                       txtOutput9.setValue(checki.getLawsuitName());
                       txtOutput11.setValue(checki.getPrescription());
                       txtOutput12.setValue(checki.getExpiratePrescription());
                       txtOutput13.setValue(checki.getLawsuitDetail());
                       txtOutput14.setValue(checki.getOwnerLawsuit());
        
                       
    }
    @Listen("onClick = #btnYes")
    public void btnYes_Clicked(Event event) {
        
          TypedQuery<DataOffense> cki = DataOffense.findDataOffensesByIdentifyNumber(txtOutput3.getValue());
                DataOffense checki = cki.getSingleResult(); 
        
        checki.setResultDataOffense("Audited");
        alert("ผ่านการตรวจสอบข้อมูล");
        checki.persist();
        wnd.detach();
      
    
  }

  @Listen("onClick = #btnNo")
    public void btnNo_Clicked(Event event) {
        
          TypedQuery<DataOffense> cki = DataOffense.findDataOffensesByIdentifyNumber(txtOutput3.getValue());
                DataOffense checki = cki.getSingleResult(); 
        
        checki.setResultDataOffense("Not Audited");
        alert("ไม่ผ่านการตรวจสอบข้อมูล" + " " + checki.getFirstName() + " " + checki.getLastName() + " " + "มีประวัติการกระทำความผิด");
        checki.persist();
        wnd.detach();
     
    
  }
}
