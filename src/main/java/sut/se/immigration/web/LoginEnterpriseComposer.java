package sut.se.immigration.web;

import org.zkoss.zk.roo.annotations.ZKComposer;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import static org.zkoss.zk.ui.select.Selectors.find;
import org.zkoss.zul.*;
import java.util.HashMap;


import org.zkoss.zul.Window;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;


import org.zkoss.zk.ui.Executions;


import org.zkoss.zk.ui.util.Clients;


import java.util.Map;
import java.lang.Exception;
import static org.zkoss.zk.ui.select.Selectors.find;
import org.zkoss.zul.*;
import org.zkoss.zul.Window;
import org.zkoss.zk.ui.Executions;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import org.zkoss.zk.ui.Sessions;
import java.net.InetAddress;

import sut.se.immigration.entity.RegisterEnterprise;
import sut.se.immigration.entity.LoginEnterprise;
import sut.se.immigration.entity.IpAddress;


@ZKComposer(zul = "LoginEnterpriseIndex.zul")
public class LoginEnterpriseComposer {

    private static final long serialVersionUID = 698644667166466592L;

   public void afterCompose(Component comp) {

      String yourIP="";    
      try {
          InetAddress addr = InetAddress.getLocalHost(); 
          yourIP=addr.getHostAddress();
          
        } 
      catch (Exception e3) {
      }


           
           try{   
            TypedQuery<IpAddress> aa1 = IpAddress.findIpAddressesByIpEquals(yourIP);
            IpAddress ab1 = aa1.getSingleResult();
  
              Executions.sendRedirect("ipaddress.zul");
             

            }
            catch(Exception e){

            }
            
        







    }

   
    @Listen("onClick = #btn_register")
    public void Register_Clicked(Event event) {

      Window window = (Window)Executions.createComponents(
                  "RegisterEnterpriseIndex.zul", null, null);
         window.doModal();

    }

    @Listen("onClick = #userlogin")
    public void userlogin_Clicked(Event event) {
      Textbox userlogin = (Textbox)find(getPage(),"#userlogin").get(0);
       userlogin.setValue("");
    }



    @Listen("onClick = #userpass")
    public void userpass_Clicked(Event event) {
     Textbox userpass = (Textbox)find(getPage(),"#userpass").get(0);
      userpass.setValue("");

    }



    @Listen("onClick = #btn_login")
    public void btn_login_Clicked(Event event) {
          Textbox userlogin = (Textbox)find(getPage(),"#userlogin").get(0);
          Textbox userpass = (Textbox)find(getPage(),"#userpass").get(0);



          Map<String, Object> arguments = new HashMap<String, Object>(); 
          arguments.put("value1", userlogin.getText() );
          Sessions.getCurrent().setAttribute("allmyvalues", arguments);

           















          
          String x="true",y="true",o="1";
          

          try{

            TypedQuery<RegisterEnterprise> aa1 = RegisterEnterprise.findRegisterEnterprisesByRegister_useridEquals(userlogin.getValue());
            RegisterEnterprise ab1 = aa1.getSingleResult();
            
            
        
          }catch(Exception e){
              x="false";
              
          }


          try{
            TypedQuery<RegisterEnterprise> aa2 = RegisterEnterprise.findRegisterEnterprisesByRegister_passwordEquals(userpass.getValue());
            RegisterEnterprise ab2 = aa2.getSingleResult();
           
                    
               
          }catch(Exception e){
              y="false";
              //เพิ่มข้อมูลจำนวนครั้งในการ Login พิดพลาด
              if(x=="true"){
                  TypedQuery<RegisterEnterprise> n = RegisterEnterprise.findRegisterEnterprisesByRegister_useridEquals(userlogin.getValue());
                  RegisterEnterprise n1 = n.getSingleResult();

                  int login_error = Integer.parseInt(n1.getStateLogin());
                  login_error++;
                  String str_login=Integer.toString(login_error);
                  if(login_error<=4)
                  alert("กรุณาตรวจสอบ Password ของคุณอีกครั้ง\n จำนวน Login ผิดพลาดครั้งที่ "+str_login+" ครั้ง"+" จาก 5 ครั้ง");

                  n1.setStateLogin(str_login);
                  n1.persist();

                  //ตรวจสอบจำนวนการ login ผิด 5 ครั้ง
                  if(login_error==5){
                     alert("จำนวนครั้งในการ Login ของคุณครบ 5 ครั้งแล้ว \nกรุณาติดต่อผู้ดูแลระบบ\nหากคุณพยายามทำการ Login เราจะทำการ Block IP Address ของคุณ");
                  }

                  if(login_error>=6){

                                try {
                                    InetAddress addr = InetAddress.getLocalHost(); 
                                    IpAddress ip = new IpAddress();
                                    ip.setIp(addr.getHostAddress());
                                    ip.persist();
                                    Executions.sendRedirect("ipaddress.zul");
                                } 
                                catch (Exception e3) {
                                }
                  }

              }
              
          }


          if(x=="true"&&y=="true"){


            TypedQuery<RegisterEnterprise> ss = RegisterEnterprise.findRegisterEnterprisesByRegister_useridEquals(userlogin.getValue());
                  RegisterEnterprise ss1 = ss.getSingleResult();

                  int login_error1 = Integer.parseInt(ss1.getStateLogin());
                
                  if(login_error1<=4){
                        ss1.setStateLogin("0");
                        ss1.persist();
                        Executions.sendRedirect("enterpriseindex.zul");
                  }
                  if(login_error1>=5){
                       alert("Account นี้ ถูกระงับ การใช้ กรุณาติดต่อผู้ดูแลระบบ");
                  }


           
          }

          else if(x=="false"&&y=="true"){
             
              alert("ไม่พบ Username ที่คุณระบุ กรุณาตรวจสอบอีกครั้ง");
          }
          
          else if(x=="false"&&y=="false"){
               alert("Username และ Password ของคุณไม่ถูกต้อง กรุณาตรวจสอบใหม่อีกครับ");
          }
          



    }

}

