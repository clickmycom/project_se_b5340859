package sut.se.immigration.web;

import org.zkoss.zk.roo.annotations.ZKComposer;
import org.zkoss.zk.ui.Component;

import org.zkoss.*;

import org.zkoss.zk.ui.event.Event;

import org.zkoss.zk.ui.select.annotation.Listen;
import static org.zkoss.zk.ui.select.Selectors.find;
import org.zkoss.zul.*;
import javax.persistence.TypedQuery;
import org.zkoss.zk.ui.Executions;

import sut.se.immigration.entity.Workers;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import org.zkoss.zk.ui.util.Clients;
import java.lang.String;
import org.zkoss.lang.Threads;

@ZKComposer(zul = "RegisterRelate.zul")
public class RegisterRelateComposer {

    private static final long serialVersionUID = 4587855985538498506L;

    public void afterCompose(Component comp) {
        
    }


    @Listen("onClick = #btn_submit") 
    public void btn_submit_Clicked(Event event) {

            Workers t = new Workers();
            
            DateFormat datesave = new SimpleDateFormat("dd/MM/yyyy");
            String xx00=datesave.format(db.getValue()).substring(0,6);
           

            String xx01=datesave.format(db.getValue()).substring(6,10);
           

            int xx0001=Integer.parseInt(xx01);
            xx0001+=543;

            String xxx001= Integer.toString(xx0001);

            String xx02=xx00+xxx001;
            

            t.setCheckin(xx02);
            
            t.setRegister_position(register_position.getValue());
            t.setRegister_number(register_number.getValue());
            t.setRegister_name(register_name.getText());
            t.setRegister_Mname(register_Mname.getText());

            if(register_sex.getSelectedIndex() == 0){
                    t.setRegister_sex("Male");
            }
            if(register_sex.getSelectedIndex() == 1){
                    t.setRegister_sex("Female");
            }

             t.setRegister_chad(register_chad.getText());
             t.setRegister_relation(register_relation.getText());

            try{
                int row=0;
                for(Workers e:  Workers.findAllWorkerses()){
                    row++;
                }

                if(row>0){
                String pass="true";
                for(Workers e:  Workers.findAllWorkerses()){

                    if(e.getRegister_position().equals(register_position.getValue())){
                        pass="false";
                        Clients.showNotification("หมายเลข บัตรขาเข้าเลขที่ ซ้ำซ้อน","error",register_position,null,3000);
                    }
                    
                    if(e.getRegister_number().equals(register_number.getValue())){
                        int j=0;
                        pass="false";
                        Clients.showNotification("หมายเลข หนังสือเดินทาง ซ้ำซ้อน","error",register_number,null,1700);
                    }
                }

                    if(pass=="true"){
                            Clients.showNotification("บันทึกข้อมูลเรียบร้อย",null,null,null,3000);
                            t.persist();
                            register_position.setValue("");
                            register_number.setValue("");
                            register_name.setText("");
                            register_Mname.setText("");
                            register_chad.setText("");
                            register_relation.setText("");
                    }

                
                }
                if(row==0){
                    Clients.showNotification("บันทึกข้อมูลเรียบร้อย",null,null,null,3000);
                    t.persist();
                
                    register_position.setValue("");
                register_number.setValue("");
                register_name.setText("");
                register_Mname.setText("");
                register_chad.setText("");
                register_relation.setText("");
                }

                //เคลียร์ช่อง textbox
                
                

            }
            catch(Exception e){
                Clients.showNotification("ไม่สามารถบันทึกข้อมูลได้กรุณาตรวจสอบข้อมูล","error",null,null,3000);
            }

    }


    @Listen("onClick = #register_position") 
    public void register_position_Clicked(Event event) {
            Clients.showNotification("ป้อนข้อมูล เฉพาะตัวเลข 13 หลักเท่านั้น", "info",register_position, null, 2100);
    }

    @Listen("onChange = #register_position") 
    public void register_position1_Clicked(Event event) {
             String x=register_position.getText();
             int a=x.length();
            if(a<13){
                Clients.showNotification("ข้อมูลไม่ครบ 13 หลัก","error",register_position, null, 1700);
            }
            
    }

      @Listen("onChange = #register_number") 
    public void register_number2_Clicked(Event event) {
             String x=register_number.getText();
             int a=x.length();
            if(a<13){
                Clients.showNotification("ข้อมูลไม่ครบ 13 หลัก","error",register_number, null, 1700);
            }
            
    }


    @Listen("onClick = #register_number") 
    public void setRegister_number_Clicked(Event event) {
            Clients.showNotification("ป้อนข้อมูล เฉพาะตัวเลข 13 หลักเท่านั้น", "info",register_number, null, 2100);
    }

    @Listen("onClick = #register_name") 
    public void register_name_Clicked(Event event) {
            Clients.showNotification("ป้อนข้อมูล เฉพาะภาษาอังกฤษเท่านั้น", "info",register_name, null, 2100);
    }

    @Listen("onClick = #register_Mname") 
    public void register_Mname_Clicked(Event event) {
            Clients.showNotification("ป้อนข้อมูล เฉพาะภาษาอังกฤษเท่านั้น", "info",register_Mname, null, 2100);
    }

    @Listen("onClick = #register_chad") 
    public void register_chad_Clicked(Event event) {
            Clients.showNotification("ป้อนข้อมูล เฉพาะภาษาอังกฤษเท่านั้น", "info",register_chad, null, 2100);
    }

    @Listen("onClick = #register_relation") 
    public void register_relation_Clicked(Event event) {
            Clients.showNotification("ป้อนข้อมูล เฉพาะภาษาอังกฤษเท่านั้น", "info",register_relation, null, 2100);
    }





}
