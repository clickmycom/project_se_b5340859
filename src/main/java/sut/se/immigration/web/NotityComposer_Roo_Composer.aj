// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package sut.se.immigration.web;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Calendar;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Image;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Toolbarbutton;
import org.zkoss.zul.Window;
import sut.se.immigration.web.NotityComposer;
import zk.roo.Composer;

privileged aspect NotityComposer_Roo_Composer {
    
    declare parents: NotityComposer extends Composer;
    
    declare @type: NotityComposer: @Component;
    
    declare @type: NotityComposer: @Scope("prototype");
    
    @Wire
    public Window NotityComposer.wndMain;
    
    @Wire
    public Image NotityComposer.image;
    
    @Wire
    public Toolbarbutton NotityComposer.register;
    
    @Wire
    public Toolbarbutton NotityComposer.regiser;
    
    @Wire
    public Toolbarbutton NotityComposer.registr;
    
    @Wire
    public Toolbarbutton NotityComposer.regstr;
    
    @Wire
    public Calendar NotityComposer.cal;
    
    @Wire
    public Image NotityComposer.ime;
    
    @Wire
    public Datebox NotityComposer.ddmmyystart;
    
    @Wire
    public Datebox NotityComposer.ddmmyyent;
    
    @Wire
    public Textbox NotityComposer.name;
    
    @Wire
    public Textbox NotityComposer.nationality;
    
    @Wire
    public Textbox NotityComposer.visa;
    
    @Wire
    public Textbox NotityComposer.idpassport;
    
    @Wire
    public Textbox NotityComposer.idarrival;
    
    @Wire
    public Textbox NotityComposer.address;
    
    @Wire
    public Textbox NotityComposer.phone;
    
    @Wire
    public Textbox NotityComposer.nameofficer;
    
    @Wire
    public Toolbarbutton NotityComposer.btnAdd;
    
    @Wire
    public Toolbarbutton NotityComposer.btnExit;
    
    @Wire
    public Div NotityComposer.show_btn;
    
}
