package sut.se.immigration.web;


import org.zkoss.*;
import org.zkoss.zk.roo.annotations.ZKComposer;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import static org.zkoss.zk.ui.select.Selectors.find;
import org.zkoss.zk.ui.select.annotation.Listen;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import sut.se.immigration.entity.Workers;

import org.zkoss.zk.ui.Executions;

import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zul.Window;

import java.util.HashMap;
import org.zkoss.zk.ui.Sessions;  
import org.zkoss.bind.annotation.Command; 
import java.lang.Exception;
import java.text.DateFormat;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.Wire;
import java.lang.*;
import java.util.Map;
import java.util.*;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import org.zkoss.zk.ui.*;
import org.zkoss.zul.*;
import java.util.List;
import java.security.MessageDigest;

@ZKComposer(zul = "SearchAlienIndex.zul")
public class SearchAlienComposer {

    private static final long serialVersionUID = 426073226640313844L;

    public void afterCompose(Component comp) {

    	Rows befor_table_repay_date = (Rows) find(getPage(), "#befor_table_repay_date").get(0);
    	for(Workers e0: Workers.findAllWorkerses()){
       Row row1 =new Row();
       Label lb11= new Label(e0.getCheckin()+"");
       Label lb12= new Label(e0.getCheckout()+"");
       Label lb13= new Label(e0.getRegister_number()+"");
       Label lb14= new Label(e0.getRegister_position()+"");
       Label lb15= new Label(e0.getRegister_name()+"");
       Label lb16= new Label(e0.getRegister_Mname()+"");
       Label lb17= new Label(e0.getRegister_sex()+"");
       Label lb18= new Label(e0.getRegister_chad()+"");
       Label lb19= new Label(e0.getRegister_relation()+"");

        row1.appendChild(lb11);
        row1.appendChild(lb12);
        row1.appendChild(lb13);
        row1.appendChild(lb14);
        row1.appendChild(lb15);
       
        row1.appendChild(lb17);
        row1.appendChild(lb18);
        
        befor_table_repay_date.appendChild(row1);
  	}

   }


   @Listen("onChange = #name")
    public void btn_find_Clicked(Event event) {
        Textbox name = (Textbox) find(getPage(), "#name").get(0);

        for(Component cv : find(getPage(), "#befor_table_repay_date > row")){
            cv.detach();
        }

        Rows befor_table_repay_date = (Rows) find(getPage(), "#befor_table_repay_date").get(0); 


        TypedQuery<Workers> aa = Workers.findWorkersesByRegister_nameLike(name.getValue());
       	List<Workers> stdNow = aa.getResultList();

        for (Workers e0 : stdNow) {
          	
 			    Row row1 =new Row();
       Label lb11= new Label(e0.getCheckin()+"");
       Label lb12= new Label(e0.getCheckout()+"");
       Label lb13= new Label(e0.getRegister_number()+"");
       Label lb14= new Label(e0.getRegister_position()+"");
       Label lb15= new Label(e0.getRegister_name()+"");
       Label lb16= new Label(e0.getRegister_Mname()+"");
       Label lb17= new Label(e0.getRegister_sex()+"");
       Label lb18= new Label(e0.getRegister_chad()+"");
       Label lb19= new Label(e0.getRegister_relation()+"");

        row1.appendChild(lb11);
        row1.appendChild(lb12);
        row1.appendChild(lb13);
        row1.appendChild(lb14);
        row1.appendChild(lb15);
       
        row1.appendChild(lb17);
        row1.appendChild(lb18);
        
        befor_table_repay_date.appendChild(row1);
          	
        }

}



@Listen("onChange = #pass")
    public void pass_find_Clicked(Event event) {
        Textbox pass = (Textbox) find(getPage(), "#pass").get(0);

        for(Component cv : find(getPage(), "#befor_table_repay_date > row")){
            cv.detach();
        }

        Rows befor_table_repay_date = (Rows) find(getPage(), "#befor_table_repay_date").get(0); 


        TypedQuery<Workers> aa = Workers.findWorkersesByRegister_numberLike(pass.getValue());
       	List<Workers> stdNow = aa.getResultList();

        for (Workers e0 : stdNow) {
          	
 			Row row1 =new Row();
       Label lb11= new Label(e0.getCheckin()+"");
       Label lb12= new Label(e0.getCheckout()+"");
       Label lb13= new Label(e0.getRegister_number()+"");
       Label lb14= new Label(e0.getRegister_position()+"");
       Label lb15= new Label(e0.getRegister_name()+"");
       Label lb16= new Label(e0.getRegister_Mname()+"");
       Label lb17= new Label(e0.getRegister_sex()+"");
       Label lb18= new Label(e0.getRegister_chad()+"");
       Label lb19= new Label(e0.getRegister_relation()+"");

        row1.appendChild(lb11);
        row1.appendChild(lb12);
        row1.appendChild(lb13);
        row1.appendChild(lb14);
        row1.appendChild(lb15);
       
        row1.appendChild(lb17);
        row1.appendChild(lb18);
        
        befor_table_repay_date.appendChild(row1);
          	
        }

}



@Listen("onChange = #checkin")
    public void checkin_find_Clicked(Event event) {
        Datebox checkin = (Datebox) find(getPage(), "#checkin").get(0);

        for(Component cv : find(getPage(), "#befor_table_repay_date > row")){
            cv.detach();
        }

        Rows befor_table_repay_date = (Rows) find(getPage(), "#befor_table_repay_date").get(0); 


        TypedQuery<Workers> aa = Workers.findWorkersesByCheckinLike(checkin.getText());
       	List<Workers> stdNow = aa.getResultList();

        for (Workers e0 : stdNow) {
          	
 			Row row1 =new Row();
       Label lb11= new Label(e0.getCheckin()+"");
       Label lb12= new Label(e0.getCheckout()+"");
       Label lb13= new Label(e0.getRegister_number()+"");
       Label lb14= new Label(e0.getRegister_position()+"");
       Label lb15= new Label(e0.getRegister_name()+"");
       Label lb16= new Label(e0.getRegister_Mname()+"");
       Label lb17= new Label(e0.getRegister_sex()+"");
       Label lb18= new Label(e0.getRegister_chad()+"");
       Label lb19= new Label(e0.getRegister_relation()+"");

        row1.appendChild(lb11);
        row1.appendChild(lb12);
        row1.appendChild(lb13);
        row1.appendChild(lb14);
        row1.appendChild(lb15);
       
        row1.appendChild(lb17);
        row1.appendChild(lb18);
        
        befor_table_repay_date.appendChild(row1);
          	
        }

}



@Listen("onChange = #checkout")
    public void checkout_find_Clicked(Event event) {
        Datebox checkout = (Datebox) find(getPage(), "#checkout").get(0);

        for(Component cv : find(getPage(), "#befor_table_repay_date > row")){
            cv.detach();
        }

        Rows befor_table_repay_date = (Rows) find(getPage(), "#befor_table_repay_date").get(0); 


        TypedQuery<Workers> aa = Workers.findWorkersesByCheckoutLike(checkout.getText());
       	List<Workers> stdNow = aa.getResultList();

        for (Workers e0 : stdNow) {
          	
 			Row row1 =new Row();
       Label lb11= new Label(e0.getCheckin()+"");
       Label lb12= new Label(e0.getCheckout()+"");
       Label lb13= new Label(e0.getRegister_number()+"");
       Label lb14= new Label(e0.getRegister_position()+"");
       Label lb15= new Label(e0.getRegister_name()+"");
       Label lb16= new Label(e0.getRegister_Mname()+"");
       Label lb17= new Label(e0.getRegister_sex()+"");
       Label lb18= new Label(e0.getRegister_chad()+"");
       Label lb19= new Label(e0.getRegister_relation()+"");

        row1.appendChild(lb11);
        row1.appendChild(lb12);
        row1.appendChild(lb13);
        row1.appendChild(lb14);
        row1.appendChild(lb15);
       
        row1.appendChild(lb17);
        row1.appendChild(lb18);
        
        befor_table_repay_date.appendChild(row1);
          	
        }

}

@Listen("onClick = #logout")
    public void logout_Clicked(Event event) {
         Executions.sendRedirect("enterpriseindex.zul");
    }


@Listen("onChange = #no1")
    public void no1_find_Clicked(Event event) {
        Textbox no1 = (Textbox) find(getPage(), "#no1").get(0);

        for(Component cv : find(getPage(), "#befor_table_repay_date > row")){
            cv.detach();
        }

        Rows befor_table_repay_date = (Rows) find(getPage(), "#befor_table_repay_date").get(0); 


        TypedQuery<Workers> aa = Workers.findWorkersesByRegister_positionLike(no1.getText());
       	List<Workers> stdNow = aa.getResultList();

        for (Workers e0 : stdNow) {
          	
 			Row row1 =new Row();
		       Label lb11= new Label(e0.getCheckin()+"");
           Label lb12= new Label(e0.getCheckout()+"");
           Label lb13= new Label(e0.getRegister_number()+"");
           Label lb14= new Label(e0.getRegister_position()+"");
           Label lb15= new Label(e0.getRegister_name()+"");
          
           Label lb17= new Label(e0.getRegister_sex()+"");
           Label lb18= new Label(e0.getRegister_chad()+"");
          

            row1.appendChild(lb11);
            row1.appendChild(lb12);
            row1.appendChild(lb13);
            row1.appendChild(lb14);
            row1.appendChild(lb15);
            
            row1.appendChild(lb17);
            row1.appendChild(lb18);
          
            befor_table_repay_date.appendChild(row1);
          	
        }

}



@Listen("onChange = #sex")
    public void sex_find_Clicked(Event event) {
        Textbox sex = (Textbox) find(getPage(), "#sex").get(0);

        for(Component cv : find(getPage(), "#befor_table_repay_date > row")){
            cv.detach();
        }

        Rows befor_table_repay_date = (Rows) find(getPage(), "#befor_table_repay_date").get(0); 


        TypedQuery<Workers> aa = Workers.findWorkersesByRegister_sexLike(sex.getText());
       	List<Workers> stdNow = aa.getResultList();

        for (Workers e0 : stdNow) {
          	
 			Row row1 =new Row();
       Label lb11= new Label(e0.getCheckin()+"");
       Label lb12= new Label(e0.getCheckout()+"");
       Label lb13= new Label(e0.getRegister_number()+"");
       Label lb14= new Label(e0.getRegister_position()+"");
       Label lb15= new Label(e0.getRegister_name()+"");
       Label lb16= new Label(e0.getRegister_Mname()+"");
       Label lb17= new Label(e0.getRegister_sex()+"");
       Label lb18= new Label(e0.getRegister_chad()+"");
       Label lb19= new Label(e0.getRegister_relation()+"");

        row1.appendChild(lb11);
        row1.appendChild(lb12);
        row1.appendChild(lb13);
        row1.appendChild(lb14);
        row1.appendChild(lb15);
       
        row1.appendChild(lb17);
        row1.appendChild(lb18);
        
        befor_table_repay_date.appendChild(row1);
          	
        }

}


@Listen("onChange = #chad")
    public void chad_find_Clicked(Event event) {
        Textbox chad = (Textbox) find(getPage(), "#chad").get(0);

        for(Component cv : find(getPage(), "#befor_table_repay_date > row")){
            cv.detach();
        }

        Rows befor_table_repay_date = (Rows) find(getPage(), "#befor_table_repay_date").get(0); 


        TypedQuery<Workers> aa = Workers.findWorkersesByRegister_chadLike(chad.getText());
       	List<Workers> stdNow = aa.getResultList();

        for (Workers e0 : stdNow) {
          	
 			Row row1 =new Row();
       Label lb11= new Label(e0.getCheckin()+"");
       Label lb12= new Label(e0.getCheckout()+"");
       Label lb13= new Label(e0.getRegister_number()+"");
       Label lb14= new Label(e0.getRegister_position()+"");
       Label lb15= new Label(e0.getRegister_name()+"");
       Label lb16= new Label(e0.getRegister_Mname()+"");
       Label lb17= new Label(e0.getRegister_sex()+"");
       Label lb18= new Label(e0.getRegister_chad()+"");
       Label lb19= new Label(e0.getRegister_relation()+"");

        row1.appendChild(lb11);
        row1.appendChild(lb12);
        row1.appendChild(lb13);
        row1.appendChild(lb14);
        row1.appendChild(lb15);
       
        row1.appendChild(lb17);
        row1.appendChild(lb18);
        
        befor_table_repay_date.appendChild(row1);
          	
        }

}




@Listen("onChange = #changerow")
    public void find_Clicked(Event event) {
        Textbox changerow = (Textbox) find(getPage(), "#changerow").get(0);
        
        Grid sh = (Grid) find(getPage(), "#sh").get(0);

    	sh.setPageSize(Integer.parseInt(changerow.getValue()));
        
    }


 @Listen("onClick = #english")
    public void english_Clicked(Event event) {
         
            Textbox find = (Textbox) find(getPage(), "#find").get(0);
            find.setValue("Advanced search pattern. Please enter your criteria below:");

            Column db11 = (Column) find(getPage(), "#db11").get(0);
            db11.setLabel("Check-In:");

            Column db23 = (Column) find(getPage(), "#db23").get(0);
            db23.setLabel("Check-Out");

            Column registerchadEN = (Column) find(getPage(), "#registerchadEN").get(0);
            registerchadEN.setLabel("Nationality:");

            Column registerposition = (Column) find(getPage(), "#registerposition").get(0);
            registerposition.setLabel("TM6 NO:");

            Column registernumber = (Column) find(getPage(), "#registernumber").get(0);
            registernumber.setLabel("Passport NO:");

             Column db2222 = (Column) find(getPage(), "#db2222").get(0);
             db2222.setLabel("First Name Family Name:");

             

             Column registersex = (Column) find(getPage(), "#registersex").get(0);
             registersex.setLabel("Gender:");

             

             Label changerowEN = (Label) find(getPage(), "#changerowEN").get(0);
             changerowEN.setValue("Performing in front of:");


    }


     @Listen("onClick = #thai")
    public void thai_Clicked(Event event) {
            
            Textbox find = (Textbox) find(getPage(), "#find").get(0);
            find.setValue("รูปแบบการค้นหาข้อมูลขั้นสูง กรุณาระบุข้อมูลที่คุณต้องการค้นหาด้านล่าง:");

            Column db11 = (Column) find(getPage(), "#db11").get(0);
            db11.setLabel("วันที่เข้า:");

            Column db23 = (Column) find(getPage(), "#db23").get(0);
            db23.setLabel("วันที่ออก");

            Column registerchadEN = (Column) find(getPage(), "#registerchadEN").get(0);
            registerchadEN.setLabel("สัญชาติ:");

            Column registerposition = (Column) find(getPage(), "#registerposition").get(0);
            registerposition.setLabel("บัตรขาเข้าเลขที่:");

            Column registernumber = (Column) find(getPage(), "#registernumber").get(0);
            registernumber.setLabel("หมายเลขหนังสือเดินทาง:");

             Column db2222 = (Column) find(getPage(), "#db2222").get(0);
             db2222.setLabel("ชื่อ - สกุล:");

        
             Column registersex = (Column) find(getPage(), "#registersex").get(0);
             registersex.setLabel("เพศ:");

            

             Label changerowEN = (Label) find(getPage(), "#changerowEN").get(0);
             changerowEN.setValue("จำนวนการแสดงต่อหน้า:");
    }




}
