package sut.se.immigration.web;

import org.zkoss.zk.roo.annotations.ZKComposer;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zul.Doublebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.*;
import org.zkoss.zk.ui.event.*;
import static org.zkoss.zk.ui.select.Selectors.find;
import org.zkoss.zul.*;
import sut.se.immigration.entity.DataOffense;
import java.lang.*;
import java.io.*;
import java.util.*;
import javax.persistence.*;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.lang.Exception;
import java.lang.Object;
import org.zkoss.zul.Listbox;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import sut.se.immigration.entity.Personnal;
import sut.se.immigration.entity.AdminPersonnel;
import org.zkoss.zul.Window;
import org.zkoss.zk.ui.select.annotation.Wire;

@ZKComposer(zul = "DeleteDataOffense.zul")
public class DeleteDataOffense {

    private static final long serialVersionUID = 5483290940451751756L;
    @Wire
    Window wndMain;

    public void afterCompose(Component comp) {
      try{
      Session sessIdPersonnal =  Sessions.getCurrent();
      String id = String.valueOf(sessIdPersonnal.getAttribute("personnelid"));
          txtOutputIdD6.setValue(id); 

          TypedQuery<Personnal> iduser = Personnal.findPersonnalsByIdPersonnal(txtOutputIdD6.getValue());
                Personnal user = iduser.getSingleResult();
                    txtOutputNameD6.setValue(user.getNamePersonnal()); 
      }
     catch(Exception e){
            Session sessIdAdmin =  Sessions.getCurrent();
                    String id1 = String.valueOf(sessIdAdmin.getAttribute("adminid"));
                    txtOutputIdD6.setValue(id1); 

            TypedQuery<AdminPersonnel> ad = AdminPersonnel.findAdminPersonnelsByIdAdmin(txtOutputIdD6.getValue());
                    AdminPersonnel admin = ad.getSingleResult();
                    txtOutputNameD6.setValue(admin.getNameAdmin());
     }   

    }

    @Listen("onClick = #btnFindId")
    public void btnFindId_Clicked(Event event){
        try{

             TypedQuery<DataOffense> dt = DataOffense.findDataOffensesByLawsuitId(findId.getValue());
                    DataOffense ds = dt.getSingleResult(); 
                        alert("พบข้อมูลประวัติของผู้กระทำความผิดที่ต้องการลบ " + " " + ds.getLawsuitId()); 
                       
                    Row row1 = new Row();
                       Label lb1 = new Label();
                       Label lb2 = new Label();
                       Label lb3 = new Label();
                       Label lb4 = new Label();
                    Row row2 = new Row();
                       Label lb5 = new Label();
                       Label lb6 = new Label();
                       Label lb7 = new Label();
                       Label lb8 = new Label();

                       lb1.setValue(ds.getLawsuitId());
                       row1.appendChild(lb1);
                       lb2.setValue(ds.getFirstName() + " " + ds.getLastName());
                       row1.appendChild(lb2);
                       lb3.setValue(ds.getLawsuitName());
                       row1.appendChild(lb3);
                       lb4.setValue(ds.getOwnerLawsuit());
                       row1.appendChild(lb4);
                       rshow1.appendChild(row1);
                       lb5.setValue(ds.getIdentifyNumber());
                       row2.appendChild(lb5);
                       lb6.setValue(ds.getNationality());
                       row2.appendChild(lb6);
                       lb7.setValue(ds.getPrescription());
                       row2.appendChild(lb7);
                       lb8.setValue(ds.getExpiratePrescription());
                       row2.appendChild(lb8);
                       rshow2.appendChild(row2);

      }
      catch(Exception e){
                alert("ไม่พบข้อมูลนี้ในฐานข้อมูล");
            }
    
  }

  @Listen("onClick = #btnDelete")
    public void btnDelete_Clicked(Event event) {
      try{
            TypedQuery<DataOffense> dt = DataOffense.findDataOffensesByLawsuitId(findId.getValue());
                  DataOffense ds = dt.getSingleResult();

                  Map<String, Object> arguments = new HashMap<String, Object>(); 
                      arguments.put("setid",findId.getValue() );

                  Window window = (Window)Executions.createComponents(
                          "DeleteMassage.zul", null, arguments);
                          window.doModal();

      }catch(Exception e){
            alert("ข้อมูลถูกลบไปแล้วไม่สามารถลบข้อมูลได้");
        }              
    
  }

  @Listen("onClick = #btnSubmit")
    public void btnSubmit_Clicked(Event event) {
          Label lbnumber = (Label)find(getPage(),"window > hbox > vbox > div > #lbnumber").get(0);
       
          try{
                TypedQuery<DataOffense> dt = DataOffense.findDataOffensesByLawsuitId(lbnumber.getValue());
                      DataOffense ds = dt.getSingleResult();
                          alert("ลบข้อมูลของหมายเลข " + " " + ds.getLawsuitId() + " " + "เรียบร้อยแล้ว"); 
                          ds.remove();
                          wndMain.detach();
                          

          }catch(Exception t){
                alert("ข้อมูลถูกลบไปแล้วไม่สามารถลบข้อมูลได้");
                
          }              
  }

  @Listen("onClick = #btnCancel")
    public void btnCancel_Clicked(Event event) {
           
            wndMain.detach();

    }

  @Listen("onClick = #btnSubmit23")
    public void btnSubmit19_Clicked(Event event) {
        Executions.sendRedirect("register.zul");
    
  }

  @Listen("onClick = #btnSubmit20")
    public void btnSubmit20_Clicked(Event event) {
        Executions.sendRedirect("LoginBorderpass.zul");
    
  }

  @Listen("onClick = #btnSubmit21")
    public void bbtnSubmit21_Clicked(Event event) {
        Executions.sendRedirect("notity.zul");
    
  }

  @Listen("onClick = #btnSubmit22")
    public void btnSubmit22_Clicked(Event event) {
        Executions.sendRedirect("CheckDataOffense.zul");
    
  }

  @Listen("onClick = #btnHom6")
    public void btnHome6_Clicked(Event event) {
        Executions.sendRedirect("HomeDataOffense.zul");
    
  }

  @Listen("onClick = #btnSave6")
    public void btnSave6_Clicked(Event event) {
        Executions.sendRedirect("SaveDataOffense.zul");
    
  }

  @Listen("onClick = #btnEdit6")
    public void btnEdit6_Clicked(Event event) {
        Executions.sendRedirect("EditDataOffense.zul");
    
  }

  @Listen("onClick = #btnExit6")
    public void btnExit6_Clicked(Event event) {
        Executions.sendRedirect("LoginDataOffense.zul");
        txtOutputIdD6.setValue("");
        txtOutputNameD6.setValue(""); 

    
  }

}
