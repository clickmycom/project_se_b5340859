package sut.se.immigration.web;

import org.zkoss.*;
import org.zkoss.zk.roo.annotations.ZKComposer;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import static org.zkoss.zk.ui.select.Selectors.find;
import org.zkoss.zk.ui.select.annotation.Listen;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import sut.se.immigration.entity.Workers;

import org.zkoss.zk.ui.Executions;

import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zul.Window;

import java.util.HashMap;
import org.zkoss.zk.ui.Sessions;  
import org.zkoss.bind.annotation.Command; 
import java.lang.Exception;
import java.text.DateFormat;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.Wire;
import java.lang.*;
import java.util.Map;
import java.util.*;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import org.zkoss.zk.ui.*;
import org.zkoss.zul.*;
import java.util.List;
import java.security.MessageDigest;

@ZKComposer(zul = "AllAlienIndex.zul")
public class AllAlienComposer {

    private static final long serialVersionUID = 2627834058466686591L;

    public void afterCompose(Component comp) {

    	

    	Rows befor_table_repay_date = (Rows) find(getPage(), "#befor_table_repay_date").get(0);
    	for(Workers e0: Workers.findAllWorkerses()){
       Row row1 =new Row();
       Label lb11= new Label(e0.getCheckin()+"");
       Label lb12= new Label(e0.getCheckout()+"");
       Label lb13= new Label(e0.getRegister_number()+"");
       Label lb14= new Label(e0.getRegister_position()+"");
       Label lb15= new Label(e0.getRegister_name()+"");
       Label lb16= new Label(e0.getRegister_Mname()+"");
       Label lb17= new Label(e0.getRegister_sex()+"");
       Label lb18= new Label(e0.getRegister_chad()+"");
       Label lb19= new Label(e0.getRegister_relation()+"");

        row1.appendChild(lb11);
        row1.appendChild(lb12);
        row1.appendChild(lb13);
        row1.appendChild(lb14);
        row1.appendChild(lb15);
        row1.appendChild(lb16);
        row1.appendChild(lb17);
        row1.appendChild(lb18);
        row1.appendChild(lb19);
        befor_table_repay_date.appendChild(row1);
  	}

   }


   @Listen("onChange = #changerow")
    public void find_Clicked(Event event) {
        Textbox changerow = (Textbox) find(getPage(), "#changerow").get(0);
        
        Grid sh = (Grid) find(getPage(), "#sh").get(0);

    	sh.setPageSize(Integer.parseInt(changerow.getValue()));
        
    }

    @Listen("onClick = #logout")
    public void logout_Clicked(Event event) {
         Executions.sendRedirect("enterpriseindex.zul");
    }

    @Listen("onClick = #find")
    public void fin11d_Clicked(Event event) {
         find.setValue("");
    }



    @Listen("onClick = #btn_find")
    public void btn_find_Clicked(Event event) {
        Textbox find = (Textbox) find(getPage(), "#find").get(0);

        for(Component cv : find(getPage(), "#befor_table_repay_date > row")){
            cv.detach();
        }

        Rows befor_table_repay_date = (Rows) find(getPage(), "#befor_table_repay_date").get(0); 


        TypedQuery<Workers> aa = Workers.findWorkersesByRegister_nameLike(find.getValue());
       	List<Workers> stdNow = aa.getResultList();

        for (Workers e0 : stdNow) {
          	
 			Row row1 =new Row();
		       Label lb11= new Label(e0.getCheckin()+"");
		       Label lb12= new Label(e0.getCheckout()+"");
		       Label lb13= new Label(e0.getRegister_number()+"");
		       Label lb14= new Label(e0.getRegister_position()+"");
		       Label lb15= new Label(e0.getRegister_name()+"");
		       Label lb16= new Label(e0.getRegister_Mname()+"");
		       Label lb17= new Label(e0.getRegister_sex()+"");
		       Label lb18= new Label(e0.getRegister_chad()+"");
		       Label lb19= new Label(e0.getRegister_relation()+"");

		        row1.appendChild(lb11);
		        row1.appendChild(lb12);
		        row1.appendChild(lb13);
		        row1.appendChild(lb14);
		        row1.appendChild(lb15);
		        row1.appendChild(lb16);
		        row1.appendChild(lb17);
		        row1.appendChild(lb18);
		        row1.appendChild(lb19);

             
            befor_table_repay_date.appendChild(row1);
          	
        }



    }




     @Listen("onClick = #english")
    public void english_Clicked(Event event) {
          	Toolbarbutton btn_find = (Toolbarbutton) find(getPage(), "#btn_find").get(0);
            btn_find.setLabel("Search");

            Textbox find = (Textbox) find(getPage(), "#find").get(0);
            find.setValue("Please enter your first name or last name nearby:");


            Column db11 = (Column) find(getPage(), "#db11").get(0);
            db11.setLabel("Check-In:");

            Column db23 = (Column) find(getPage(), "#db23").get(0);
            db23.setLabel("Check-Out");

            Column registerchadEN = (Column) find(getPage(), "#registerchadEN").get(0);
            registerchadEN.setLabel("Nationality:");

            Column registerposition = (Column) find(getPage(), "#registerposition").get(0);
            registerposition.setLabel("TM6 NO:");

            Column registernumber = (Column) find(getPage(), "#registernumber").get(0);
            registernumber.setLabel("Passport NO:");

             Column db2222 = (Column) find(getPage(), "#db2222").get(0);
             db2222.setLabel("First Name Family Name:");

             Column registerMname = (Column) find(getPage(), "#registerMname").get(0);
             registerMname.setLabel("Middle Name:");

             Column registersex = (Column) find(getPage(), "#registersex").get(0);
             registersex.setLabel("Gender:");

             
             Column registerrelation13 = (Column) find(getPage(), "#registerrelation13").get(0);
             registerrelation13.setLabel("Relation:");

             Label changerowEN = (Label) find(getPage(), "#changerowEN").get(0);
             changerowEN.setValue("Performing in front of:");


    }


     @Listen("onClick = #thai")
    public void thai_Clicked(Event event) {
         	Toolbarbutton btn_find = (Toolbarbutton) find(getPage(), "#btn_find").get(0);
            btn_find.setLabel("ค้นหา");


			Textbox find = (Textbox) find(getPage(), "#find").get(0);
            find.setValue("กรุณาใส่ชื่อของคุณหรือนามสกุลที่ใกล้เคียง:");


            Column db11 = (Column) find(getPage(), "#db11").get(0);
            db11.setLabel("วันที่เข้า:");

            Column db23 = (Column) find(getPage(), "#db23").get(0);
            db23.setLabel("วันที่ออก");

            Column registerchadEN = (Column) find(getPage(), "#registerchadEN").get(0);
            registerchadEN.setLabel("สัญชาติ:");

            Column registerposition = (Column) find(getPage(), "#registerposition").get(0);
            registerposition.setLabel("บัตรขาเข้าเลขที่:");

            Column registernumber = (Column) find(getPage(), "#registernumber").get(0);
            registernumber.setLabel("หมายเลขหนังสือเดินทาง:");

             Column db2222 = (Column) find(getPage(), "#db2222").get(0);
             db2222.setLabel("ชื่อ - สกุล:");

             Column registerMname = (Column) find(getPage(), "#registerMname").get(0);
             registerMname.setLabel("ชื่อกลาง:");

             Column registersex = (Column) find(getPage(), "#registersex").get(0);
             registersex.setLabel("เพศ:");

             
             Column registerrelation13 = (Column) find(getPage(), "#registerrelation13").get(0);
             registerrelation13.setLabel("ความสัมพันธ์:");

             Label changerowEN = (Label) find(getPage(), "#changerowEN").get(0);
             changerowEN.setValue("จำนวนการแสดงต่อหน้า:");
    }

}
