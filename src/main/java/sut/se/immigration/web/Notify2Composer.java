package sut.se.immigration.web;

import java.*;
import java.lang.Exception;
import java.lang.Object;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import javax.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;
import org.zkoss.*;
import org.zkoss.zk.roo.annotations.ZKComposer;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import static org.zkoss.zk.ui.select.Selectors.find;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zul.Doublebox;
import org.zkoss.zul.Listbox;
import sut.se.immigration.entity.Notify;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import org.zkoss.zk.ui.*;
import org.zkoss.zul.*;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zul.Datebox;


@ZKComposer(zul = "Notify2.zul")
public class Notify2Composer {

    private static final long serialVersionUID = 8061434679752228905L;

    public void afterCompose(Component comp) {
    }
     @Listen("onClick = #btn_find")
    public void btn_find_Clicked(Event event) {


    TypedQuery<Notify> eee = Notify.findNotifysByIdpassport(find.getText());
      Notify newbt = eee.getSingleResult(); 
      
       address.setValue(newbt.getAddress());
            ddmmyystart.setValue(newbt.getDdmmyystart());
    ddmmyyent.setValue(newbt.getDdmmyyent());
      name.setValue(newbt.getName());
     
       visa.setValue(newbt.getVisa());
                
        idpassport.setValue(newbt.getIdpassport());
         idarrival.setValue(newbt.getIdarrival());
      
       phone.setValue(newbt.getPhone());
       nameofficer.setValue(newbt.getNameofficer());
       nationality.setValue(newbt.getNationality());
     }

      @Listen("onClick = #btnAdd")
    public void btnAdd_Clicked(Event event) {

     
    TypedQuery<Notify> eee = Notify.findNotifysByIdpassport(find.getText());
      Notify newbt = eee.getSingleResult();
        
       newbt.setDdmmyystart(ddmmyystart.getValue());
        newbt.setDdmmyyent(ddmmyyent.getValue());
        newbt.setName(name.getValue());
        newbt.setNationality(nationality.getValue());
        newbt.setVisa(visa.getValue());
        newbt.setIdpassport(idpassport.getValue());
        newbt.setIdarrival(idarrival.getValue());
        newbt.setAddress(address.getValue());
        newbt.setPhone(phone.getValue());
        newbt.setNameofficer(nameofficer.getValue());

      try {
            newbt.persist();
             alert("บันทึกเรียบร้อย" );
            ddmmyystart.setValue("");
            ddmmyyent.setValue("");
             name.setValue("");
        nationality.setValue("");
        visa.setValue("");
                
         idpassport.setValue("");
        idarrival.setValue("");
        address.setValue("");
        phone.setValue("");
        nameofficer.setValue("");
        } catch (Exception e) {
            alert("ไม่สามารถบันทึกข้อมูลได้");
        }
    }
       
       @Listen("onClick = #btnExit")
    public void btnExit_Clicked(Event event) {
    TypedQuery<Notify> eee = Notify.findNotifysByIdpassport(find.getText());
    Notify newbt = eee.getSingleResult();
        
       newbt.setDdmmyystart(ddmmyystart.getValue());
        newbt.setDdmmyyent(ddmmyyent.getValue());
        newbt.setName(name.getValue());
        newbt.setNationality(nationality.getValue());
        newbt.setVisa(visa.getValue());
        newbt.setIdpassport(idpassport.getValue());
        newbt.setIdarrival(idarrival.getValue());
        newbt.setAddress(address.getValue());
        newbt.setPhone(phone.getValue());
        newbt.setNameofficer(nameofficer.getValue());
      
      
        ddmmyystart.setValue("");
            ddmmyyent.setValue("");
             name.setValue("");
        nationality.setValue("");
        visa.setValue("");
                
         idpassport.setValue("");
        idarrival.setValue("");
        address.setValue("");
        phone.setValue("");
        nameofficer.setValue("");
          
            }
          


}
