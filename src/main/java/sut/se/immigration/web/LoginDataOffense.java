package sut.se.immigration.web;

import org.zkoss.zk.roo.annotations.ZKComposer;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zul.Doublebox;
import org.zkoss.zul.Toolbar;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import static org.zkoss.zk.ui.select.Selectors.find;
import org.zkoss.zul.*;
import org.zkoss.zk.ui.*;
import org.zkoss.zk.ui.event.*;
import sut.se.immigration.entity.Personnal;
import java.lang.*;
import java.io.*;
import java.util.*;
import javax.persistence.*;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.lang.Exception;
import java.lang.Object;
import org.zkoss.zul.Listbox;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.EventInterceptor;
import sut.se.immigration.entity.AdminPersonnel;


@ZKComposer(zul = "LoginDataOffense.zul")
public class LoginDataOffense {

    private static final long serialVersionUID = -7823312785707277956L;

    public void afterCompose(Component comp) {

    }
    
         
                    

          

      @Listen("onClick = #btnLogin")
    public void btnLogin_Clicked(Event event) {

      Component adminLayout = (Component)find(getPage(),"window > borderlayout > center > borderlayout > center > panel > panelchildren > #adminLayout").get(0);
      Component memberLayout = (Component)find(getPage(),"window > borderlayout > center > borderlayout > center > panel > panelchildren > #memberLayout").get(0);
      Component gbLongin1 = (Component)find(getPage(),"#gbLongin1").get(0);
      Component gbLoginShow = (Component)find(getPage(),"#gbLoginShow").get(0);
      Textbox txtInputName5 = (Textbox)find(getPage(),"#txtInputName5").get(0); 
      Textbox txtInputName6 = (Textbox)find(getPage(),"#txtInputName6").get(0); 
      Component panelCheck = (Component)find(getPage(),"#panelCheck").get(0);

        try{
             TypedQuery<Personnal> us = Personnal.findPersonnalsByIdPersonnalEquals(txtInputUser.getValue());
                   Personnal pu = us.getSingleResult();
            
                        
                      if (txtInputPass.getValue().equals(pu.getPassPersonnal())) {
                          alert("พนักงาน ID :" + " " + pu.getIdPersonnal() + " " + pu.getNamePersonnal() + "เข้าระบบข้อมูลประวัติการกระทำความผิด");
                          memberLayout.setVisible(true);
                          gbLongin1.setVisible(false);
                          gbLoginShow.setVisible(true);
                          panelCheck.setVisible(true);
                          txtInputName5.setValue(pu.getIdPersonnal());
                          txtInputName6.setValue(pu.getNamePersonnal());

                          String idPersonnal = String.valueOf(txtInputUser.getValue());
                           Session sessIdPersonnal =  Sessions.getCurrent();
                            sessIdPersonnal.setAttribute("personnelid",idPersonnal);
                            
                            
                      }else {
                         alert("รหัสผ่านไม่ถูกต้อง");
                         txtInputPass.setValue("");
                      }

                       
        }
        catch(Exception e){

          try{
          TypedQuery<AdminPersonnel> ad = AdminPersonnel.findAdminPersonnelsByIdAdmin(txtInputUser.getValue());
                AdminPersonnel admin = ad.getSingleResult();
            
                        
                      if (txtInputPass.getValue().equals(admin.getPassAdmin())) {
                          alert("Admin ID :" + " " + admin.getIdAdmin() + " " + admin.getNameAdmin() + "เข้าระบบข้อมูลประวัติการกระทำความผิด");
                          adminLayout.setVisible(true);
                          gbLongin1.setVisible(false);
                           gbLoginShow.setVisible(true);
                           panelCheck.setVisible(true);
                           txtInputName5.setValue(admin.getIdAdmin());
                           txtInputName6.setValue(admin.getNameAdmin());

                          String idAdmin = String.valueOf(txtInputUser.getValue());
                           Session sessIdAdmin =  Sessions.getCurrent();
                            sessIdAdmin.setAttribute("adminid",idAdmin);
                            
                            
                      }else {
                         alert("รหัสผ่านไม่ถูกต้อง");
                         txtInputPass.setValue("");
                      }
          }
          catch(Exception ed){            
                alert("ชื่อผู้ใช้และรหัสผ่านที่ท่านกรอกไม่ใช่ข้อมูลของพนักงาน");
                txtInputUser.setValue("");
                txtInputPass.setValue("");
        }
    }
  }


    @Listen("onClick = #btnSubmit")
    public void btnSubmit_Clicked(Event event) {
        Executions.sendRedirect("register.zul");
    
  }

  @Listen("onClick = #btnSubmit1")
    public void btnSubmit1_Clicked(Event event) {
        Executions.sendRedirect("LoginBorderpass.zul");
    
  }

  @Listen("onClick = #btnSubmit2")
    public void btnSubmit2_Clicked(Event event) {
        Executions.sendRedirect("notity.zul");
    
  }

   @Listen("onClick = #btnSubmit3")
    public void btnSubmit3_Clicked(Event event) {
        Executions.sendRedirect("CheckDataOffense.zul");
    
  }

   @Listen("onClick = #delete1")
    public void delete1_Clicked(Event event) {
        Executions.sendRedirect("DeleteDataOffense.zul");
    
    }

    @Listen("onClick = #save1")
    public void save1_Clicked(Event event) {
      Executions.sendRedirect("SaveDataOffense.zul");
    }

    @Listen("onClick = #edit1")
    public void edit1_Clicked(Event event) {
      Executions.sendRedirect("EditDataOffense.zul");
    }

    @Listen("onClick = #exit1")
    public void exit1_Clicked(Event event) {
      Executions.sendRedirect("LoginDataOffense.zul");
       txtInputName5.setValue("");
       txtInputName6.setValue("");
    }

    @Listen("onClick = #exit12")
    public void exit12_Clicked(Event event) {
      Executions.sendRedirect("LoginDataOffense.zul");
       txtInputName5.setValue("");
       txtInputName6.setValue("");
    }
}
