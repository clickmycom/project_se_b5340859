package sut.se.immigration.web;

import org.zkoss.zk.roo.annotations.ZKComposer;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zul.Doublebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.*;
import org.zkoss.zk.ui.event.*;
import org.zkoss.zk.ui.util.EventInterceptor;
import static org.zkoss.zk.ui.select.Selectors.find;
import org.zkoss.zul.*;
import sut.se.immigration.entity.DataOffense;
import java.lang.*;
import java.io.*;
import java.util.*;
import javax.persistence.*;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.lang.Exception;
import java.lang.Object;
import org.zkoss.zul.Listbox;
import sut.se.immigration.entity.Personnal;
import sut.se.immigration.entity.AdminPersonnel;
import org.zkoss.zk.ui.util.Clients;


@ZKComposer(zul = "SaveDataOffense.zul")
public class SaveDataOffense {

    private static final long serialVersionUID = -6709514142025366314L;

    public void afterCompose(Component comp) {
      try{
        Session sessIdPersonnal =  Sessions.getCurrent();
      String id = String.valueOf(sessIdPersonnal.getAttribute("personnelid"));
      txtInputName.setValue(id); 

      TypedQuery<Personnal> iduser = Personnal.findPersonnalsByIdPersonnal(txtInputName.getValue());
                   Personnal user = iduser.getSingleResult();
                    txtInputName1.setValue(user.getNamePersonnal()); 

     }
     catch(Exception e){
            Session sessIdAdmin =  Sessions.getCurrent();
                    String id1 = String.valueOf(sessIdAdmin.getAttribute("adminid"));
                    txtInputName.setValue(id1); 

            TypedQuery<AdminPersonnel> ad = AdminPersonnel.findAdminPersonnelsByIdAdmin(txtInputName.getValue());
                    AdminPersonnel admin = ad.getSingleResult();
                    txtInputName1.setValue(admin.getNameAdmin());
     }   
                    
    }

    @Listen("onClick = #btnSave2")
    public void btnSave2_Clicked(Event event) {

        DataOffense d = new DataOffense();
             DateFormat datesave = new SimpleDateFormat("dd/MM/yyyy");
          
                 d.setFirstName(txtInput1.getValue());
                 d.setLastName(txtInput2.getValue());
                 d.setIdentifyNumber(txtInput3.getValue());
                 d.setAge(ageCombobox.getValue());
                 d.setBirthDate(datesave.format(db.getValue()));
                 d.setNationality(nationlityCombobox.getValue());
                 d.setAddress(txtInput4.getValue());
                 d.setLawsuitId(txtInput5.getValue());
                 d.setLawsuitName(txtInput6.getValue());
                 d.setPrescription(datesave.format(db1.getValue()));
                 d.setExpiratePrescription(datesave.format(db2.getValue()));
                 d.setLawsuitDetail(txtInput7.getValue());
                 d.setOwnerLawsuit(txtInput8.getValue());
            try{
                d.persist();
                alert("บันทึกข้อมูลเรียบร้อยแล้ว");

            }
            catch(Exception e){
                alert("ไม่สามารถบันทึกข้อมูลได้ กรุณากรอกข้อมูลให้ครบถ้วน");
            }
            

    }

    @Listen("onClick = #txtInput1")
    public void txtInput1_Clicked(Event event) {
       Textbox txtInput1 = (Textbox)find(getPage(),"#txtInput1").get(0);
         Clients.showNotification("ใส่ได้เฉพาะ a-z และ A-Z เท่านั้น ไม่เกิน 30 ตัวอักษร", txtInput1);
    }

     @Listen("onClick = #txtInput2")
    public void txtInput2_Clicked(Event event) {
       Textbox txtInput2 = (Textbox)find(getPage(),"#txtInput2").get(0);
         Clients.showNotification("ใส่ได้เฉพาะ a-z และ A-Z เท่านั้น ไม่เกิน 30 ตัวอักษร", txtInput2);
    }

     @Listen("onClick = #txtInput3")
    public void txtInput3_Clicked(Event event) {
       Textbox txtInput3 = (Textbox)find(getPage(),"#txtInput3").get(0);
         Clients.showNotification("ใส่ได้เฉพาะเลข 0-9 จำนวน 13 ตัวเท่านั้น", txtInput3);
    }

    @Listen("onClick = #txtInput4")
    public void txtInput4_Clicked(Event event) {
       Textbox txtInput4 = (Textbox)find(getPage(),"#txtInput4").get(0);
         Clients.showNotification("กรอกที่อยู่ โดยใส่ได้ a-z,A-Z,0-9 และช่องว่าง", txtInput4);
    }

    @Listen("onClick = #nationlityCombobox")
    public void nationlityCombobox_Clicked(Event event) {
       Combobox nationlityCombobox = (Combobox)find(getPage(),"#nationlityCombobox").get(0);
         Clients.showNotification("ใส่ได้เฉพาะ a-z และ A-Z เท่านั้น ไม่เกิน 40 ตัวอักษร", nationlityCombobox);
    }

    @Listen("onClick = #txtInput5")
    public void txtInput5_Clicked(Event event) {
       Textbox txtInput5 = (Textbox)find(getPage(),"#txtInput5").get(0);
         Clients.showNotification("ใส่ได้เฉพาะเลข 0-9 และ A-Z จำนวน 8 ตัวเท่านั้น", txtInput5);
    }

    @Listen("onClick = #txtInput6")
    public void txtInput6_Clicked(Event event) {
       Textbox txtInput6 = (Textbox)find(getPage(),"#txtInput6").get(0);
         Clients.showNotification("ใส่ได้เฉพาะ a-z,A-Z และช่องว่าง ใส่ได้ไม่เกิน 50 ตัวอักษร", txtInput6);
    }

    @Listen("onClick = #txtInput8")
    public void txtInput8_Clicked(Event event) {
       Textbox txtInput8 = (Textbox)find(getPage(),"#txtInput8").get(0);
         Clients.showNotification("ใส่ได้เฉพาะ a-z,A-Z และช่องว่าง ใส่ได้ไม่เกิน 30 ตัวอักษร", txtInput8);
    }

    @Listen("onClick = #txtInput7")
    public void txtInput7_Clicked(Event event) {
       Textbox txtInput7 = (Textbox)find(getPage(),"#txtInput7").get(0);
         Clients.showNotification("กรอกรายละเอียดคดีความ", txtInput7);
    }

    
    @Listen("onClick = #btnReset2")
    public void btnReset2_Clicked(Event event) {
      
              txtInput1.setValue("");
              txtInput2.setValue("");
              txtInput3.setValue("");
              ageCombobox.setValue("");
              nationlityCombobox.setValue("");
              txtInput4.setValue("");
              txtInput5.setValue("");
              txtInput6.setValue("");
              txtInput7.setValue("");
              txtInput8.setValue("");
    }

    @Listen("onClick = #tbHome")
    public void tbHome_Clicked(Event event) {
        Executions.sendRedirect("HomeDataOffense.zul");
    
    }

    @Listen("onClick = #tbDelete")
    public void tbSave_Clicked(Event event) {
      Executions.sendRedirect("DeleteDataOffense.zul");
    }

    @Listen("onClick = #tbEdit")
    public void tbEdit_Clicked(Event event) {
      Executions.sendRedirect("EditDataOffense.zul");
    }

    @Listen("onClick = #tbExit")
    public void tbExit_Clicked(Event event) {
      Executions.sendRedirect("LoginDataOffense.zul");
      txtInputName.setValue(""); 
      txtInputName1.setValue("");
     }   

    

    @Listen("onClick = #btnSubmit8")
    public void btnSubmit8_Clicked(Event event) {
        Executions.sendRedirect("register.zul");
    
  }

  @Listen("onClick = #btnSubmit9")
    public void btnSubmit9_Clicked(Event event) {
        Executions.sendRedirect("LoginBorderpass.zul");
    
  }

  @Listen("onClick = #btnSubmit10")
    public void btnSubmit10_Clicked(Event event) {
        Executions.sendRedirect("notity.zul");
    
  }

   @Listen("onClick = #btnSubmit11")
    public void btnSubmit11_Clicked(Event event) {
        Executions.sendRedirect("CheckDataOffense.zul");
    
  }
}
