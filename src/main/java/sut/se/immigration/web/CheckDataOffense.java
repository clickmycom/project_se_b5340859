package sut.se.immigration.web;

import org.zkoss.zk.roo.annotations.ZKComposer;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zul.Doublebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.*;
import org.zkoss.zk.ui.event.*;
import static org.zkoss.zk.ui.select.Selectors.find;
import org.zkoss.zul.*;
import sut.se.immigration.entity.DataOffense;
import java.lang.*;
import java.io.*;
import java.util.*;
import javax.persistence.*;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.lang.Exception;
import java.lang.Object;
import org.zkoss.zul.Listbox;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import sut.se.immigration.entity.Personnal;
import sut.se.immigration.entity.AdminPersonnel;
import org.zkoss.zul.Window;
import org.zkoss.zk.ui.select.annotation.Wire;

@ZKComposer(zul = "CheckDataOffense.zul")
public class CheckDataOffense {

    private static final long serialVersionUID = -8907238091691696285L;
    @Wire
    Window wndMain;

    public void afterCompose(Component comp) {
      try{
           Session sessIdPersonnal =  Sessions.getCurrent();
                  String id = String.valueOf(sessIdPersonnal.getAttribute("personnelid"));
                  txtOutputIdC.setValue(id); 

            TypedQuery<Personnal> iduser = Personnal.findPersonnalsByIdPersonnal(txtOutputIdC.getValue());
                   Personnal user = iduser.getSingleResult();
                   txtOutputNameC.setValue(user.getNamePersonnal());
     }
     catch(Exception e){
            Session sessIdAdmin =  Sessions.getCurrent();
                    String id1 = String.valueOf(sessIdAdmin.getAttribute("adminid"));
                    txtOutputIdC.setValue(id1); 

            TypedQuery<AdminPersonnel> ad = AdminPersonnel.findAdminPersonnelsByIdAdmin(txtOutputIdC.getValue());
                    AdminPersonnel admin = ad.getSingleResult();
                    txtOutputNameC.setValue(admin.getNameAdmin());
     }   

    }

    @Listen("onClick = #btnChecked")
    public void btnChecked_Clicked(Event event) {

      try{
        TypedQuery<DataOffense> ckf = DataOffense.findDataOffensesByFirstName(findFname.getValue());
          DataOffense checkf = ckf.getSingleResult(); 
        TypedQuery<DataOffense> ckl = DataOffense.findDataOffensesByLastName(findLname.getValue());
          DataOffense checkl = ckl.getSingleResult(); 
        TypedQuery<DataOffense> cki = DataOffense.findDataOffensesByIdentifyNumber(findIdnumber.getValue());
                DataOffense checki = cki.getSingleResult(); 
                     alert("พบข้อมูลประวัติขการกระทำความผิดของชื่อ" + " " + checkf.getFirstName() + " " + checkl.getLastName() + " " + "เลขบัตรประชาชน" + " " + checki.getIdentifyNumber()); 
                     
                       txtOutputc1.setValue(checkf.getFirstName());
                       txtOutputc2.setValue(checkl.getLastName());
                       txtOutputc3.setValue(checki.getIdentifyNumber());
                       txtOutputc4.setValue(checki.getNationality());
                       txtOutputc5.setValue(checki.getLawsuitName());
                       txtOutputc6.setValue(checki.getExpiratePrescription());
                       
                        String identifyNumber = String.valueOf(findIdnumber.getValue());
                           Session sessidentifyNumber =  Sessions.getCurrent();
                            sessidentifyNumber.setAttribute("identifyNumberId",identifyNumber);

        }
      catch(Exception e){
                alert("ไม่พบข้อมูลการกระทำความผิด");
                findFname.setValue("");
                findLname.setValue("");
                findIdnumber.setValue("");
        }
    
  }

  @Listen("onClick = #btnView")
    public void btnView_Clicked(Event event) {
        
        try{
        TypedQuery<DataOffense> ckf = DataOffense.findDataOffensesByFirstName(findFname.getValue());
            DataOffense checkf = ckf.getSingleResult(); 
        TypedQuery<DataOffense> ckl = DataOffense.findDataOffensesByLastName(findLname.getValue());
            DataOffense checkl = ckl.getSingleResult(); 
        TypedQuery<DataOffense> cki = DataOffense.findDataOffensesByIdentifyNumber(findIdnumber.getValue());
            DataOffense checki = cki.getSingleResult(); 

    
                  Window window = (Window)Executions.createComponents(
                          "ViewDataOffense.zul", null, null);
                          window.doModal();
      }
      catch(Exception e){
                alert("ไม่พบข้อมูลการกระทำความผิด จึงไม่สมารถแสดงรายละเอียดข้อมูลได้");
        }

        
  }


  @Listen("onClick = #btnHome5")
    public void btnHome5_Clicked(Event event) {
        Executions.sendRedirect("HomeDataOffense.zul");
    
  }

  @Listen("onClick = #btnExit5")
    public void btnExit5_Clicked(Event event) {
        Executions.sendRedirect("LoginDataOffense.zul");
        txtOutputIdC.setValue("");
        txtOutputNameC.setValue(""); 

    
  }

  @Listen("onClick = #btnSubmit16")
    public void btnSubmit16_Clicked(Event event) {
        Executions.sendRedirect("register.zul");
    
  }

  @Listen("onClick = #btnSubmit17")
    public void btnSubmit17_Clicked(Event event) {
        Executions.sendRedirect("LoginBorderpass.zul");
    
  }

  @Listen("onClick = #btnSubmit18")
    public void btnSubmit18_Clicked(Event event) {
        Executions.sendRedirect("notity.zul");
    
  }
}
