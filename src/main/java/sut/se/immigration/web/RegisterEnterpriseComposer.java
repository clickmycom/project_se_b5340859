package sut.se.immigration.web;

import org.zkoss.zk.roo.annotations.ZKComposer;
import org.zkoss.zk.ui.Component;

import org.zkoss.*;

import org.zkoss.zk.ui.event.Event;

import org.zkoss.zk.ui.select.annotation.Listen;
import static org.zkoss.zk.ui.select.Selectors.find;
import org.zkoss.zul.*;
import javax.persistence.TypedQuery;
import org.zkoss.zk.ui.Executions;


import sut.se.immigration.entity.RegisterEnterprise;

@ZKComposer(zul = "RegisterEnterpriseIndex.zul")
public class RegisterEnterpriseComposer {

    private static final long serialVersionUID = -232468271600193740L;

    public void afterCompose(Component comp) {
    }


    @Listen("onClick = #btn_submit") // ประเมินพันกงาน
    public void btn_submit_Clicked(Event event) {
    	
			RegisterEnterprise t = new RegisterEnterprise();

            t.setRegister_name(register_name.getText());
            t.setRegister_position(register_position.getText());
            t.setRegister_email(register_email.getText());
            if(register_sex.getSelectedIndex() == 0){
                	t.setRegister_sex("Male");
        	}
       		if(register_sex.getSelectedIndex() == 1){
                	t.setRegister_sex("Female");
        	}
                t.setRegister_userid(register_userid.getText());
                t.setRegister_password(register_password.getText());
                t.setRegister_Location(register_Location.getText());
                t.setRegister_Type(register_Type.getText());
                t.setRegister_district(register_district.getText());
                t.setRegister_prefecture(register_prefecture.getText());
                t.setRegister_county(register_county.getText());
                t.setRegister_code(register_code.getText());
                t.setRegister_phone(register_phone.getText());
                t.setRegister_fax(register_fax.getText());
                t.setStateLogin("0");

            try {
            	alert("กำลังบันทึกข้อมูล");
            	t.persist();
            	Executions.sendRedirect("loginenterpriseindex.zul");
        	} catch(javax.validation.ConstraintViolationException e) {
           		alert("ไม่สามารถบันทึกข้อมูลได้");
        	}










    }




}
