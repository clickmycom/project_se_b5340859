package sut.se.immigration.web;

import org.zkoss.zk.roo.annotations.ZKComposer;
import org.zkoss.zk.ui.Component;
import org.zkoss.*;

import org.zkoss.zk.ui.event.Event;

import org.zkoss.zk.ui.select.annotation.Listen;
import static org.zkoss.zk.ui.select.Selectors.find;
import org.zkoss.zul.*;

import org.zkoss.zk.ui.Executions; 
import javax.persistence.TypedQuery;
import org.zkoss.lang.Threads;
import org.zkoss.zk.ui.util.Clients;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.lang.Exception;
import java.lang.Object;

import java.util.*;


import sut.se.immigration.entity.BorderpassData;

@ZKComposer(zul = "LoginBorderpass.zul")
public class LoginBorderpassComposser {

    private static final long serialVersionUID = -2604741170516070786L;

    public void afterCompose(Component comp) {
    }

    

    @Listen("onClick = #btnSubmit11")
    public void btnSubmit11_Clicked(Event event) {

        try{
    	Textbox txtid = (Textbox)find(getPage(),"#txtid").get(0);
    	Textbox txtname = (Textbox)find(getPage(),"#txtname").get(0);
    	Textbox txtlast = (Textbox)find(getPage(),"#txtlast").get(0);
    	Textbox txtoc = (Textbox)find(getPage(),"#txtoc").get(0);
    	Textbox cbxnat = (Textbox)find(getPage(),"#cbxnat").get(0);
    	Textbox txtdom = (Textbox)find(getPage(),"#txtdom").get(0);
    	Radiogroup rdosex = (Radiogroup)find(getPage(),"#rdosex").get(0);
        

    	BorderpassData t = new BorderpassData();
    	DateFormat datesave = new SimpleDateFormat("dd/MM/yyyy");
            t.setIdpersonal(txtid.getValue());
            t.setName(txtname.getValue());
            t.setLastname(txtlast.getValue());
            t.setOccupation(txtoc.getValue());
            t.setNationality(cbxnat.getValue());
            t.setDomicile(txtdom.getValue());
            t.setDatebirth(datesave.format(db1.getValue()));
            
            if(rdosex.getSelectedIndex() == 0){
        			
                	t.setSex("Male");
                	t.persist();

       		}
       		else{
                	
                	t.setSex("Female");
                	t.persist();
        	}

        
            t.persist();
  			alert("บันทึกข้อมูลเรียบร้อยแล้ว");

            txtid.setValue("");
        txtname.setValue("");
        txtlast.setValue("");
        txtoc.setValue("");
        cbxnat.setValue("");
        txtdom.setValue("");
       

    }
        catch(Exception e){
                alert("ท่านกรอกข้อมูลไม่ถูกต้องครบถ้วน กรุณากรอกข้อมูลให้ถูกต้องครบถ้วน");
            }
    

    }
  		
            



@Listen("onClick = #btnClear11")
    public void btnClear11_Clicked(Event event) {
    	txtid.setValue("");
    	txtname.setValue("");
    	txtlast.setValue("");
    	txtoc.setValue("");
    	cbxnat.setValue("");
    	txtdom.setValue("");
    }
@Listen("onClick = #tbSave")
    public void tbSave_Clicked(Event event) {
       Executions.sendRedirect("LoginBorderpass.zul");
}

@Listen("onClick = #tbEdit")
    public void tbEdit_Clicked(Event event) {
       Executions.sendRedirect("BorderpassSearch.zul");
}

}
