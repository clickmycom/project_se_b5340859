package sut.se.immigration.web;

import org.zkoss.zk.roo.annotations.ZKComposer;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import static org.zkoss.zk.ui.select.Selectors.find;
import org.zkoss.zul.*;


import org.zkoss.zul.Window;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;

import java.util.HashMap;

import org.zkoss.zk.ui.Executions;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlNativeComponent;
import org.zkoss.zul.Datebox;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import org.zkoss.zk.ui.Sessions;

import sut.se.immigration.entity.RegisterEnterprise;
import sut.se.immigration.entity.Workers;

import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Row;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Timer;

@ZKComposer(zul = "EnterpriseIndex.zul")
public class EnterpriseComposer {

    private static final long serialVersionUID = -6204957722693997440L;
    String change1="";
    String change2="";
 
    public void afterCompose(Component comp) {
        String  value1= "";
         final HashMap<String, Object> map = (HashMap<String, Object>) Sessions.getCurrent().getAttribute("allmyvalues"); 
            Textbox fax_en = (Textbox) find(getPage(), "#fax_en").get(0);

            Textbox name_en = (Textbox) find(getPage(), "#name_en").get(0);
            Textbox type_en = (Textbox) find(getPage(), "#type_en").get(0);
            Textbox bon_en = (Textbox) find(getPage(), "#bon_en").get(0);
            Textbox aum_en = (Textbox) find(getPage(), "#aum_en").get(0);
            Textbox jun_en = (Textbox) find(getPage(), "#jun_en").get(0);
            Textbox code_en = (Textbox) find(getPage(), "#code_en").get(0);
            Textbox phone_en = (Textbox) find(getPage(), "#phone_en").get(0);

            Textbox namepersonnel = (Textbox) find(getPage(), "#namepersonnel").get(0);
            Textbox namepersonnel1 = (Textbox) find(getPage(), "#namepersonnel1").get(0);
            Textbox namepersonnel31 = (Textbox) find(getPage(), "#namepersonnel31").get(0);
            Textbox namepersonnel319 = (Textbox) find(getPage(), "#namepersonnel319").get(0);

            value1=(map.get("value1")+"");
            alert("ไม่พบข้อมูล");


            try{

            TypedQuery<RegisterEnterprise> aa = RegisterEnterprise.findRegisterEnterprisesByRegister_useridEquals(value1);
            RegisterEnterprise ab = aa.getSingleResult();
            
            name_en.setValue(ab.getRegister_Location());
            type_en.setValue(ab.getRegister_Type());

            bon_en.setValue(ab.getRegister_district());
            fax_en.setValue(ab.getRegister_fax());
            
            aum_en.setValue(ab.getRegister_prefecture());

            jun_en.setValue(ab.getRegister_county());
            code_en.setValue(ab.getRegister_code());
            phone_en.setValue(ab.getRegister_phone());



            namepersonnel.setValue(ab.getRegister_position());
            namepersonnel1.setValue(ab.getRegister_name());

             namepersonnel31.setValue(ab.getRegister_email());
             namepersonnel319.setValue(ab.getRegister_phone());


            }
            catch(Exception e){
                alert("ไม่พบข้อมูล");
            }
    }


    @Listen("onClick = #register")
    public void Register_Clicked(Event event) {
        Groupbox  gb1 = (Groupbox) find(getPage(), "#gb1").get(0);
        Groupbox  gb = (Groupbox) find(getPage(), "#gb").get(0);
        Groupbox  iconenter = (Groupbox) find(getPage(), "#iconenter").get(0);
        Groupbox  dataenterprise = (Groupbox) find(getPage(), "#dataenterprise").get(0);     
                    gb1.setOpen(false); 
                    gb.setOpen(false);
                     dataenterprise.setOpen(true);
                     iconenter.setOpen(true);
                    popup.close();            
    }



    // แก้ไข บุคคลต่างด้าว
    @Listen("onClick = #editpeople1")
    public void editpeoplehead_Clicked(Event event) {
        
        Textbox  checkincenter = (Textbox) find(getPage(), "#checkincenter").get(0);
        Textbox  checkoutcenter = (Textbox) find(getPage(), "#checkoutcenter").get(0);
        Textbox  register_position = (Textbox) find(getPage(), "#register_position").get(0);
        Textbox  register_number = (Textbox) find(getPage(), "#register_number").get(0);
        Textbox  register_name1 = (Textbox) find(getPage(), "#register_name1").get(0);
        Textbox  register_Mname = (Textbox) find(getPage(), "#register_Mname").get(0);
        Textbox  register_sex = (Textbox) find(getPage(), "#register_sex").get(0);
        Textbox  register_chad1 = (Textbox) find(getPage(), "#register_chad1").get(0);
        Textbox  register_relation1 = (Textbox) find(getPage(), "#register_relation1").get(0);

        Textbox  checkin = (Textbox) find(getPage(), "#checkin").get(0);
        
        

        Row  datein = (Row) find(getPage(), "#datein").get(0);
        Datebox  db00000 = (Datebox) find(getPage(), "#db00000").get(0);
        checkincenter.setWidth("50%");
        db00000.setVisible(true);

        // DateFormat datesave = new SimpleDateFormat("dd/MM/yyyy");


        // checkincenter.setValue(datesave.format(db00000.getValue()));

        Datebox  db00001 = (Datebox) find(getPage(), "#db00001").get(0);
        checkoutcenter.setWidth("50%");
        db00001.setVisible(true);





      
        // Datebox dateboxcheckin = new Datebox();
        // dateboxcheckin.setMold("rounded");
        // dateboxcheckin.setId("datecheckin");
        // alert(dateboxcheckin.getId());
        // find(getPage(), "#datein").get(0).appendChild(dateboxcheckin);
        
        // dateboxcheckin.setWidth("210px");
        // dateboxcheckin.setStyle("text-align:center");
        // dateboxcheckin.setFormat("dd/MM/yyyy");


        // Row  dateout = (Row) find(getPage(), "#dateout").get(0);

     



        
      
        


        checkincenter.setReadonly(true);
        checkoutcenter.setReadonly(true);
        register_position.setReadonly(false);
        register_number.setReadonly(false);
        register_name1.setReadonly(false);
        register_Mname.setReadonly(false);
        register_sex.setReadonly(false);
        register_chad1.setReadonly(false);
        register_relation1.setReadonly(false);

        //checkincenter.setStyle("text-align:center;font-weight:bold;border-style:outset; border-width:1px; border-color:red blue green black");
        //checkoutcenter.setStyle("text-align:center;font-weight:bold;border-style:outset; border-width:1px; border-color:red blue green black");
        register_position.setStyle("text-align:center;font-weight:bold;border-style:outset; border-width:1px; border-color:red blue green black");
        register_number.setStyle("text-align:center;font-weight:bold;border-style:outset; border-width:1px; border-color:red blue green black");
        register_name1.setStyle("text-align:center;font-weight:bold;border-style:outset; border-width:1px; border-color:red blue green black");
        register_Mname.setStyle("text-align:center;font-weight:bold;border-style:outset; border-width:1px; border-color:red blue green black");
        register_sex.setStyle("text-align:center;font-weight:bold;border-style:outset; border-width:1px; border-color:red blue green black");
        register_chad1.setStyle("text-align:center;font-weight:bold;border-style:outset; border-width:1px; border-color:red blue green black");
        register_relation1.setStyle("text-align:center;font-weight:bold;border-style:outset; border-width:1px; border-color:red blue green black");

    }

    // ส่วนของข้อมูลบุคคนต่างด้าว แก้ไข
    @Listen("onChange = #checkincenter")
    public void checkincenter_Clicked(Event event) {
        Textbox  checkincenter = (Textbox) find(getPage(), "#checkincenter").get(0);
        checkincenter.setStyle("text-align:center;font-weight:bold;border-style:outset; border-width:1px; border-color:red blue green black;color:red");
        Clients.showNotification(checkincenter.getValue(),null,checkincenter,null,1200);

    }

    //ถ้า Datebox Checkin มีการเปลี่ยนแแปลง
    @Listen("onChange = #db00000")
    public void dateCheckin_Clicked(Event event) {
        Textbox  checkincenter = (Textbox) find(getPage(), "#checkincenter").get(0);
        Datebox  db00000 = (Datebox) find(getPage(), "#db00000").get(0);
        DateFormat datesave = new SimpleDateFormat("dd/MM/yyyy");
        checkincenter.setValue(datesave.format(db00000.getValue()));

    }

      //ถ้า Datebox Checkout มีการเปลี่ยนแแปลง
    @Listen("onChange = #db00001")
    public void dateCheckout_Clicked(Event event) {
        Textbox  checkoutcenter = (Textbox) find(getPage(), "#checkoutcenter").get(0);
        Datebox  db00001 = (Datebox) find(getPage(), "#db00001").get(0);
        DateFormat datesave1 = new SimpleDateFormat("dd/MM/yyyy");
        checkoutcenter.setValue(datesave1.format(db00001.getValue()));

    }



    @Listen("onChange = #checkoutcenter")
    public void checkoutcenter_Clicked(Event event) {
        Textbox  checkoutcenter = (Textbox) find(getPage(), "#checkoutcenter").get(0);
        checkoutcenter.setStyle("text-align:center;font-weight:bold;border-style:outset; border-width:1px; border-color:red blue green black;color:red");
        Clients.showNotification(checkoutcenter.getValue(),null,checkoutcenter,null,1200);
    }

    @Listen("onChange = #register_position")
    public void register_position_Clicked(Event event) {
        change1="true";
        
        Textbox  register_position = (Textbox) find(getPage(), "#register_position").get(0);
        register_position.setStyle("text-align:center;font-weight:bold;border-style:outset; border-width:1px; border-color:red blue green black;color:red");
        Clients.showNotification(register_position.getValue(),null,register_position,null,1200);
    }


    @Listen("onChange = #register_number")
    public void register_number_Clicked(Event event) {
        change2="true";
        Textbox  register_number = (Textbox) find(getPage(), "#register_number").get(0);
        register_number.setStyle("text-align:center;font-weight:bold;border-style:outset; border-width:1px; border-color:red blue green black;color:red");
        Clients.showNotification(register_number.getValue(),null,register_number,null,1200);        
    }


    @Listen("onChange = #register_name1")
    public void register_name1_Clicked(Event event) {
        Textbox  register_name1 = (Textbox) find(getPage(), "#register_name1").get(0);
        register_name1.setStyle("text-align:center;font-weight:bold;border-style:outset; border-width:1px; border-color:red blue green black;color:red");
        Clients.showNotification(register_name1.getValue(),null,register_name1,null,1200);
    }


    @Listen("onChange = #register_Mname")
    public void register_Mname_Clicked(Event event) {
        Textbox  register_Mname = (Textbox) find(getPage(), "#register_Mname").get(0);
        register_Mname.setStyle("text-align:center;font-weight:bold;border-style:outset; border-width:1px; border-color:red blue green black;color:red");
        Clients.showNotification(register_Mname.getValue(),null,register_Mname,null,1200);
    }


    @Listen("onChange = #register_sex")
    public void register_sex_Clicked(Event event) {
        Textbox  register_sex = (Textbox) find(getPage(), "#register_sex").get(0);
        register_sex.setStyle("text-align:center;font-weight:bold;border-style:outset; border-width:1px; border-color:red blue green black;color:red");
        Clients.showNotification(register_sex.getValue(),null,register_sex,null,1200);
    }


    @Listen("onChange = #register_chad1")
    public void register_chad1_Clicked(Event event) {
        Textbox  register_chad1 = (Textbox) find(getPage(), "#register_chad1").get(0);
        register_chad1.setStyle("text-align:center;font-weight:bold;border-style:outset; border-width:1px; border-color:red blue green black;color:red");
        Clients.showNotification(register_chad1.getValue(),null,register_chad1,null,1200);

    }


    @Listen("onChange = #register_relation1")
    public void register_relation1_Clicked(Event event) {
        
        Textbox  register_relation1 = (Textbox) find(getPage(), "#register_relation1").get(0);
        register_relation1.setStyle("text-align:center;font-weight:bold;border-style:outset; border-width:1px; border-color:red blue green black;color:red");
        Clients.showNotification(register_relation1.getValue(),null,register_relation1,null,1200);
                                                    
    }


    @Listen("onClick = #btn_find")
    public void btn_find_Clicked(Event event) {
        Groupbox  gb1 = (Groupbox) find(getPage(), "#gb1").get(0);
        Groupbox  gb = (Groupbox) find(getPage(), "#gb").get(0);
        Groupbox  dataenterprise = (Groupbox) find(getPage(), "#dataenterprise").get(0); 
        Groupbox  iconenter = (Groupbox) find(getPage(), "#iconenter").get(0);
        Groupbox  menu01 = (Groupbox) find(getPage(), "#menu01").get(0);
        
                          

        Workers t = new Workers();
        Textbox find = (Textbox) find(getPage(), "#find").get(0);

        try{
            TypedQuery<Workers> alien = Workers.findWorkersesByRegister_numberEquals(find.getValue());
            Workers aliens = alien.getSingleResult();
            //ข้อมูลทางซ้าย
            checkin.setValue(aliens.getCheckin());
            checkout.setValue(aliens.getCheckout());
            register_name.setValue(aliens.getRegister_name());
            registerchad.setValue(aliens.getRegister_chad());
            registerrelation.setValue(aliens.getRegister_number());

            //ข้อมูลส่วนกลาง
            
            checkincenter.setValue(aliens.getCheckin());
            checkoutcenter.setValue(aliens.getCheckout());
            register_position.setValue(aliens.getRegister_position());
            register_number.setValue(aliens.getRegister_number());
            register_name1.setValue(aliens.getRegister_name());
            register_Mname.setValue(aliens.getRegister_Mname());
            register_sex.setValue(aliens.getRegister_sex());
            register_chad1.setValue(aliens.getRegister_chad());
            register_relation1.setValue(aliens.getRegister_relation());

                    gb1.setOpen(false); 
                    gb.setOpen(false);
                    iconenter.setOpen(false);
                    dataenterprise.setOpen(false);       
                    menu01.setOpen(true); 
                    find.setValue("");
            }
            catch(Exception e){
                Clients.showNotification("ไม่พบหมายเลขหนังสือเดินทาง ที่ระบุ", "error",find, "before_center", 2300);
            }


    }

    // ส่วนของข้อมูลบุคคนต่างด้าว แก้ไข จบ

    @Listen("onClick = #find")
    public void find_Clicked(Event event) {
        Textbox find = (Textbox) find(getPage(), "#find").get(0);
        find.setValue("");
        Clients.showNotification("กรุณาระบุ หมายเลขหนังสือเดินทาง 13 หลัก", "info",find, "before_center", 1900);
    }


    @Listen("onClick = #logout")
    public void logout_Clicked(Event event) {
        Executions.sendRedirect("loginenterpriseindex.zul");
    }

    @Listen("onClick = #searchall")
    public void searchall_Clicked(Event event) {
        Executions.sendRedirect("searchalienindex.zul");
    }


   @Listen("onClick = #savepeople")
    public void savepeople_Clicked(Event event) {

      Window window = (Window)Executions.createComponents(
                  "RegisterRelate.zul", null, null);
         window.doModal();

    }

    @Listen("onClick = #showalien")
    public void showalien_Clicked(Event event) {

    Executions.sendRedirect("allalienindex.zul");

    }



    @Listen("onClick = #editpeople")
    public void editpeople_Clicked(Event event) {

          Executions.sendRedirect("checkoutindex.zul");

    }



  @Listen("onClick = #deletepeople1")
    public void deletepeople1_Clicked(Event event) {

        Workers t = new Workers();
        Textbox register_number = (Textbox) find(getPage(), "#register_number").get(0);

        try{
            TypedQuery<Workers> alien = Workers.findWorkersesByRegister_numberEquals(register_number.getValue());
            Workers aliens = alien.getSingleResult();

            //ข้อมูลทางซ้าย
            checkin.setValue("");
            checkout.setValue("");
            register_name.setValue("");
            registerchad.setValue("");
            registerrelation.setValue("");

            //ข้อมูลส่วนกลาง
            
            checkincenter.setValue("");
            checkoutcenter.setValue("");
            register_position.setValue("");
            register_number.setValue("");
            register_name1.setValue("");
            register_Mname.setValue("");
            register_sex.setValue("");
            register_chad1.setValue("");
            register_relation1.setValue("");

            Clients.showNotification("ลบข้อมูลเรียบร้อยแล้ว","info",null, null, 1700);
            aliens.remove();
            }
            catch(Exception e){
                Clients.showNotification("ไม่สามารถลบข้อมูลนี้ได้","error",null, null, 1700);
            }

    }


    //แก้ไขแล้วบันทึกบุคคลต่างด้าว
    @Listen("onClick = #savepeople1") 
    public void savepeople1_Clicked(Event event) {
            
        Textbox registerrelation = (Textbox) find(getPage(), "#registerrelation").get(0);
        Textbox  checkincenter = (Textbox) find(getPage(), "#checkincenter").get(0);
        Textbox  checkoutcenter = (Textbox) find(getPage(), "#checkoutcenter").get(0);
        Textbox  register_position = (Textbox) find(getPage(), "#register_position").get(0);
        Textbox  register_number = (Textbox) find(getPage(), "#register_number").get(0);
        Textbox  register_name1 = (Textbox) find(getPage(), "#register_name1").get(0);
        Textbox  register_Mname = (Textbox) find(getPage(), "#register_Mname").get(0);
        Textbox  register_sex = (Textbox) find(getPage(), "#register_sex").get(0);
        Textbox  register_chad1 = (Textbox) find(getPage(), "#register_chad1").get(0);
        Textbox  register_relation1 = (Textbox) find(getPage(), "#register_relation1").get(0);

        Textbox  checkin = (Textbox) find(getPage(), "#checkin").get(0);
        Textbox  checkout = (Textbox) find(getPage(), "#checkout").get(0);
        Textbox  register_name = (Textbox) find(getPage(), "#register_name").get(0);
        Textbox  registerchad = (Textbox) find(getPage(), "#registerchad").get(0);
    
        Datebox  db00000 = (Datebox) find(getPage(), "#db00000").get(0);



        try{
            TypedQuery<Workers> alien = Workers.findWorkersesByRegister_numberEquals(registerrelation.getValue());
            Workers aliens = alien.getSingleResult();
            String pass1="true";
                
                for(Workers e:  Workers.findAllWorkerses()){
                    if(change1=="true"){
                        if(e.getRegister_position().equals(register_position.getValue())){
                            pass1="false";
                            Clients.showNotification("หมายเลข บัตรขาเข้าเลขที่ ซ้ำซ้อน","error",register_position,null,3000);
                        }
                    }
                    if(change2=="true"){
                        if(e.getRegister_number().equals(register_number.getValue())){
                            pass1="false";
                            Clients.showNotification("หมายเลข หนังสือเดินทาง ซ้ำซ้อน","error",register_number,null,1700);
                        }
                    }
                }

                    if(pass1=="true"){
                        
                        aliens.setCheckin(checkincenter.getValue());
                            if(checkoutcenter.getValue()!=""){
                                aliens.setCheckout(checkoutcenter.getValue());
                            }

                       
                        
                        
                        aliens.setRegister_position(register_position.getValue());
                        aliens.setRegister_number(register_number.getValue());
                        aliens.setRegister_name(register_name1.getValue());
                        aliens.setRegister_Mname(register_Mname.getValue());
                        aliens.setRegister_sex(register_sex.getValue());
                        aliens.setRegister_chad(register_chad1.getValue());
                        aliens.setRegister_relation(register_relation1.getValue());


                       
                        aliens.persist();

                        checkincenter.setReadonly(true);
                        checkoutcenter.setReadonly(true);
                        register_position.setReadonly(true);
                        register_number.setReadonly(true);
                        register_name1.setReadonly(true);
                        register_Mname.setReadonly(true);
                        register_sex.setReadonly(true);
                        register_chad1.setReadonly(true);
                        register_relation1.setReadonly(true);

                        checkincenter.setStyle("text-align:center;background-color:#FFE4B5");
                        checkoutcenter.setStyle("text-align:center;background-color:#FFE4B5");
                        register_position.setStyle("text-align:center;background-color:#FFE4B5");
                        register_number.setStyle("text-align:center;background-color:#FFE4B5");
                        register_name1.setStyle("text-align:center;background-color:#FFE4B5");
                        register_Mname.setStyle("text-align:center;background-color:#FFE4B5");
                        register_sex.setStyle("text-align:center;background-color:#FFE4B5");
                        register_chad1.setStyle("text-align:center;background-color:#FFE4B5");
                        register_relation1.setStyle("text-align:center;background-color:#FFE4B5");


                       
                          registerchad.setValue("");
                          checkin.setValue("");
                          checkout.setValue("");
                          register_name.setValue("");
                          registerrelation.setValue(""); 
                          checkincenter.setValue(""); 
                          checkoutcenter.setValue("");                     
                          register_position.setValue("");                     
                          register_number.setValue("");                     
                          register_name1.setValue("");  
                          register_Mname.setValue("");  
                          register_sex.setValue(""); 
                          register_chad1.setValue("");  
                          register_relation1.setValue(""); 


                       
                        Clients.showNotification("บันทึกการแก้ไข เรียบร้อยแล้ว", "info",null, null, 3100);

                          Row  datein = (Row) find(getPage(), "#datein").get(0);
                           
                            checkincenter.setWidth("99%");
                            db00000.setVisible(false);

        // DateFormat datesave = new SimpleDateFormat("dd/MM/yyyy");


        // checkincenter.setValue(datesave.format(db00000.getValue()));

                        Datebox  db00001 = (Datebox) find(getPage(), "#db00001").get(0);
                        checkoutcenter.setWidth("99%");
                        db00001.setVisible(false);


                        //org.zkoss.lang.Threads.sleep(1000);
                    //Executions.getCurrent().sendRedirect("http://foo.bar.baz:5984/bla?fasel/xyz", "_blank");   
                        
                    }
                
                         
            //ตั้งค่าให้มันกลับเป็นเหมือนเดิม


           
            }
            catch(Exception e){
                Clients.showNotification("ไม่สามารถแก้ไขข้อมูลนี้ได้","error",null, null, 1700);
                
            }

    }



     @Listen("onClick = #btn_home")
    public void btn_home_Clicked(Event event) {
        Textbox registerrelation = (Textbox) find(getPage(), "#registerrelation").get(0);
        Textbox  checkincenter = (Textbox) find(getPage(), "#checkincenter").get(0);
        Textbox  checkoutcenter = (Textbox) find(getPage(), "#checkoutcenter").get(0);
        Textbox  register_position = (Textbox) find(getPage(), "#register_position").get(0);
        Textbox  register_number = (Textbox) find(getPage(), "#register_number").get(0);
        Textbox  register_name1 = (Textbox) find(getPage(), "#register_name1").get(0);
        Textbox  register_Mname = (Textbox) find(getPage(), "#register_Mname").get(0);
        Textbox  register_sex = (Textbox) find(getPage(), "#register_sex").get(0);
        Textbox  register_chad1 = (Textbox) find(getPage(), "#register_chad1").get(0);
        Textbox  register_relation1 = (Textbox) find(getPage(), "#register_relation1").get(0);

        Textbox  checkin = (Textbox) find(getPage(), "#checkin").get(0);
        Textbox  checkout = (Textbox) find(getPage(), "#checkout").get(0);
        Textbox  register_name = (Textbox) find(getPage(), "#register_name").get(0);
        Textbox  registerchad = (Textbox) find(getPage(), "#registerchad").get(0);
    
        Datebox  db00000 = (Datebox) find(getPage(), "#db00000").get(0);

        Groupbox  gb1 = (Groupbox) find(getPage(), "#gb1").get(0);
        Groupbox  gb = (Groupbox) find(getPage(), "#gb").get(0);
             if (gb1.isOpen()&&gb.isOpen()){ 
                    gb1.setOpen(false); 
                    gb.setOpen(false);
                } 
            else { 
                    gb1.setOpen(true);
                    gb.setOpen(true); 
                }

        Groupbox  menu01 = (Groupbox) find(getPage(), "#menu01").get(0);
         menu01.setOpen(false);


         registerchad.setValue("");
                          checkin.setValue("");
                          checkout.setValue("");
                          register_name.setValue("");
                          registerrelation.setValue(""); 
                          checkincenter.setValue(""); 
                          checkoutcenter.setValue("");                     
                          register_position.setValue("");                     
                          register_number.setValue("");                     
                          register_name1.setValue("");  
                          register_Mname.setValue("");  
                          register_sex.setValue(""); 
                          register_chad1.setValue("");  
                          register_relation1.setValue(""); 

                       
                        
                        Row  datein = (Row) find(getPage(), "#datein").get(0);
                           
                        checkincenter.setWidth("99%");
                        db00000.setVisible(false);


                        Datebox  db00001 = (Datebox) find(getPage(), "#db00001").get(0);
                        checkoutcenter.setWidth("99%");
                        db00001.setVisible(false);


    }


    @Listen("onClick = #english")
    public void btn_english_Clicked(Event event) {

            Clients.showNotification("Use the English Language", "info",english, null, 1700);

            Toolbarbutton btn_find = (Toolbarbutton) find(getPage(), "#btn_find").get(0);
            btn_find.setLabel("Search");

            

            Toolbarbutton logout = (Toolbarbutton) find(getPage(), "#logout").get(0);
            logout.setLabel("  Log Out");

            Toolbarbutton savepeople1 = (Toolbarbutton) find(getPage(), "#savepeople1").get(0);
            savepeople1.setLabel("Recorded Alien");


            Toolbarbutton editpeople1 = (Toolbarbutton) find(getPage(), "#editpeople1").get(0);
            editpeople1.setLabel("Edit Aliens");


            Toolbarbutton deletepeople1 = (Toolbarbutton) find(getPage(), "#deletepeople1").get(0);
            deletepeople1.setLabel("Delete Aliens");

            Caption datahead = (Caption) find(getPage(), "#datahead").get(0);
            datahead.setLabel("User Information");
            
            //ส่วนของข้อมูลบริษัท
            Label namepersonnelEN = (Label) find(getPage(), "#namepersonnelEN").get(0);
            Label namepersonnel1s = (Label) find(getPage(), "#namepersonnel1s").get(0);
            Label namepersonnel311 = (Label) find(getPage(), "#namepersonnel311").get(0);
            Label namepersonnel3191 = (Label) find(getPage(), "#namepersonnel3191").get(0);

            namepersonnelEN.setValue("Position:");
            namepersonnel1s.setValue("Name:  ");
            namepersonnel311.setValue("E-Mail:");
            namepersonnel3191.setValue("Telephone");

            Label name_en1 = (Label) find(getPage(), "#name_en1").get(0);
            Label type_en1 = (Label) find(getPage(), "#type_en1").get(0);
            Label bon_en1 = (Label) find(getPage(), "#bon_en1").get(0);
            Label aum_en1 = (Label) find(getPage(), "#aum_en1").get(0);
            Label jun_en1 = (Label) find(getPage(), "#jun_en1").get(0);
            Label code_en1 = (Label) find(getPage(), "#code_en1").get(0);
            Label phone_en1 = (Label) find(getPage(), "#phone_en1").get(0);
            Label fax_en1 = (Label) find(getPage(), "#fax_en1").get(0);

            name_en1.setValue("Enterprise Name:   ");
            type_en1.setValue("Enterprise Type:             ");
            bon_en1.setValue("Sub Distric:");
            aum_en1.setValue("Distic     ");
            jun_en1.setValue("Province    ");
            code_en1.setValue("Post Code     ");
            phone_en1.setValue("Telephone Number   ");
            fax_en1.setValue("Fax Number   ");


            //ส่วนของข้อมูลบุคคลต่างด้าว
            Label db11 = (Label) find(getPage(), "#db11").get(0);
            db11.setValue("Check-In:");

            Label db23 = (Label) find(getPage(), "#db23").get(0);
            db23.setValue("Check-Out");

            Label registerchadEN = (Label) find(getPage(), "#registerchadEN").get(0);
            registerchadEN.setValue("Nationality:");

            Label registerrelationEN = (Label) find(getPage(), "#registerrelationEN").get(0);
            registerrelationEN.setValue("PassportNO");

            Label db2222 = (Label) find(getPage(), "#db2222").get(0);
            db2222.setValue("Name:");

            Label checkintextEN = (Label) find(getPage(), "#checkintextEN").get(0);
            checkintextEN.setValue("Check-In:");

            Label checkouttextEN = (Label) find(getPage(), "#checkouttextEN").get(0);
            checkouttextEN.setValue("CheckOut");

            Label registerposition = (Label) find(getPage(), "#registerposition").get(0);
            registerposition.setValue("TM6 NO:");

            Label registernumber = (Label) find(getPage(), "#registernumber").get(0);
            registernumber.setValue("Passport NO:");

             Label registername122 = (Label) find(getPage(), "#registername122").get(0);
             registername122.setValue("First Name Family Name:");

             Label registerMname = (Label) find(getPage(), "#registerMname").get(0);
             registerMname.setValue("Middle Name:");

             Label registersex = (Label) find(getPage(), "#registersex").get(0);
             registersex.setValue("Gender:");

             Label registerchad1 = (Label) find(getPage(), "#registerchad1").get(0);
             registerchad1.setValue("Nationality:");

             Label registerrelation13 = (Label) find(getPage(), "#registerrelation13").get(0);
             registerrelation13.setValue("Passport NO:");

    }

    @Listen("onClick = #thai")
    public void btn_thai_Clicked(Event event) {

            Clients.showNotification("ใช้ภาษาไทย", "info",thai, null, 1700);

            Toolbarbutton btn_find = (Toolbarbutton) find(getPage(), "#btn_find").get(0);
            btn_find.setLabel("ค้นหา");

           
            Toolbarbutton logout = (Toolbarbutton) find(getPage(), "#logout").get(0);
            logout.setLabel("  ออกจากระบบ");

            Toolbarbutton savepeople1 = (Toolbarbutton) find(getPage(), "#savepeople1").get(0);
            savepeople1.setLabel("บันทึกรับคนต่างด้าว");


            Toolbarbutton editpeople1 = (Toolbarbutton) find(getPage(), "#editpeople1").get(0);
            editpeople1.setLabel("แก้ไขข้อมูลคนต่างด้าว");


            Toolbarbutton deletepeople1 = (Toolbarbutton) find(getPage(), "#deletepeople1").get(0);
            deletepeople1.setLabel("ลบข้อมูลคนต่างด้าว");

            Caption datahead = (Caption) find(getPage(), "#datahead").get(0);
            datahead.setLabel("ข้อมูลผู้ขอใช้บริการ");
            
            //ส่วนของข้อมูลบริษัท
            Label namepersonnelEN = (Label) find(getPage(), "#namepersonnelEN").get(0);
            Label namepersonnel1s = (Label) find(getPage(), "#namepersonnel1s").get(0);
            Label namepersonnel311 = (Label) find(getPage(), "#namepersonnel311").get(0);
            Label namepersonnel3191 = (Label) find(getPage(), "#namepersonnel3191").get(0);

            namepersonnelEN.setValue("ตำแหน่ง:");
            namepersonnel1s.setValue("ชื่อ - สกุล:  ");
            namepersonnel311.setValue("เมลล์:");
            namepersonnel3191.setValue("เบอร์โทร");

            Label name_en1 = (Label) find(getPage(), "#name_en1").get(0);
            Label type_en1 = (Label) find(getPage(), "#type_en1").get(0);
            Label bon_en1 = (Label) find(getPage(), "#bon_en1").get(0);
            Label aum_en1 = (Label) find(getPage(), "#aum_en1").get(0);
            Label jun_en1 = (Label) find(getPage(), "#jun_en1").get(0);
            Label code_en1 = (Label) find(getPage(), "#code_en1").get(0);
            Label phone_en1 = (Label) find(getPage(), "#phone_en1").get(0);
            Label fax_en1 = (Label) find(getPage(), "#fax_en1").get(0);

            name_en1.setValue("ชื่อสถานประกอบการ :   ");
            type_en1.setValue("ประเภทสถานประกอบการ  :             ");
            bon_en1.setValue("ตำบล :");
            aum_en1.setValue("อำเภอ : ");
            jun_en1.setValue("จังหวัด :  ");
            code_en1.setValue("รหัสไปรษณีย์ :  ");
            phone_en1.setValue("โทรศัพท์ : ");
            fax_en1.setValue("แฟ็ก  :");


            //ส่วนของข้อมูลบุคคลต่างด้าว
            Label db11 = (Label) find(getPage(), "#db11").get(0);
            db11.setValue("วันที่เข้าพัก:");

            Label db23 = (Label) find(getPage(), "#db23").get(0);
            db23.setValue("วันที่ออก:");

            Label registerchadEN = (Label) find(getPage(), "#registerchadEN").get(0);
            registerchadEN.setValue("สัญชาติ:");

            Label registerrelationEN = (Label) find(getPage(), "#registerrelationEN").get(0);
            registerrelationEN.setValue("หนังสือเดินทาง:");

            Label db2222 = (Label) find(getPage(), "#db2222").get(0);
            db2222.setValue("ชื่อ - สกุล:");

            Label checkintextEN = (Label) find(getPage(), "#checkintextEN").get(0);
            checkintextEN.setValue("วันที่เข้าพัก : ");

            Label checkouttextEN = (Label) find(getPage(), "#checkouttextEN").get(0);
            checkouttextEN.setValue("วันที่ออก : ");

            Label registerposition = (Label) find(getPage(), "#registerposition").get(0);
            registerposition.setValue("บัตรขาเข้าเลขที่ : ");

            Label registernumber = (Label) find(getPage(), "#registernumber").get(0);
            registernumber.setValue("หมายเลขหนังสือเดินทาง :");

             Label registername122 = (Label) find(getPage(), "#registername122").get(0);
             registername122.setValue("ชื่อ - สกุล :");

             Label registerMname = (Label) find(getPage(), "#registerMname").get(0);
             registerMname.setValue("ชื่อกลาง : ");

             Label registersex = (Label) find(getPage(), "#registersex").get(0);
             registersex.setValue("เพศ : ");

             Label registerchad1 = (Label) find(getPage(), "#registerchad1").get(0);
             registerchad1.setValue("สัญชาติ : ");

             Label registerrelation13 = (Label) find(getPage(), "#registerrelation13").get(0);
             registerrelation13.setValue("ความสัมพันธ์ :");

    }

}
