package sut.se.immigration.web;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.*;
import org.zkoss.zul.*;
import java.util.List;
import org.zkoss.zk.roo.annotations.ZKComposer;
import org.zkoss.zk.ui.Component;
import org.zkoss.*;
import org.zkoss.zk.roo.annotations.ZKComposer;

import org.zkoss.zk.ui.event.Event;
import static org.zkoss.zk.ui.select.Selectors.find;
import org.zkoss.zk.ui.select.annotation.Listen;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.*;
import org.zkoss.zul.*;
import java.util.List;
import org.zkoss.zk.ui.Executions;
import java.lang.Exception;
import java.util.List;
import sut.se.immigration.entity.Notify;
@ZKComposer(zul = "Notify3.zul")
public class Notify3Composer {

    private static final long serialVersionUID = 5049536823599281991L;

    public void afterCompose(Component comp) {

    }

   @Listen("onClick = #btn_find")
    public void btn_find_Click(Event event) {

      Textbox find = (Textbox) find(getPage(), "#find").get(0);
      Rows tabel = (Rows) find(getPage(), "#tabel").get(0);
        for(Component cv : find(getPage(), "#tabel > row")){
            cv.detach();
        }

      TypedQuery<Notify> aa = Notify.findNotifysByIdpassportLike(find.getValue());
      List<Notify> stdNow = aa.getResultList();
        for (Notify e0 : stdNow) {
          Row row1 =new Row();
          Label lb11= new Label(e0.getDdmmyystart()+"");
          Label lb12= new Label(e0.getDdmmyyent()+"");
          Label lb13= new Label(e0.getNameofficer()+"");
          Label lb14= new Label(e0.getName()+"");
    
          row1.appendChild(lb11);
          row1.appendChild(lb12);
          row1.appendChild(lb13);
          row1.appendChild(lb14);
          Button bt1 =new Button("ลบ");
          bt1.setId(e0.getIdpassport());
          bt1.setParent(row1);
          bt1.addEventListener("onClick", new EventListener(){

            public void onEvent(Event ee2) throws UiException {  

              TypedQuery<Notify> aa = Notify.findNotifysByIdpassportEquals(ee2.getTarget().getId()+"");
              Notify del = aa.getSingleResult();
            // alert(del.getName());
              del.remove();
              Executions.sendRedirect("Notify3.zul");

            }
          });
 
          row1.appendChild(bt1);
          tabel.appendChild(row1);
        }
      }
    }