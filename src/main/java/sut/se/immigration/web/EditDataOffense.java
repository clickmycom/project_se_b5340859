package sut.se.immigration.web;

import org.zkoss.zk.roo.annotations.ZKComposer;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zul.Doublebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.*;
import org.zkoss.zk.ui.event.*;
import static org.zkoss.zk.ui.select.Selectors.find;
import org.zkoss.zul.*;
import sut.se.immigration.entity.DataOffense;
import java.lang.*;
import java.io.*;
import java.util.*;
import javax.persistence.*;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.lang.Exception;
import java.lang.Object;
import org.zkoss.zul.Listbox;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import sut.se.immigration.entity.Personnal;
import sut.se.immigration.entity.AdminPersonnel;

@ZKComposer(zul = "EditDataOffense.zul")
public class EditDataOffense {

    private static final long serialVersionUID = -259253093763592396L;

    public void afterCompose(Component comp) {
      try{
      Session sessIdPersonnal =  Sessions.getCurrent();
      String id = String.valueOf(sessIdPersonnal.getAttribute("personnelid"));
      txtInputName3.setValue(id); 

      TypedQuery<Personnal> iduser = Personnal.findPersonnalsByIdPersonnal(txtInputName3.getValue());
                   Personnal user = iduser.getSingleResult();
                    txtInputName2.setValue(user.getNamePersonnal()); 

      }
     catch(Exception e){
            Session sessIdAdmin =  Sessions.getCurrent();
                    String id1 = String.valueOf(sessIdAdmin.getAttribute("adminid"));
                    txtInputName3.setValue(id1); 

            TypedQuery<AdminPersonnel> ad = AdminPersonnel.findAdminPersonnelsByIdAdmin(txtInputName3.getValue());
                    AdminPersonnel admin = ad.getSingleResult();
                    txtInputName2.setValue(admin.getNameAdmin());
     }   
                    
    }

    @Listen("onClick = #btnFind")
    public void btnFind_Clicked(Event event) {

      
      try{

             TypedQuery<DataOffense> dt = DataOffense.findDataOffensesByLawsuitId(find.getValue());
                    DataOffense ds = dt.getSingleResult(); 
                     alert("พบข้อมูลประวัติของผู้กระทำความผิดของคดีความเลขที่ " + " " + ds.getLawsuitId()); 
                       
                       Row row = new Row();
                       Label lb1 = new Label();
                       Label lb2 = new Label();
                       Label lb3 = new Label();
                       Label lb4 = new Label();
                       Label lb5 = new Label();

                       lb1.setValue(ds.getLawsuitId());
                       row.appendChild(lb1);
                       lb2.setValue(ds.getFirstName() + " " + ds.getLastName());
                       row.appendChild(lb2);
                       lb3.setValue(ds.getLawsuitName());
                       row.appendChild(lb3);
                       lb4.setValue(ds.getPrescription());
                       row.appendChild(lb4);
                       lb5.setValue(ds.getExpiratePrescription());
                       row.appendChild(lb5);
                       rshow.appendChild(row);
                       

      }
      catch(Exception e){
                alert("ไม่พบข้อมูลที่ท่านค้นหา กรุณาหมายเลขคดีใหม่");
                
            }
    }

    @Listen("onClick = #btnEdit3")
    public void btnEdit3_Clicked(Event event) {

      
        TypedQuery<DataOffense> dt = DataOffense.findDataOffensesByLawsuitId(find.getValue());
                    DataOffense ds = dt.getSingleResult();

                       txtOutput1.setValue(ds.getFirstName());
                       txtOutput2.setValue(ds.getLastName());
                       txtOutput3.setValue(ds.getIdentifyNumber());
                       txtOutput4.setValue(ds.getAge());
                       txtOutput5.setValue(ds.getBirthDate());
                       txtOutput6.setValue(ds.getNationality());
                       txtOutput7.setValue(ds.getAddress());
                       txtOutput8.setValue(ds.getLawsuitId());
                       txtOutput9.setValue(ds.getLawsuitName());
                       txtOutput11.setValue(ds.getPrescription());
                       txtOutput12.setValue(ds.getExpiratePrescription());
                       txtOutput13.setValue(ds.getLawsuitDetail());
                       txtOutput14.setValue(ds.getOwnerLawsuit());
                       
    }

    @Listen("onClick = #btnSave3")
    public void btnSave3_Clicked(Event event) {


      TypedQuery<DataOffense> dt = DataOffense.findDataOffensesByLawsuitId(find.getValue());
                    DataOffense ds = dt.getSingleResult();

         ds.setFirstName(txtOutput1.getValue());
                 ds.setLastName(txtOutput2.getValue());
                 ds.setIdentifyNumber(txtOutput3.getValue());
                 ds.setAge(txtOutput4.getValue());
                 ds.setBirthDate(txtOutput5.getValue());
                 ds.setNationality(txtOutput6.getValue());
                 ds.setAddress(txtOutput7.getValue());
                 ds.setLawsuitId(txtOutput8.getValue());
                 ds.setLawsuitName(txtOutput9.getValue());
                 ds.setPrescription(txtOutput11.getValue());
                 ds.setExpiratePrescription(txtOutput12.getValue());
                 ds.setLawsuitDetail(txtOutput13.getValue());
                 ds.setOwnerLawsuit(txtOutput14.getValue());
            try{
                ds.persist();
                alert("บันทึกการแก้ไขข้อมูลเรียบร้อยแล้ว");


            }
            catch(Exception t){
                alert("ไม่สามารถบันทึกการแก้ไขข้อมูลได้ กรุณากรอกข้อมูลให้ครบถ้วน");
            }
             find.setValue("");
    
    }

    @Listen("onClick = #btnReset3")
    public void btnReset3_Clicked(Event event) {

                       txtOutput1.setValue("");
                       txtOutput2.setValue("");
                       txtOutput3.setValue("");
                       txtOutput4.setValue("");
                       txtOutput5.setValue("");
                       txtOutput6.setValue("");
                       txtOutput7.setValue("");
                       txtOutput8.setValue("");
                       txtOutput9.setValue("");
                       txtOutput11.setValue("");
                       txtOutput12.setValue("");
                       txtOutput13.setValue("");
                       txtOutput14.setValue("");

    }
    
    @Listen("onClick = #btnHome")
    public void btnHome_Clicked(Event event) {
        Executions.sendRedirect("HomeDataOffense.zul");
    
  }

    @Listen("onClick = #btnSave")
    public void btnSave_Clicked(Event event) {
      Executions.sendRedirect("SaveDataOffense.zul");
    }

    @Listen("onClick = #btnDelete")
    public void btnEdit_Clicked(Event event) {
      Executions.sendRedirect("DeleteDataOffense.zul");
    }

    @Listen("onClick = #btnExit")
    public void btnExit_Clicked(Event event) {
      Executions.sendRedirect("LoginDataOffense.zul");
      txtInputName3.setValue("");
      txtInputName2.setValue(""); 
    }

    @Listen("onClick = #btnSubmit12")
    public void btnSubmit12_Clicked(Event event) {
        Executions.sendRedirect("register.zul");
    
  }

    @Listen("onClick = #btnSubmit13")
    public void btnSubmit13_Clicked(Event event) {
        Executions.sendRedirect("LoginBorderpass.zul");
    
  }

    @Listen("onClick = #btnSubmit14")
    public void btnSubmit14_Clicked(Event event) {
        Executions.sendRedirect("notity.zul");
    
  }

    @Listen("onClick = #btnSubmit15")
    public void btnSubmit15_Clicked(Event event) {
        Executions.sendRedirect("CheckDataOffense.zul");
    
  }
}
