package sut.se.immigration.web;

import org.zkoss.zk.roo.annotations.ZKComposer;
import org.zkoss.zk.ui.Component;

import org.zkoss.*;

import org.zkoss.zk.ui.event.Event;

import org.zkoss.zk.ui.select.annotation.Listen;
import static org.zkoss.zk.ui.select.Selectors.find;
import org.zkoss.zul.*;
import javax.persistence.TypedQuery;
import org.zkoss.zk.ui.Executions;

import sut.se.immigration.entity.Workers;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import org.zkoss.zk.ui.util.Clients;
import java.lang.String;
import org.zkoss.lang.Threads;

@ZKComposer(zul = "checkoutIndex.zul")
public class CheckoutComposer {

    private static final long serialVersionUID = 7082369540491289277L;

    public void afterCompose(Component comp) {

    }

    @Listen("onClick = #find001")
    public void find001_Clicked(Event event) {
        find001.setValue("");
    }

    @Listen("onClick = #home1")
    public void home1_Clicked(Event event) {
         Executions.sendRedirect("enterpriseindex.zul");
    }


    @Listen("onClick = #btn_find1")
    public void btn_find1_Clicked(Event event) {
     
        Groupbox  iconenter = (Groupbox) find(getPage(), "#iconenter").get(0);
     
                          

        Workers t = new Workers();
        Textbox find001 = (Textbox) find(getPage(), "#find001").get(0);

        try{
            TypedQuery<Workers> alien = Workers.findWorkersesByRegister_numberEquals(find001.getValue());
            Workers aliens = alien.getSingleResult();
            //ข้อมูลทางซ้าย
            checkin.setValue(aliens.getCheckin());
            
            register_name.setValue(aliens.getRegister_name());
            registerchad.setValue(aliens.getRegister_chad());
            registerrelation.setValue(aliens.getRegister_number());

                    iconenter.setOpen(true);
                    find001.setValue("");
            }
            catch(Exception e){
                Clients.showNotification("ไม่พบหมายเลขหนังสือเดินทาง ที่ระบุ", "error",find001, "before_center", 2300);
            }


    }

    @Listen("onClick = #btn_submit")
    public void btn_submit1_Clicked(Event event) {
         Workers t = new Workers();
         Textbox registerrelation = (Textbox) find(getPage(), "#registerrelation").get(0);
         Textbox checkin = (Textbox) find(getPage(), "#checkin").get(0);
         Textbox register_name = (Textbox) find(getPage(), "#register_name").get(0);
         Textbox registerchad = (Textbox) find(getPage(), "#registerchad").get(0);


          try{
            TypedQuery<Workers> alien = Workers.findWorkersesByRegister_numberEquals(registerrelation.getValue());
            Workers aliens = alien.getSingleResult();
            //ข้อมูลทางซ้าย
           
            
            //String str_login=Integer.toString(login_error);
            aliens.setCheckout(String.valueOf(db.getText().trim()));
           
            aliens.persist();
            Clients.showNotification("บันทึกข้อมูลเรียบร้อย", "info",null,null, 1700);

            checkin.setValue("");
            register_name.setValue("");
            registerchad.setValue("");
            registerrelation.setValue("");


            }
            catch(javax.validation.ConstraintViolationException e){
                Clients.showNotification("ไม่สามารถบันทึกข้อมูลได้", "error",null,null, 2300);
            }


    }





}
