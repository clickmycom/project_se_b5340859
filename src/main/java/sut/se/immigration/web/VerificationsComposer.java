package sut.se.immigration.web;

import org.zkoss.zk.roo.annotations.ZKComposer;
import org.zkoss.zk.ui.Component;
import org.zkoss.*;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import static org.zkoss.zk.ui.select.Selectors.find;
import org.zkoss.zul.*;
import javax.persistence.TypedQuery;
import org.zkoss.zk.ui.Executions;
import sut.se.immigration.entity.Register;
import org.zkoss.lang.Threads;

@ZKComposer(zul = "verification.zul")
public class VerificationsComposer {

    private static final long serialVersionUID = 2070323772043024820L;

    public void afterCompose(Component comp) {
    }

    @Listen("onClick = #aa1")
        public void aa1_Clicked(Event event) {
        Executions.getCurrent().sendRedirect("register.zul");
      
    }

    @Listen("onClick = #aa3")
    public void aa3_Clicked(Event event) {


    		 Textbox bb2 = (Textbox)find(getPage(),"#bb2").get(0);
    		 Textbox bb3 = (Textbox)find(getPage(),"#bb3").get(0);
             Textbox bb4 = (Textbox)find(getPage(),"#bb4").get(0);
             Textbox bb5 = (Textbox)find(getPage(),"#bb5").get(0);
             Textbox bb6 = (Textbox)find(getPage(),"#bb6").get(0);
             Textbox bb7 = (Textbox)find(getPage(),"#bb7").get(0);
             Textbox bb8 = (Textbox)find(getPage(),"#bb8").get(0);
             Textbox bb9 = (Textbox)find(getPage(),"#bb9").get(0);
             Textbox bb10 = (Textbox)find(getPage(),"#bb10").get(0);
             Textbox bb11 = (Textbox)find(getPage(),"#bb11").get(0);
             Textbox bb12 = (Textbox)find(getPage(),"#bb12").get(0);
             Textbox bb13 = (Textbox)find(getPage(),"#bb13").get(0);
             Textbox bb14 = (Textbox)find(getPage(),"#bb14").get(0);
             Textbox bb15 = (Textbox)find(getPage(),"#bb15").get(0);
             Textbox bb16 = (Textbox)find(getPage(),"#bb16").get(0);
             Textbox bb17 = (Textbox)find(getPage(),"#bb17").get(0);


            try{

            	TypedQuery<Register> dt = Register.findRegistersByIdregister(bb1.getText());
      			Register ds = dt.getSingleResult(); 

                 bb2.setValue(ds.getIdregister());
                 bb3.setValue(ds.getIdnumber());
                 bb4.setValue(ds.getFname());
                 bb5.setText(ds.getLname());
                 bb6.setText(ds.getRace());
                 bb7.setText(ds.getNationality());
                 bb8.setText(ds.getSex());
                 bb9.setText(ds.getDate());
                 bb10.setText(ds.getHigh());
                 bb11.setText(ds.getWeight());
                 bb12.setText(ds.getPassport());
                 bb13.setText(ds.getCounntry());
                 bb14.setText(ds.getCpassport());
                 bb15.setText( ds.getEpassport());
                 bb16.setText(ds.getVehicle());
                 bb17.setText(ds.getPhone());
            }
             catch(Exception e1213){
                alert("ไม่พบข้อมูล");
            }
      
    }


    // @Listen("onClick = #aa4")
    // public void aa4_Clicked(Event event) {
    		
    //          Textbox bb2 = (Textbox)find(getPage(),"#bb2").get(0);
    // 		 Textbox bb3 = (Textbox)find(getPage(),"#bb3").get(0);
    //          Textbox bb4 = (Textbox)find(getPage(),"#bb4").get(0);
    //          Textbox bb5 = (Textbox)find(getPage(),"#bb5").get(0);
    //          Textbox bb6 = (Textbox)find(getPage(),"#bb6").get(0);
    //          Textbox bb7 = (Textbox)find(getPage(),"#bb7").get(0);
    //          Textbox bb8 = (Textbox)find(getPage(),"#bb8").get(0);
    //          Textbox bb9 = (Textbox)find(getPage(),"#bb9").get(0);
    //          Textbox bb10 = (Textbox)find(getPage(),"#bb10").get(0);
    //          Textbox bb11 = (Textbox)find(getPage(),"#bb11").get(0);
    //          Textbox bb12 = (Textbox)find(getPage(),"#bb12").get(0);
    //          Textbox bb13 = (Textbox)find(getPage(),"#bb13").get(0);
    //          Textbox bb14 = (Textbox)find(getPage(),"#bb14").get(0);
    //          Textbox bb15 = (Textbox)find(getPage(),"#bb15").get(0);
    //          Textbox bb16 = (Textbox)find(getPage(),"#bb16").get(0);
    //          Textbox bb17 = (Textbox)find(getPage(),"#bb17").get(0);
    //          Textbox bb18 = (Textbox)find(getPage(),"#bb18").get(0);

    // 		Verifications d = new Verifications();
    //              d.setIdregister(bb2.getText());
    //              d.setIdnumber(bb3.getText());
    //              d.setFname(bb4.getText());
    //              d.setLname(bb5.getText());
    //              d.setRace(bb6.getText());
    //              d.setNationality(bb7.getText());
    //              d.setSex(bb8.getText());
    //              d.setDate(bb9.getText());
    //              d.setHigh(bb10.getText());
    //              d.setWeight(bb11.getText());
    //              d.setPassport(bb12.getText());
    //              d.setCounntry(bb13.getText());
    //              d.setCpassport(bb14.getText());
    //              d.setEpassport(bb15.getText());
    //              d.setVehicle(bb16.getText());
    //              d.setPhone(bb17.getText());
    //              d.setVerificationse(bb18.getText());

    //         try{
    //             d.persist();
    //           bb2.setValue("");
    //           bb3.setValue("");
    //           bb4.setValue("");
    //           bb5.setValue("");
    //           bb6.setValue("");
    //           bb7.setValue("");
    //           bb8.setValue("");
    //           bb9.setValue("");
    //           bb10.setValue("");
    //           bb11.setValue("");
    //           bb12.setValue("");
    //           bb13.setValue("");
    //           bb14.setValue("");
    //           bb15.setValue("");
    //           bb16.setValue("");
    //           bb17.setValue("");
    //           bb18.setValue("");


    //             alert("บันทึกข้อมูลเรียบร้อยแล้ว");

    //         }
    //         catch(Exception e){
    //             alert("ไม่สามารถบันทึกข้อมูลได้ กรุณากรอกข้อมูลให้ครบถ้วน");
    //         }

    // }
}
