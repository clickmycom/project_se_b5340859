package sut.se.immigration.web;

import org.zkoss.zk.roo.annotations.ZKComposer;
import org.zkoss.zk.ui.Component;

import org.zkoss.*;

import org.zkoss.zk.ui.event.Event;

import org.zkoss.zk.ui.select.annotation.Listen;
import static org.zkoss.zk.ui.select.Selectors.find;
import org.zkoss.zul.*;
import javax.persistence.TypedQuery;
import org.zkoss.zk.ui.Executions;

import sut.se.immigration.entity.Register;
import org.zkoss.lang.Threads;
@ZKComposer(zul = "register.zul")
public class RegisterComposer {

    private static final long serialVersionUID = 7271741177331122376L;

    public void afterCompose(Component comp) {
    		
    }

    @Listen("onClick = #Login1")
    public void Login1_Clicked(Event event) {

        Textbox l1 = (Textbox)find(getPage(),"#l1").get(0);
        Textbox l2 = (Textbox)find(getPage(),"#l2").get(0);
        Combobox l3 = (Combobox)find(getPage(),"#l3").get(0);
        
        if(l1.getValue() == "" || l2.getValue() == "" || l3.getValue() == ""){
            alert("ข้อมูลไม่ถูกต้อง");
        } 
        else{
            alert("ยินดีตอนรับ"+" "+l3.getValue()+" "+l2.getValue());
        }

        l1.setValue("");
        l2.setValue("");
        l3.setValue("");

    }

	@Listen("onClick = #Save1")
    public void Save1_Clicked(Event event) {
    		
             Textbox id1 = (Textbox)find(getPage(),"#id1").get(0);
             Textbox id2 = (Textbox)find(getPage(),"#id2").get(0);
             Textbox id3 = (Textbox)find(getPage(),"#id3").get(0);
             Textbox id4 = (Textbox)find(getPage(),"#id4").get(0);
             Textbox id5 = (Textbox)find(getPage(),"#id5").get(0);
             Textbox id6 = (Textbox)find(getPage(),"#id6").get(0);
             Textbox id7 = (Textbox)find(getPage(),"#id7").get(0);
             Textbox id8 = (Textbox)find(getPage(),"#id8").get(0);
             Textbox id9 = (Textbox)find(getPage(),"#id9").get(0);
             Textbox id10 = (Textbox)find(getPage(),"#id10").get(0);
             Textbox id11 = (Textbox)find(getPage(),"#id11").get(0);
             Textbox id12 = (Textbox)find(getPage(),"#id12").get(0);
             Textbox id13 = (Textbox)find(getPage(),"#id13").get(0);
             Textbox id14 = (Textbox)find(getPage(),"#id14").get(0);
             Textbox id15 = (Textbox)find(getPage(),"#id15").get(0);
             Textbox id16 = (Textbox)find(getPage(),"#id16").get(0);

    		Register d = new Register();
                 d.setIdregister(id1.getText());
                 d.setIdnumber(id2.getText());
                 d.setFname(id3.getText());
                 d.setLname(id4.getText());
                 d.setRace(id5.getText());
                 d.setNationality(id6.getText());
                 d.setSex(id7.getText());
                 //d.setDate(datesave.format(id8.getText());
                 d.setDate(id8.getText());
                 d.setHigh(id9.getText());
                 d.setWeight(id10.getText());
                 d.setPassport(id11.getText());
                 d.setCounntry(id12.getText());
                 d.setCpassport(id13.getText());
                 d.setEpassport(id14.getText());
                 // d.setCpassport(datesave.format(id13.getText());
                 // d.setEpassport(datesave.format(id14.getText());
                 d.setVehicle(id15.getText());
                 d.setPhone(id16.getText());

            try{
                d.persist();
                id1.setValue("");
              id2.setValue("");
              id3.setValue("");
              id4.setValue("");
              id5.setValue("");
              id6.setValue("");
              id7.setValue("");
              id8.setValue("");
              id9.setValue("");
              id10.setValue("");
              id11.setValue("");
              id12.setValue("");
              id13.setValue("");
              id14.setValue("");
              id15.setValue("");
              id16.setValue("");

                alert("บันทึกข้อมูลเรียบร้อยแล้ว");

            }
            catch(Exception e){
                alert("ไม่สามารถบันทึกข้อมูลได้ กรุณากรอกข้อมูลให้ครบถ้วน");
            }

    }

    @Listen("onClick = #Search1")
    public void Search1_Clicked(Event event) {


    		 Textbox ed0 = (Textbox)find(getPage(),"#ed0").get(0);
    		 Textbox ed1 = (Textbox)find(getPage(),"#ed1").get(0);
             Textbox ed2 = (Textbox)find(getPage(),"#ed2").get(0);
             Textbox ed3 = (Textbox)find(getPage(),"#ed3").get(0);
             Textbox ed4 = (Textbox)find(getPage(),"#ed4").get(0);
             Textbox ed5 = (Textbox)find(getPage(),"#ed5").get(0);
             Textbox ed6 = (Textbox)find(getPage(),"#ed6").get(0);
             Textbox ed7 = (Textbox)find(getPage(),"#ed7").get(0);
             Textbox ed8 = (Textbox)find(getPage(),"#ed8").get(0);
             Textbox ed9 = (Textbox)find(getPage(),"#ed9").get(0);
             Textbox ed10 = (Textbox)find(getPage(),"#ed10").get(0);
             Textbox ed11 = (Textbox)find(getPage(),"#ed11").get(0);
             Textbox ed12 = (Textbox)find(getPage(),"#ed12").get(0);
             Textbox ed13 = (Textbox)find(getPage(),"#ed13").get(0);
             Textbox ed14 = (Textbox)find(getPage(),"#ed14").get(0);
             Textbox ed15 = (Textbox)find(getPage(),"#ed15").get(0);
             Textbox ed16 = (Textbox)find(getPage(),"#ed16").get(0);


            try{

            	TypedQuery<Register> dt = Register.findRegistersByIdregister(ed0.getText());
      			Register ds = dt.getSingleResult(); 
      			//lert(ds.getFname());
                 ed1.setValue(ds.getIdregister());
                 
                 ed2.setValue(ds.getIdnumber());
                 ed3.setValue(ds.getFname());
                 ed4.setText(ds.getLname());
                 //ds.setLname(id4.getText());
                 ed5.setText(ds.getRace());
                 ed6.setText(ds.getNationality());
                 ed7.setText(ds.getSex());
                 ed8.setText(ds.getDate());
                 //ds.setDate(datesave.format(id8.getText());;
                 ed9.setText(ds.getHigh());
                 ed10.setText(ds.getWeight());
                 ed11.setText(ds.getPassport());
                 ed12.setText(ds.getCounntry());
                 ed13.setText(ds.getCpassport());
                 ed14.setText( ds.getEpassport());
                 // ds.setCpassport(datesave.format(id13.getText());;
                 // ds.setEpassport(datesave.format(id14.getText());;
                 ed15.setText(ds.getVehicle());
                 ed16.setText(ds.getPhone());
            }
             catch(Exception e1213){
                alert("ไม่พบข้อมูล");
            }
      
    }


    @Listen("onClick = #Update1")
    public void Update1_Clicked(Event event) {

    	
    	 Textbox ed0 = (Textbox)find(getPage(),"#ed0").get(0);
    		 Textbox ed1 = (Textbox)find(getPage(),"#ed1").get(0);
             Textbox ed2 = (Textbox)find(getPage(),"#ed2").get(0);
             Textbox ed3 = (Textbox)find(getPage(),"#ed3").get(0);
             Textbox ed4 = (Textbox)find(getPage(),"#ed4").get(0);
             Textbox ed5 = (Textbox)find(getPage(),"#ed5").get(0);
             Textbox ed6 = (Textbox)find(getPage(),"#ed6").get(0);
             Textbox ed7 = (Textbox)find(getPage(),"#ed7").get(0);
             Textbox ed8 = (Textbox)find(getPage(),"#ed8").get(0);
             Textbox ed9 = (Textbox)find(getPage(),"#ed9").get(0);
             Textbox ed10 = (Textbox)find(getPage(),"#ed10").get(0);
             Textbox ed11 = (Textbox)find(getPage(),"#ed11").get(0);
             Textbox ed12 = (Textbox)find(getPage(),"#ed12").get(0);
             Textbox ed13 = (Textbox)find(getPage(),"#ed13").get(0);
             Textbox ed14 = (Textbox)find(getPage(),"#ed14").get(0);
             Textbox ed15 = (Textbox)find(getPage(),"#ed15").get(0);
             Textbox ed16 = (Textbox)find(getPage(),"#ed16").get(0);
      TypedQuery<Register> dt = Register.findRegistersByIdregister(ed0.getText());
       Register ds = dt.getSingleResult(); 

       
                 ds.setIdregister(ed1.getText());
                 ds.setIdnumber(ed2.getText());
                 ds.setFname(ed3.getText());
                 ds.setLname(ed4.getText());
                 ds.setRace(ed5.getText());
                 ds.setNationality(ed6.getText());
                 ds.setSex(ed7.getText());
                 ds.setDate(ed8.getText());
                 //ds.setDate(datesave.format(id8.getText());;
                 ds.setHigh(ed9.getText());
                 ds.setWeight(ed10.getText());
                 ds.setPassport(ed11.getText());
                 ds.setCounntry(ed12.getText());
                 ds.setCpassport(ed13.getText());
                 //ds.setCpassport(datesave.format(id13.getText());;
                 ds.setEpassport(ed14.getText());
                 //ds.setEpassport(datesave.format(id14.getText());;
                 ds.setVehicle(ed15.getText());
                 ds.setPhone(ed16.getText());
            try{
                ds.persist();
	              ed1.setValue("");
	              ed2.setValue("");
	              ed3.setValue("");
	              ed4.setValue("");
	              ed5.setValue("");
	              ed6.setValue("");
	              ed7.setValue("");
	              ed8.setValue("");
	              ed9.setValue("");
	              ed10.setValue("");
	              ed11.setValue("");
	              ed12.setValue("");
	              ed13.setValue("");
	              ed14.setValue("");
	              ed15.setValue("");
	              ed16.setValue("");






                alert("บันทึกการแก้ไขข้อมูลเรียบร้อยแล้ว");

            }
            catch(Exception t){
                alert("ไม่สามารถบันทึกการแก้ไขข้อมูลได้ กรุณากรอกข้อมูลให้ครบถ้วน");
            }
 
    
    }

    @Listen("onClick = #Search0")
        public void Search0_Clicked(Event event) {
        Executions.getCurrent().sendRedirect("verification.zul");
      
    }


}
