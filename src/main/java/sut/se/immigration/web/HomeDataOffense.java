package sut.se.immigration.web;

import org.zkoss.zk.roo.annotations.ZKComposer;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zul.Doublebox;
import org.zkoss.zk.ui.Executions;
import static org.zkoss.zk.ui.select.Selectors.find;
import org.zkoss.zul.*;
import sut.se.immigration.entity.DataOffense;
import java.lang.*;
import java.io.*;
import java.util.*;
import javax.persistence.*;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.lang.Exception;
import java.lang.Object;
import org.zkoss.zul.Listbox;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.*;
import org.zkoss.zk.ui.event.*;
import sut.se.immigration.entity.Personnal;
import sut.se.immigration.entity.AdminPersonnel;

@ZKComposer(zul = "HomeDataOffense.zul")
public class HomeDataOffense {

    private static final long serialVersionUID = -5894129335418210923L;

    public void afterCompose(Component comp) {
      Component adminLayoutH = (Component)find(getPage(),"window > borderlayout > center > borderlayout > center > panel > panelchildren > #adminLayoutH").get(0);
      Component memberLayoutH = (Component)find(getPage(),"window > borderlayout > center > borderlayout > center > panel > panelchildren > #memberLayoutH").get(0);
      try{
        Session sessIdPersonnal =  Sessions.getCurrent();
      String id = String.valueOf(sessIdPersonnal.getAttribute("personnelid"));
      txtInputU5.setValue(id);
      memberLayoutH.setVisible(true);
      adminLayoutH.setVisible(false); 

      TypedQuery<Personnal> iduser = Personnal.findPersonnalsByIdPersonnal(txtInputU5.getValue());
                   Personnal user = iduser.getSingleResult();
                    txtInputN5.setValue(user.getNamePersonnal());

     }
     catch(Exception e){
            Session sessIdAdmin =  Sessions.getCurrent();
                    String id1 = String.valueOf(sessIdAdmin.getAttribute("adminid"));
                    txtInputU5.setValue(id1); 
                    adminLayoutH.setVisible(true);
                    memberLayoutH.setVisible(false);

            TypedQuery<AdminPersonnel> ad = AdminPersonnel.findAdminPersonnelsByIdAdmin(txtInputU5.getValue());
                    AdminPersonnel admin = ad.getSingleResult();
                    txtInputN5.setValue(admin.getNameAdmin());
     }                  

    }

    @Listen("onClick = #delete")
    public void delete_Clicked(Event event) {
        Executions.sendRedirect("DeleteDataOffense.zul");
    
    }

    @Listen("onClick = #save")
    public void save_Clicked(Event event) {
      Executions.sendRedirect("SaveDataOffense.zul");
    }

    @Listen("onClick = #edit")
    public void edit_Clicked(Event event) {
      Executions.sendRedirect("EditDataOffense.zul");
    }

    @Listen("onClick = #exit")
    public void exit_Clicked(Event event) {
      Executions.sendRedirect("LoginDataOffense.zul");
      txtInputU5.setValue("");
      txtInputN5.setValue("");
    }

    @Listen("onClick = #btnSubmit4")
    public void btnSubmit4_Clicked(Event event) {
        Executions.sendRedirect("register.zul");
    
  }

    @Listen("onClick = #btnSubmit5")
    public void btnSubmit5_Clicked(Event event) {
        Executions.sendRedirect("LoginBorderpass.zul");
    
  }

    @Listen("onClick = #btnSubmit6")
    public void btnSubmit6_Clicked(Event event) {
        Executions.sendRedirect("notity.zul");
    
  }

    @Listen("onClick = #btnSubmit7")
    public void btnSubmit7_Clicked(Event event) {
        Executions.sendRedirect("CheckDataOffense.zul");
    
  }
  @Listen("onClick = #exit123")
    public void exit123_Clicked(Event event) {
        Executions.sendRedirect("LoginDataOffense.zul");
        txtInputU5.setValue("");
      txtInputN5.setValue("");
    
  }
}
