// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package sut.se.immigration.entity;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import sut.se.immigration.entity.Workers;

privileged aspect Workers_Roo_Finder {
    
    public static TypedQuery<Workers> Workers.findWorkersesByCheckinLike(String Checkin) {
        if (Checkin == null || Checkin.length() == 0) throw new IllegalArgumentException("The Checkin argument is required");
        Checkin = Checkin.replace('*', '%');
        if (Checkin.charAt(0) != '%') {
            Checkin = "%" + Checkin;
        }
        if (Checkin.charAt(Checkin.length() - 1) != '%') {
            Checkin = Checkin + "%";
        }
        EntityManager em = Workers.entityManager();
        TypedQuery<Workers> q = em.createQuery("SELECT o FROM Workers AS o WHERE LOWER(o.Checkin) LIKE LOWER(:Checkin)", Workers.class);
        q.setParameter("Checkin", Checkin);
        return q;
    }
    
    public static TypedQuery<Workers> Workers.findWorkersesByCheckoutLike(String Checkout) {
        if (Checkout == null || Checkout.length() == 0) throw new IllegalArgumentException("The Checkout argument is required");
        Checkout = Checkout.replace('*', '%');
        if (Checkout.charAt(0) != '%') {
            Checkout = "%" + Checkout;
        }
        if (Checkout.charAt(Checkout.length() - 1) != '%') {
            Checkout = Checkout + "%";
        }
        EntityManager em = Workers.entityManager();
        TypedQuery<Workers> q = em.createQuery("SELECT o FROM Workers AS o WHERE LOWER(o.Checkout) LIKE LOWER(:Checkout)", Workers.class);
        q.setParameter("Checkout", Checkout);
        return q;
    }
    
    public static TypedQuery<Workers> Workers.findWorkersesByRegister_chadLike(String register_chad) {
        if (register_chad == null || register_chad.length() == 0) throw new IllegalArgumentException("The register_chad argument is required");
        register_chad = register_chad.replace('*', '%');
        if (register_chad.charAt(0) != '%') {
            register_chad = "%" + register_chad;
        }
        if (register_chad.charAt(register_chad.length() - 1) != '%') {
            register_chad = register_chad + "%";
        }
        EntityManager em = Workers.entityManager();
        TypedQuery<Workers> q = em.createQuery("SELECT o FROM Workers AS o WHERE LOWER(o.register_chad) LIKE LOWER(:register_chad)", Workers.class);
        q.setParameter("register_chad", register_chad);
        return q;
    }
    
    public static TypedQuery<Workers> Workers.findWorkersesByRegister_nameLike(String register_name) {
        if (register_name == null || register_name.length() == 0) throw new IllegalArgumentException("The register_name argument is required");
        register_name = register_name.replace('*', '%');
        if (register_name.charAt(0) != '%') {
            register_name = "%" + register_name;
        }
        if (register_name.charAt(register_name.length() - 1) != '%') {
            register_name = register_name + "%";
        }
        EntityManager em = Workers.entityManager();
        TypedQuery<Workers> q = em.createQuery("SELECT o FROM Workers AS o WHERE LOWER(o.register_name) LIKE LOWER(:register_name)", Workers.class);
        q.setParameter("register_name", register_name);
        return q;
    }
    
    public static TypedQuery<Workers> Workers.findWorkersesByRegister_numberEquals(String register_number) {
        if (register_number == null || register_number.length() == 0) throw new IllegalArgumentException("The register_number argument is required");
        EntityManager em = Workers.entityManager();
        TypedQuery<Workers> q = em.createQuery("SELECT o FROM Workers AS o WHERE o.register_number = :register_number", Workers.class);
        q.setParameter("register_number", register_number);
        return q;
    }
    
    public static TypedQuery<Workers> Workers.findWorkersesByRegister_numberLike(String register_number) {
        if (register_number == null || register_number.length() == 0) throw new IllegalArgumentException("The register_number argument is required");
        register_number = register_number.replace('*', '%');
        if (register_number.charAt(0) != '%') {
            register_number = "%" + register_number;
        }
        if (register_number.charAt(register_number.length() - 1) != '%') {
            register_number = register_number + "%";
        }
        EntityManager em = Workers.entityManager();
        TypedQuery<Workers> q = em.createQuery("SELECT o FROM Workers AS o WHERE LOWER(o.register_number) LIKE LOWER(:register_number)", Workers.class);
        q.setParameter("register_number", register_number);
        return q;
    }
    
    public static TypedQuery<Workers> Workers.findWorkersesByRegister_positionLike(String register_position) {
        if (register_position == null || register_position.length() == 0) throw new IllegalArgumentException("The register_position argument is required");
        register_position = register_position.replace('*', '%');
        if (register_position.charAt(0) != '%') {
            register_position = "%" + register_position;
        }
        if (register_position.charAt(register_position.length() - 1) != '%') {
            register_position = register_position + "%";
        }
        EntityManager em = Workers.entityManager();
        TypedQuery<Workers> q = em.createQuery("SELECT o FROM Workers AS o WHERE LOWER(o.register_position) LIKE LOWER(:register_position)", Workers.class);
        q.setParameter("register_position", register_position);
        return q;
    }
    
    public static TypedQuery<Workers> Workers.findWorkersesByRegister_sexLike(String register_sex) {
        if (register_sex == null || register_sex.length() == 0) throw new IllegalArgumentException("The register_sex argument is required");
        register_sex = register_sex.replace('*', '%');
        if (register_sex.charAt(0) != '%') {
            register_sex = "%" + register_sex;
        }
        if (register_sex.charAt(register_sex.length() - 1) != '%') {
            register_sex = register_sex + "%";
        }
        EntityManager em = Workers.entityManager();
        TypedQuery<Workers> q = em.createQuery("SELECT o FROM Workers AS o WHERE LOWER(o.register_sex) LIKE LOWER(:register_sex)", Workers.class);
        q.setParameter("register_sex", register_sex);
        return q;
    }
    
}
