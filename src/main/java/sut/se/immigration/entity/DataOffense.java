package sut.se.immigration.entity;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.validation.constraints.NotNull;
import javax.persistence.Column;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findDataOffensesByLawsuitId", "findDataOffensesByLawsuitIdEquals", "findDataOffensesByFirstName", "findDataOffensesByLastName", "findDataOffensesByIdentifyNumber" })
public class DataOffense {

    @NotNull
    @Size(min = 2)
    @Pattern(regexp = "[a-zA-Z]{2,30}")
    private String firstName;
    
    @NotNull
    @Size(min = 2)
    @Pattern(regexp = "[a-zA-Z]{2,30}")
    private String lastName;
    
    @NotNull
    @Size(min = 13)
    @Pattern(regexp = "[0-9]{13}")
    private String identifyNumber;

    @Size(min = 2)
    @Pattern(regexp = "[0-9]{2}")
    private String age;
    
    @Size(min = 2)
    private String birthDate;
    
    @NotNull
    @Size(min = 2)
    @Pattern(regexp = "[a-zA-Z]{2,40}")
    private String nationality;

    @Size(min = 2)
    private String address;
    
    @NotNull
    @Size(min = 8)
    @Pattern(regexp = "[N]\\d{7}")
    private String lawsuitId;
    
    @NotNull
    @Size(min = 2)
    @Pattern(regexp = "[a-zA-Z\\s|]{2,50}")
    private String lawsuitName;
    
    @Size(min = 2)
    private String prescription;
    
    
    @Size(min = 2)
    private String expiratePrescription;
    
    @Size(min = 2)
    private String lawsuitDetail;
    
    @NotNull
    @Size(min = 2)
    @Pattern(regexp = "[a-zA-Z\\s|]{2,30}")
    private String ownerLawsuit;

    @Size(min = 2)
    private String resultDataOffense;
}
