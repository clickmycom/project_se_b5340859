package sut.se.immigration.entity;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.NotNull;

import javax.validation.constraints.Size;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findNotifysByIdpassportEquals", "findNotifysByIdpassport", "findNotifysByIdpassportLike" })
public class Notify {


    
    @Pattern(regexp = "[0-9][0-9][/][0-9][0-9][/][0-9][0-9][0-9][0-9]")
    private String ddmmyystart;


    @Size(min = 2)
    @Pattern(regexp = "[0-9][0-9][/][0-9][0-9][/][0-9][0-9][0-9][0-9]")
    private String ddmmyyent;

    @NotNull
    @Size(min = 2)
    @Pattern(regexp = "[a-zA-Z.\\s|]{1,80}")
    private String name;

    @NotNull
    @Size(min = 2)
    @Pattern(regexp = "[a-zA-Z]{1,50}")
    private String nationality;

    @NotNull
    @Size(min = 2)
    @Pattern(regexp = "[0-9]{6}")
    private String visa;

    @Size(min = 2)
    private String byvehicle;

    @NotNull
    @Size(min = 2)
    @Pattern(regexp = "[A-Za-z]\\d{6}")
    private String idpassport;

    @NotNull
    @Size(min = 2)
   @Pattern(regexp = "[A-Za-z0-9]{8}")
    private String idarrival;

    @NotNull
    @Size(min = 2)
    @Pattern(regexp = "[a-zA-Z0-9./@\\s|]{1,300}")
    private String address;

    @NotNull
    @Pattern(regexp = "[0-9]{10}")
    private String phone;

    @NotNull
    @Size(min = 2)
    @Pattern(regexp = "[a-zA-Z.\\s|]{1,100}")
    private String nameofficer;
}
