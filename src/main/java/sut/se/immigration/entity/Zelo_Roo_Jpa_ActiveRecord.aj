// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package sut.se.immigration.entity;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;
import sut.se.immigration.entity.Zelo;

privileged aspect Zelo_Roo_Jpa_ActiveRecord {
    
    @PersistenceContext
    transient EntityManager Zelo.entityManager;
    
    public static final EntityManager Zelo.entityManager() {
        EntityManager em = new Zelo().entityManager;
        if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
        return em;
    }
    
    public static long Zelo.countZeloes() {
        return entityManager().createQuery("SELECT COUNT(o) FROM Zelo o", Long.class).getSingleResult();
    }
    
    public static List<Zelo> Zelo.findAllZeloes() {
        return entityManager().createQuery("SELECT o FROM Zelo o", Zelo.class).getResultList();
    }
    
    public static Zelo Zelo.findZelo(Long id) {
        if (id == null) return null;
        return entityManager().find(Zelo.class, id);
    }
    
    public static List<Zelo> Zelo.findZeloEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM Zelo o", Zelo.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    @Transactional
    public void Zelo.persist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }
    
    @Transactional
    public void Zelo.remove() {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            Zelo attached = Zelo.findZelo(this.id);
            this.entityManager.remove(attached);
        }
    }
    
    @Transactional
    public void Zelo.flush() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.flush();
    }
    
    @Transactional
    public void Zelo.clear() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.clear();
    }
    
    @Transactional
    public Zelo Zelo.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        Zelo merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
}
