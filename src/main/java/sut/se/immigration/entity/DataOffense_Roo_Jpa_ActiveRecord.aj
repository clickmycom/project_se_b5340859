// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package sut.se.immigration.entity;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;
import sut.se.immigration.entity.DataOffense;

privileged aspect DataOffense_Roo_Jpa_ActiveRecord {
    
    @PersistenceContext
    transient EntityManager DataOffense.entityManager;
    
    public static final EntityManager DataOffense.entityManager() {
        EntityManager em = new DataOffense().entityManager;
        if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
        return em;
    }
    
    public static long DataOffense.countDataOffenses() {
        return entityManager().createQuery("SELECT COUNT(o) FROM DataOffense o", Long.class).getSingleResult();
    }
    
    public static List<DataOffense> DataOffense.findAllDataOffenses() {
        return entityManager().createQuery("SELECT o FROM DataOffense o", DataOffense.class).getResultList();
    }
    
    public static DataOffense DataOffense.findDataOffense(Long id) {
        if (id == null) return null;
        return entityManager().find(DataOffense.class, id);
    }
    
    public static List<DataOffense> DataOffense.findDataOffenseEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM DataOffense o", DataOffense.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    @Transactional
    public void DataOffense.persist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }
    
    @Transactional
    public void DataOffense.remove() {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            DataOffense attached = DataOffense.findDataOffense(this.id);
            this.entityManager.remove(attached);
        }
    }
    
    @Transactional
    public void DataOffense.flush() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.flush();
    }
    
    @Transactional
    public void DataOffense.clear() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.clear();
    }
    
    @Transactional
    public DataOffense DataOffense.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        DataOffense merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
}
