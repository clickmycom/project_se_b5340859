package sut.se.immigration.entity;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findAdminPersonnelsByIdAdmin", "findAdminPersonnelsByIdAdminEquals", "findAdminPersonnelsByPassAdminEquals", "findAdminPersonnelsByPassAdmin" })
public class AdminPersonnel {

    @NotNull
    @Size(min = 10, max = 10)
    private String IdAdmin;

    @NotNull
    @Size(min = 2)
    private String nameAdmin;

    @NotNull
    @Size(min = 8, max = 16)
    private String passAdmin;
}
