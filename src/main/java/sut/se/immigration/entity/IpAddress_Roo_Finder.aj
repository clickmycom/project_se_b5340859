// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package sut.se.immigration.entity;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import sut.se.immigration.entity.IpAddress;

privileged aspect IpAddress_Roo_Finder {
    
    public static TypedQuery<IpAddress> IpAddress.findIpAddressesByIpEquals(String ip) {
        if (ip == null || ip.length() == 0) throw new IllegalArgumentException("The ip argument is required");
        EntityManager em = IpAddress.entityManager();
        TypedQuery<IpAddress> q = em.createQuery("SELECT o FROM IpAddress AS o WHERE o.ip = :ip", IpAddress.class);
        q.setParameter("ip", ip);
        return q;
    }
    
}
