// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package sut.se.immigration.entity;

import sut.se.immigration.entity.Verifications;

privileged aspect Verifications_Roo_JavaBean {
    
    public String Verifications.getIdregister() {
        return this.idregister;
    }
    
    public void Verifications.setIdregister(String idregister) {
        this.idregister = idregister;
    }
    
    public String Verifications.getIdnumber() {
        return this.idnumber;
    }
    
    public void Verifications.setIdnumber(String idnumber) {
        this.idnumber = idnumber;
    }
    
    public String Verifications.getFname() {
        return this.fname;
    }
    
    public void Verifications.setFname(String fname) {
        this.fname = fname;
    }
    
    public String Verifications.getLname() {
        return this.lname;
    }
    
    public void Verifications.setLname(String lname) {
        this.lname = lname;
    }
    
    public String Verifications.getRace() {
        return this.race;
    }
    
    public void Verifications.setRace(String race) {
        this.race = race;
    }
    
    public String Verifications.getNationality() {
        return this.nationality;
    }
    
    public void Verifications.setNationality(String nationality) {
        this.nationality = nationality;
    }
    
    public String Verifications.getSex() {
        return this.sex;
    }
    
    public void Verifications.setSex(String sex) {
        this.sex = sex;
    }
    
    public String Verifications.getDate() {
        return this.date;
    }
    
    public void Verifications.setDate(String date) {
        this.date = date;
    }
    
    public String Verifications.getHigh() {
        return this.high;
    }
    
    public void Verifications.setHigh(String high) {
        this.high = high;
    }
    
    public String Verifications.getWeight() {
        return this.weight;
    }
    
    public void Verifications.setWeight(String weight) {
        this.weight = weight;
    }
    
    public String Verifications.getPassport() {
        return this.passport;
    }
    
    public void Verifications.setPassport(String passport) {
        this.passport = passport;
    }
    
    public String Verifications.getCounntry() {
        return this.counntry;
    }
    
    public void Verifications.setCounntry(String counntry) {
        this.counntry = counntry;
    }
    
    public String Verifications.getCpassport() {
        return this.cpassport;
    }
    
    public void Verifications.setCpassport(String cpassport) {
        this.cpassport = cpassport;
    }
    
    public String Verifications.getEpassport() {
        return this.epassport;
    }
    
    public void Verifications.setEpassport(String epassport) {
        this.epassport = epassport;
    }
    
    public String Verifications.getVehicle() {
        return this.vehicle;
    }
    
    public void Verifications.setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }
    
    public String Verifications.getPhone() {
        return this.phone;
    }
    
    public void Verifications.setPhone(String phone) {
        this.phone = phone;
    }
    
    public String Verifications.getVerificationse() {
        return this.verificationse;
    }
    
    public void Verifications.setVerificationse(String verificationse) {
        this.verificationse = verificationse;
    }
    
}
