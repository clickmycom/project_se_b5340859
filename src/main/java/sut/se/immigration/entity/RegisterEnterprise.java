package sut.se.immigration.entity;

import javax.validation.constraints.Size;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findRegisterEnterprisesByRegister_useridEquals", "findRegisterEnterprisesByRegister_passwordEquals", "findRegisterEnterprisesByRegister_LocationEquals", "findRegisterEnterprisesByRegister_TypeEquals", "findRegisterEnterprisesByRegister_codeEquals", "findRegisterEnterprisesByRegister_countyEquals", "findRegisterEnterprisesByRegister_districtEquals", "findRegisterEnterprisesByRegister_emailEquals", "findRegisterEnterprisesByRegister_faxEquals", "findRegisterEnterprisesByRegister_nameEquals", "findRegisterEnterprisesByRegister_phoneEquals", "findRegisterEnterprisesByRegister_positionEquals", "findRegisterEnterprisesByRegister_prefectureEquals", "findRegisterEnterprisesByStateLoginEquals", "findRegisterEnterprisesByIpEquals" })
public class RegisterEnterprise {

    @Size(min = 2)
    private String register_userid;

    @Size(min = 2)
    private String register_password;

    @Size(min = 5)
    private String register_Location;

    @Size(min = 5)
    private String register_Type;

    @Size(min = 5)
    private String register_district;

    @Size(min = 5)
    private String register_prefecture;

    @Size(min = 5)
    private String register_county;

    @Size(min = 5)
    private String register_code;

    @Size(min = 5)
    private String register_phone;

    @Size(min = 5)
    private String register_fax;

    @Size(min = 1)
    private String register_sex;

    @Size(min = 1)
    private String register_email;

    @Size(min = 5)
    private String register_name;

    @Size(min = 5)
    private String register_position;

    private String StateLogin;

    @Size(min = 2)
    private String Ip;
}
