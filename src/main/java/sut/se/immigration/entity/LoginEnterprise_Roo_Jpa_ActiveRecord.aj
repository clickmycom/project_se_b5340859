// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package sut.se.immigration.entity;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;
import sut.se.immigration.entity.LoginEnterprise;

privileged aspect LoginEnterprise_Roo_Jpa_ActiveRecord {
    
    @PersistenceContext
    transient EntityManager LoginEnterprise.entityManager;
    
    public static final EntityManager LoginEnterprise.entityManager() {
        EntityManager em = new LoginEnterprise().entityManager;
        if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
        return em;
    }
    
    public static long LoginEnterprise.countLoginEnterprises() {
        return entityManager().createQuery("SELECT COUNT(o) FROM LoginEnterprise o", Long.class).getSingleResult();
    }
    
    public static List<LoginEnterprise> LoginEnterprise.findAllLoginEnterprises() {
        return entityManager().createQuery("SELECT o FROM LoginEnterprise o", LoginEnterprise.class).getResultList();
    }
    
    public static LoginEnterprise LoginEnterprise.findLoginEnterprise(Long id) {
        if (id == null) return null;
        return entityManager().find(LoginEnterprise.class, id);
    }
    
    public static List<LoginEnterprise> LoginEnterprise.findLoginEnterpriseEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM LoginEnterprise o", LoginEnterprise.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    @Transactional
    public void LoginEnterprise.persist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }
    
    @Transactional
    public void LoginEnterprise.remove() {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            LoginEnterprise attached = LoginEnterprise.findLoginEnterprise(this.id);
            this.entityManager.remove(attached);
        }
    }
    
    @Transactional
    public void LoginEnterprise.flush() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.flush();
    }
    
    @Transactional
    public void LoginEnterprise.clear() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.clear();
    }
    
    @Transactional
    public LoginEnterprise LoginEnterprise.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        LoginEnterprise merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
}
