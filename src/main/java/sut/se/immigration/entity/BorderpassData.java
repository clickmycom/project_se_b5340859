package sut.se.immigration.entity;

import javax.validation.constraints.Size;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findBorderpassDatasByIdpersonalEquals", "findBorderpassDatasByIdpersonal" })
public class BorderpassData {

    @Size(min = 2)
    private String idpersonal;

    @Size(min = 2)
    private String name;

    @Size(min = 2)
    private String lastname;

    @Size(min = 2)
    private String occupation;

    @Size(min = 2)
    private String nationality;

    @Size(min = 2)
    private String datebirth;

    @Size(min = 2)
    private String domicile;

    @Size(min = 2)
    private String sex;
}
