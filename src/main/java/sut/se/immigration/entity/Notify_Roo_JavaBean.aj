// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package sut.se.immigration.entity;

import sut.se.immigration.entity.Notify;

privileged aspect Notify_Roo_JavaBean {
    
    public String Notify.getDdmmyystart() {
        return this.ddmmyystart;
    }
    
    public void Notify.setDdmmyystart(String ddmmyystart) {
        this.ddmmyystart = ddmmyystart;
    }
    
    public String Notify.getDdmmyyent() {
        return this.ddmmyyent;
    }
    
    public void Notify.setDdmmyyent(String ddmmyyent) {
        this.ddmmyyent = ddmmyyent;
    }
    
    public String Notify.getName() {
        return this.name;
    }
    
    public void Notify.setName(String name) {
        this.name = name;
    }
    
    public String Notify.getNationality() {
        return this.nationality;
    }
    
    public void Notify.setNationality(String nationality) {
        this.nationality = nationality;
    }
    
    public String Notify.getVisa() {
        return this.visa;
    }
    
    public void Notify.setVisa(String visa) {
        this.visa = visa;
    }
    
    public String Notify.getByvehicle() {
        return this.byvehicle;
    }
    
    public void Notify.setByvehicle(String byvehicle) {
        this.byvehicle = byvehicle;
    }
    
    public String Notify.getIdpassport() {
        return this.idpassport;
    }
    
    public void Notify.setIdpassport(String idpassport) {
        this.idpassport = idpassport;
    }
    
    public String Notify.getIdarrival() {
        return this.idarrival;
    }
    
    public void Notify.setIdarrival(String idarrival) {
        this.idarrival = idarrival;
    }
    
    public String Notify.getAddress() {
        return this.address;
    }
    
    public void Notify.setAddress(String address) {
        this.address = address;
    }
    
    public String Notify.getPhone() {
        return this.phone;
    }
    
    public void Notify.setPhone(String phone) {
        this.phone = phone;
    }
    
    public String Notify.getNameofficer() {
        return this.nameofficer;
    }
    
    public void Notify.setNameofficer(String nameofficer) {
        this.nameofficer = nameofficer;
    }
    
}
