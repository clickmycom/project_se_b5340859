// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package sut.se.immigration.entity;

import sut.se.immigration.entity.RegisterEnterprise;

privileged aspect RegisterEnterprise_Roo_JavaBean {
    
    public String RegisterEnterprise.getRegister_userid() {
        return this.register_userid;
    }
    
    public void RegisterEnterprise.setRegister_userid(String register_userid) {
        this.register_userid = register_userid;
    }
    
    public String RegisterEnterprise.getRegister_password() {
        return this.register_password;
    }
    
    public void RegisterEnterprise.setRegister_password(String register_password) {
        this.register_password = register_password;
    }
    
    public String RegisterEnterprise.getRegister_Location() {
        return this.register_Location;
    }
    
    public void RegisterEnterprise.setRegister_Location(String register_Location) {
        this.register_Location = register_Location;
    }
    
    public String RegisterEnterprise.getRegister_Type() {
        return this.register_Type;
    }
    
    public void RegisterEnterprise.setRegister_Type(String register_Type) {
        this.register_Type = register_Type;
    }
    
    public String RegisterEnterprise.getRegister_district() {
        return this.register_district;
    }
    
    public void RegisterEnterprise.setRegister_district(String register_district) {
        this.register_district = register_district;
    }
    
    public String RegisterEnterprise.getRegister_prefecture() {
        return this.register_prefecture;
    }
    
    public void RegisterEnterprise.setRegister_prefecture(String register_prefecture) {
        this.register_prefecture = register_prefecture;
    }
    
    public String RegisterEnterprise.getRegister_county() {
        return this.register_county;
    }
    
    public void RegisterEnterprise.setRegister_county(String register_county) {
        this.register_county = register_county;
    }
    
    public String RegisterEnterprise.getRegister_code() {
        return this.register_code;
    }
    
    public void RegisterEnterprise.setRegister_code(String register_code) {
        this.register_code = register_code;
    }
    
    public String RegisterEnterprise.getRegister_phone() {
        return this.register_phone;
    }
    
    public void RegisterEnterprise.setRegister_phone(String register_phone) {
        this.register_phone = register_phone;
    }
    
    public String RegisterEnterprise.getRegister_fax() {
        return this.register_fax;
    }
    
    public void RegisterEnterprise.setRegister_fax(String register_fax) {
        this.register_fax = register_fax;
    }
    
    public String RegisterEnterprise.getRegister_sex() {
        return this.register_sex;
    }
    
    public void RegisterEnterprise.setRegister_sex(String register_sex) {
        this.register_sex = register_sex;
    }
    
    public String RegisterEnterprise.getRegister_email() {
        return this.register_email;
    }
    
    public void RegisterEnterprise.setRegister_email(String register_email) {
        this.register_email = register_email;
    }
    
    public String RegisterEnterprise.getRegister_name() {
        return this.register_name;
    }
    
    public void RegisterEnterprise.setRegister_name(String register_name) {
        this.register_name = register_name;
    }
    
    public String RegisterEnterprise.getRegister_position() {
        return this.register_position;
    }
    
    public void RegisterEnterprise.setRegister_position(String register_position) {
        this.register_position = register_position;
    }
    
    public String RegisterEnterprise.getStateLogin() {
        return this.StateLogin;
    }
    
    public void RegisterEnterprise.setStateLogin(String StateLogin) {
        this.StateLogin = StateLogin;
    }
    
    public String RegisterEnterprise.getIp() {
        return this.Ip;
    }
    
    public void RegisterEnterprise.setIp(String Ip) {
        this.Ip = Ip;
    }
    
}
