// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package sut.se.immigration.entity;

import org.springframework.beans.factory.annotation.Configurable;
import sut.se.immigration.entity.Notify;

privileged aspect Notify_Roo_Configurable {
    
    declare @type: Notify: @Configurable;
    
}
