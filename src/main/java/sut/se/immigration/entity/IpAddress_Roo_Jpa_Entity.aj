// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package sut.se.immigration.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;
import sut.se.immigration.entity.IpAddress;

privileged aspect IpAddress_Roo_Jpa_Entity {
    
    declare @type: IpAddress: @Entity;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long IpAddress.id;
    
    @Version
    @Column(name = "version")
    private Integer IpAddress.version;
    
    public Long IpAddress.getId() {
        return this.id;
    }
    
    public void IpAddress.setId(Long id) {
        this.id = id;
    }
    
    public Integer IpAddress.getVersion() {
        return this.version;
    }
    
    public void IpAddress.setVersion(Integer version) {
        this.version = version;
    }
    
}
