package sut.se.immigration.entity;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findRegistersByIdregisterEquals", "findRegistersByIdregister" })
public class Register {

    @NotNull
    private String idregister;

    @NotNull
    private String idnumber;

    @NotNull
    @Size(min = 2)
    private String fname;

    @NotNull
    @Size(min = 2)
    private String lname;

    @NotNull
    @Size(min = 2)
    private String race;

    @NotNull
    @Size(min = 2)
    private String nationality;

    @NotNull
    @Size(min = 2)
    private String sex;

    @NotNull
    private String date;

    @NotNull
    private String high;

    @NotNull
    private String weight;

    @NotNull
    private String passport;

    @NotNull
    @Size(min = 2)
    private String counntry;

    @NotNull
    private String cpassport;

    @NotNull
    private String epassport;

    @NotNull
    @Size(min = 2)
    private String vehicle;

    @NotNull
    private String phone;
}
