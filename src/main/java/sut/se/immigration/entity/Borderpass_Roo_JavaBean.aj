// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package sut.se.immigration.entity;

import sut.se.immigration.entity.Borderpass;

privileged aspect Borderpass_Roo_JavaBean {
    
    public String Borderpass.getFirstname() {
        return this.firstname;
    }
    
    public void Borderpass.setFirstname(String firstname) {
        this.firstname = firstname;
    }
    
    public String Borderpass.getLastname() {
        return this.lastname;
    }
    
    public void Borderpass.setLastname(String lastname) {
        this.lastname = lastname;
    }
    
}
