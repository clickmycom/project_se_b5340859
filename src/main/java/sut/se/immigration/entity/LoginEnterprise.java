package sut.se.immigration.entity;

import javax.validation.constraints.Size;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findLoginEnterprisesByBlockIpAddressEquals", "findLoginEnterprisesByUserpassEquals" })
public class LoginEnterprise {

    @Size(min = 2)
    private String userlogin;

    @Size(min = 2)
    private String userpass;

    private String StateLogin;

    private String BlockIpAddress;
}
