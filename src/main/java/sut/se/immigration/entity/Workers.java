package sut.se.immigration.entity;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findWorkersesByRegister_numberEquals", "findWorkersesByRegister_nameLike", "findWorkersesByRegister_numberLike", "findWorkersesByCheckinLike", "findWorkersesByCheckoutLike", "findWorkersesByRegister_positionLike", "findWorkersesByRegister_sexLike", "findWorkersesByRegister_chadLike" })
public class Workers {

    @NotNull
    @Size(min = 2)
    @Pattern(regexp = "[0-9][0-9][/][0-9][0-9][/][0-9][0-9][0-9][0-9]")
    private String Checkin;

    @Size(min = 2)
    @Pattern(regexp = "[0-9][0-9][/][0-9][0-9][/][0-9][0-9][0-9][0-9]")
    private String Checkout;

    private Integer TotalDate;

    @NotNull
    @Column(unique = false)
    @Pattern(regexp = "\\d{13}")
    private String register_number;

    @NotNull
    @Pattern(regexp = "[a-zA-Z\\s|]{1,40}")
    private String register_name;

    @Pattern(regexp = "[a-zA-Z\\s|]{1,20}")
    private String register_Mname;

    private String register_sex;

    @NotNull
    @Pattern(regexp = "[a-zA-Z\\s|]{1,30}")
    private String register_chad;

    @NotNull
    @Pattern(regexp = "[a-zA-Z\\s|]{1,30}")
    private String register_relation;

    @NotNull
    @Size(min = 2)
    @Pattern(regexp = "\\d{13}")
    private String register_position;
}
